package com.binarium.chipred.facturacion;

import org.json.JSONException;
import org.json.JSONObject;

public class InvoicesUtils
{
    public static Invoice purchaseToInvoice(JSONObject purchase)
    {
        Invoice invoice = null;

        try
        {
            // Validar si el consumo tiene "Comprobante"
            if (purchase.has("comprobante") && purchase.getJSONObject("comprobante").length() > 0)
            {
                // Obtener comprobante
                JSONObject comprobante = purchase.getJSONObject("comprobante");

                // Crear nuevo objeto
                invoice = new Invoice();
                invoice.setAmount(purchase.getDouble("costo"));
                invoice.setVolume(purchase.getDouble("cantidad"));
                invoice.setDate(purchase.getString("fecha_hora"));
                invoice.setStationDesc(purchase.getString("nombre_estacion"));
                invoice.setStationNumber(purchase.getString("numero_estacion"));
                invoice.setStatus(comprobante.getString("estado_hacienda"));
                invoice.setType(comprobante.getString("tipo"));
                invoice.setInvoiceNumber(comprobante.getJSONObject("json_original").getJSONObject("factura").getString("numero_consecutivo"));
                invoice.setKey(comprobante.getJSONObject("json_original").getJSONObject("factura").getString("clave"));
            }
        }
        catch (JSONException exception)
        {
            exception.printStackTrace();
        }

        return invoice;
    }
}
