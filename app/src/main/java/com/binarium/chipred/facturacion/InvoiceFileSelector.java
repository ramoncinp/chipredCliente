package com.binarium.chipred.facturacion;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.core.content.FileProvider;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.binarium.chipred.BuildConfig;
import com.binarium.chipred.ChipRedManager;
import com.binarium.chipred.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class InvoiceFileSelector extends DialogFragment
{
    // Constantes
    private static final String TAG = SimpleDateFormat.class.getSimpleName();
    private static final String PDF_FILE = ".pdf";
    private static final String XML_FILE = ".xml";
    //
    private static final int PRODUCCION = 0;
    private static final int STAGGING = 1;
    private static final int LOCAL = 2;

    // Variables
    private boolean refreshingHaciendaFile = false;
    private Invoice invoice;
    private int wolkeEnviroment = BuildConfig.BUILD_TYPE.equals("debug") ? LOCAL : PRODUCCION;

    // Objetos
    private ArrayList<String> filePaths = new ArrayList<>();
    private ArrayList<String> fileTypes = new ArrayList<>();
    private ArrayList<String> urls = new ArrayList<>();
    private ChipRedManager chipRedManager;

    // Views
    private LinearLayout filesLayout;
    private LinearLayout getPdfLayout;
    private LinearLayout getXmlLayout;
    private LinearLayout getHMessageLayout;
    private LinearLayout getHXmlLayout;
    private LinearLayout haciendaMessageLayout;
    private LinearLayout onDownloadedLayout;
    private LinearLayout progressLayout;
    private TextView progressText;

    public static InvoiceFileSelector newInstance(String invoiceJson)
    {
        InvoiceFileSelector invoiceFileSelector = new InvoiceFileSelector();

        Bundle args = new Bundle();
        args.putString("invoice", invoiceJson);
        invoiceFileSelector.setArguments(args);

        return invoiceFileSelector;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);

        // Obtener datos de factura
        if (getArguments() != null)
        {
            // Obtener en formato de String
            String invoiceString = getArguments().getString("invoice");

            try
            {
                // Convertir a JSON
                JSONObject invoiceJSON = new JSONObject(invoiceString);

                // Convertir JSON a objeto
                invoice = new Invoice(invoiceJSON);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

        // Obtener layout
        View content = inflater.inflate(R.layout.dialog_fragment_select_invoice_file, null);

        // Obtener views
        filesLayout = content.findViewById(R.id.files_layout);
        getPdfLayout = content.findViewById(R.id.download_pdf_layout);
        getXmlLayout = content.findViewById(R.id.download_xml_layout);
        getHMessageLayout = content.findViewById(R.id.get_h_message_layout);
        getHXmlLayout = content.findViewById(R.id.download_h_xml_layout);
        haciendaMessageLayout = content.findViewById(R.id.hacienda_message_layout);
        onDownloadedLayout = content.findViewById(R.id.on_downloaded_layout);
        progressLayout = content.findViewById(R.id.progress_layout);
        progressText = content.findViewById(R.id.progress_text);

        // Crear listener
        View.OnClickListener getFileListener = new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                // Obtener view "clickeado"
                switch (view.getId())
                {
                    case R.id.download_pdf_layout:
                        Toast.makeText(getContext(), "Descargando pdf...", Toast.LENGTH_SHORT).show();
                        downloadFile(PDF_FILE, false);
                        break;

                    case R.id.download_xml_layout:
                        Toast.makeText(getContext(), "Descargando xml...", Toast.LENGTH_SHORT).show();
                        downloadFile(XML_FILE, false);
                        break;

                    case R.id.download_h_xml_layout:
                        if (getHXmlLayout.getAlpha() != 0.5f)
                        {
                            Toast.makeText(getContext(), "Descargando xml de hacienda...",
                                    Toast.LENGTH_SHORT).show();
                            downloadFile(XML_FILE, true);
                        }
                        else
                        {
                            return;
                        }
                        break;

                    case R.id.get_h_message_layout:
                        requestHMessage();
                        break;
                }

                filesLayout.setVisibility(View.GONE);
                progressLayout.setVisibility(View.VISIBLE);
            }
        };

        // Asignar listeners
        getPdfLayout.setOnClickListener(getFileListener);
        getXmlLayout.setOnClickListener(getFileListener);
        getHMessageLayout.setOnClickListener(getFileListener);
        getHXmlLayout.setOnClickListener(getFileListener);

        // Obtener botones
        Button returnButton = content.findViewById(R.id.return_button);
        returnButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                getDialog().dismiss();
            }
        });

        Button shareAllButton = content.findViewById(R.id.share_all_button);
        shareAllButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                filesLayout.setVisibility(View.GONE);
                progressLayout.setVisibility(View.VISIBLE);
                downloadAllFiles();
            }
        });

        // Checar permisos de escritura
        checkPermissions();

        // Validar si existe el archivo de hacienda
        checkHaciendaFile();

        return content;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (getDialog().getWindow() != null)
        {
            getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
        {
            Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
        }
    }

    private void checkPermissions()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED)
            {
                Log.v(TAG, "Permission is granted");
            }
            else
            {
                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }
        }
        else
        {
            //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
        }
    }

    private void downloadFile(String fileFormat, boolean mensajeHacienda)
    {
        // Limpiar listas
        urls.clear();
        filePaths.clear();
        fileTypes.clear();

        // Definir formato actual
        fileTypes.add(fileFormat);

        // Crear link
        String url;

        // Definir url en base a el ambiente de desarrollo
        switch (wolkeEnviroment)
        {
            case LOCAL:
                url = "http://192.168.0.50:9993/";
                break;

            case STAGGING:
                if (!mensajeHacienda)
                {
                    url = "http://www.chipred.com/facturas_crst/";
                }
                else
                {
                    url = "http://www.chipred.com/mensajes_crst/MH";
                }
                break;

            case PRODUCCION:
            default:
                if (!mensajeHacienda)
                {
                    url = "http://www.chipred.com/facturas_cr/";
                }
                else
                {
                    url = "http://www.chipred.com/mensajes_cr/MH";
                }

                break;
        }

        // Agregar clave de la factura y extensión
        url += invoice.getKey();
        url += fileFormat;

        // Agregar url a la lista
        urls.add(url);

        // Iniciar tarea para descargar
        new DownloadFileFromURL().execute(urls.get(0));
    }

    private void downloadAllFiles()
    {
        // Limpiar listas
        urls.clear();
        filePaths.clear();
        fileTypes.clear();

        // Crear links
        String url;
        if (wolkeEnviroment == LOCAL)
        {
            url = "http://192.168.0.50:9993/";
            url += invoice.getKey();

            // Agregar tipos de archivo (No se agrega el de mensaje de aceptación)
            fileTypes.add(".pdf");
            fileTypes.add(".xml");

            // Agregar elementos
            urls.add(url + fileTypes.get(0));
            urls.add(url + fileTypes.get(1));
        }
        else
        {
            url = "http://www.chipred.com/";
            String mensajeHaciendaUrl;
            if (wolkeEnviroment == PRODUCCION)
            {
                mensajeHaciendaUrl = (url + "mensajes_cr/MH");
                url += "facturas_cr/";
            }
            else
            {
                mensajeHaciendaUrl = (url + "mensajes_crst/MH");
                url += "facturas_crst/";
            }

            // Agregar clave de la factura
            url += invoice.getKey();
            mensajeHaciendaUrl += invoice.getKey();

            // Agregar tipos de archivo (No se agrega el de mensaje de aceptación)
            fileTypes.add(".pdf");
            fileTypes.add(".xml");


            // Agregar elementos
            urls.add(url + fileTypes.get(0));
            urls.add(url + fileTypes.get(1));

            // Validar si esta disponible el archivo de hacienda
            if (getHXmlLayout.getAlpha() == 1f)
            {
                // Agregar archivo de aceptación
                fileTypes.add(".xml");
                urls.add(mensajeHaciendaUrl + fileTypes.get(2));
            }
        }

        if (urls.size() > 2)
        {
            new DownloadFileFromURL().execute(urls.get(0), urls.get(1), urls.get(2));
        }
        else
        {
            new DownloadFileFromURL().execute(urls.get(0), urls.get(1));
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        if (getDialog().getWindow() != null)
        {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
    }

    private void showOnDownloadedLayout()
    {
        // Mostrar solo layout de archivo descargado
        onDownloadedLayout.setVisibility(View.VISIBLE);
        filesLayout.setVisibility(View.GONE);
        progressLayout.setVisibility(View.GONE);

        // Obtener views
        ImageView fileIcon = getDialog().findViewById(R.id.downloaded_file_icon);
        TextView fileName = getDialog().findViewById(R.id.downloaded_file_name);
        Button returnButton = getDialog().findViewById(R.id.downloaded_file_return_button);
        Button openFileButton = getDialog().findViewById(R.id.open_downloaded_file_button);
        CardView shareButton = getDialog().findViewById(R.id.share_downloaded_file_button);

        // Mostrar texto e icono correspondientes
        final String filePath = filePaths.get(0);
        fileName.setText(filePath.substring(filePath.lastIndexOf("/") + 1));

        // Obtener y mostrar icono
        final String fileType = fileTypes.get(0);
        if (fileType.equals(PDF_FILE))
        {
            fileIcon.setImageResource(R.drawable.ic_pdf);
        }
        else
        {
            fileIcon.setImageResource(R.drawable.ic_xml);
        }

        // Definir listeners de botones
        returnButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onDownloadedLayout.setVisibility(View.GONE);
                filesLayout.setVisibility(View.VISIBLE);
            }
        });

        openFileButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                File file = new File(filePath);
                Uri fileUri;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                {
                    fileUri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID +
                            ".fileprovider", file);
                }
                else
                {
                    fileUri = Uri.fromFile(new File(filePath));
                }

                String mime = getActivity().getContentResolver().getType(fileUri);
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(fileUri, fileType.equals(PDF_FILE) ? mime : "application" +
                        "/xhtml+xml");
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                try
                {
                    getActivity().startActivity(intent);
                }
                catch (ActivityNotFoundException e)
                {
                    Toast.makeText(getActivity(), "Error al intentar abrir archivo",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        shareButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                shareFile();
            }
        });
    }

    private void shareFile()
    {
        try
        {
            Uri fileUri;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            {
                fileUri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID +
                        ".fileprovider", new File(filePaths.get(0)));
            }
            else
            {
                fileUri = Uri.fromFile(new File(filePaths.get(0)));
            }

            if (fileUri != null)
            {
                Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_STREAM, fileUri);
                shareIntent.setDataAndType(fileUri,
                        getActivity().getContentResolver().getType(fileUri));
                startActivityForResult(Intent.createChooser(shareIntent, "Compartir archivo"), 0);
            }
        }
        catch (NullPointerException | IllegalArgumentException e)
        {
            e.printStackTrace();
        }
    }

    private void shareAllFiles()
    {
        try
        {
            // Crear Intent
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND_MULTIPLE);
            intent.putExtra(Intent.EXTRA_SUBJECT, "Archivos de facturación");
            intent.setType("*/*");

            // Crear lista para almacenar URI's
            ArrayList<Uri> files = new ArrayList<>();

            // Crear Uris de la lisa de "paths"
            for (String path : filePaths)
            {
                Uri fileUri;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                {
                    fileUri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID +
                            ".fileprovider", new File(path));
                }
                else
                {
                    fileUri = Uri.fromFile(new File(path));
                }
                files.add(fileUri);
            }

            // Iniciar Intent
            intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, files);
            startActivity(intent);
        }
        catch (NullPointerException | IllegalArgumentException e)
        {
            e.printStackTrace();
        }
    }

    private void showToastMessage(String message)
    {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    private void checkHaciendaFile()
    {
        String url;
        switch (wolkeEnviroment)
        {
            case LOCAL:
                // Mostrar layout normal
                progressLayout.setVisibility(View.GONE);
                filesLayout.setVisibility(View.VISIBLE);
                return;

            case STAGGING:
                url = "http://www.chipred.com/mensajes_crst/MH";
                break;

            case PRODUCCION:
            default:
                url = "http://www.chipred.com/mensajes_cr/MH";
                break;
        }
        final String theUrl = url + invoice.getKey() + ".xml";

        // Crear queue
        RequestQueue queue = Volley.newRequestQueue(getActivity());

        // prepare the Request
        StringRequest getRequest = new StringRequest(Request.Method.GET, theUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        if (!refreshingHaciendaFile)
                        {
                            // Mostrar contenido
                            progressLayout.setVisibility(View.GONE);
                            filesLayout.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            refreshingHaciendaFile = false;
                        }

                        getHXmlLayout.setAlpha(1f);
                        Log.d(TAG, "Archivo de Hacienda obtenido correctamente");
                    }
                }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                // Mostrar contenido
                progressLayout.setVisibility(View.GONE);
                filesLayout.setVisibility(View.VISIBLE);

                Log.e(TAG, "Error al obtener archivo de hacienda");
            }
        });

        // add it to the RequestQueue
        queue.add(getRequest);
    }

    private void requestHMessage()
    {
        // Mostrar progreso
        String waitMessage = "Consultando mensaje\npor favor espere...";
        progressLayout.setVisibility(View.VISIBLE);
        progressText.setText(waitMessage);

        if (chipRedManager == null)
            chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived()
            {
                @Override
                public void chipRedMessage(String crMessage, int webServiceN)
                {
                    progressLayout.setVisibility(View.GONE);
                    filesLayout.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), crMessage, Toast.LENGTH_LONG).show();
                }

                @Override
                public void chipRedError(String errorMessage, int webServiceN)
                {
                    progressLayout.setVisibility(View.GONE);
                    filesLayout.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
                }

                @Override
                public void showResponse(JSONObject response, int webServiceN)
                {
                    showHMessageLayout(response);
                }
            }, getActivity());

        chipRedManager.getHaciendaMessage(invoice.getKey(), invoice.getStationNumber());
    }

    private void showHMessageLayout(JSONObject response)
    {
        // Mostrar layout
        progressLayout.setVisibility(View.GONE);
        haciendaMessageLayout.setVisibility(View.VISIBLE);

        // Obtener views correspondientes
        TextView invoiceStatus = getDialog().findViewById(R.id.invoice_state_tv);
        TextView statusMessage = getDialog().findViewById(R.id.invoice_message);
        Button dismissButton = getDialog().findViewById(R.id.h_message_dismiss_button);
        Button returnButton = getDialog().findViewById(R.id.h_message_return_button);

        // Obtener datos
        try
        {
            String status = response.getJSONObject("data").getString("estado_factura");
            String message = response.getJSONObject("data").getString("mensaje_hacienda");
            invoiceStatus.setText(status);
            statusMessage.setText(message);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            haciendaMessageLayout.setVisibility(View.GONE);
            filesLayout.setVisibility(View.VISIBLE);
            Toast.makeText(getActivity(), "Error al procesar los datos", Toast.LENGTH_SHORT).show();
        }

        dismissButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                getDialog().dismiss();
            }
        });

        returnButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                haciendaMessageLayout.setVisibility(View.GONE);
                filesLayout.setVisibility(View.VISIBLE);
            }
        });

        // Revisar nuevamente si existe el archivo xml de hacienda
        refreshingHaciendaFile = true;
        checkHaciendaFile();
    }

    class DownloadFileFromURL extends AsyncTask<String, String, String>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            progressText.setText(getResources().getString(R.string.downloading));
        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            progressText.setText(getResources().getString(R.string.downloaded));

            if (s.equals("error"))
            {
                showToastMessage("Error al descargar archivos");
                progressLayout.setVisibility(View.GONE);
                filesLayout.setVisibility(View.VISIBLE);
            }
            else
            {
                // Si se descargó más de un archivo
                if (urls.size() > 1)
                {
                    // Compartir archivos
                    shareAllFiles();

                    // Cerrar diálogo
                    getDialog().dismiss();
                }
                else
                {
                    // Mostrar contenido de descargado
                    showOnDownloadedLayout();
                }
            }
        }

        @Override
        protected void onProgressUpdate(String... values)
        {
            super.onProgressUpdate(values);

            String progressString = values[0] + "%";
            progressText.setText(progressString);
            Log.d(TAG, "Progreso -> " + Integer.parseInt(values[0]));
        }

        @Override
        protected String doInBackground(String... strings)
        {
            int count;
            try
            {
                for (int i = 0; i < strings.length; i++)
                {
                    // Obtener URL del archivo
                    URL url = new URL(strings[i]);

                    // Abrir la conexión
                    URLConnection connection = url.openConnection();
                    connection.connect();

                    // Obtener tamaño del archivo para mostrar porcentaje
                    int fileLength = connection.getContentLength();

                    // Descargar el archivo
                    InputStream input = new BufferedInputStream(url.openStream(), 8192);

                    //Obtener archivo
                    File rootPath = new File(Environment.getExternalStorageDirectory(),
                            "/download" +
                                    "/");
                    if (!rootPath.exists())
                    {
                        rootPath.mkdirs();
                    }
                    final File file = new File(rootPath,
                            strings[i].substring(strings[i].lastIndexOf("/") + 1));

                    // Definir path del archivo que se guardará
                    filePaths.add(file.getAbsolutePath());

                    // Validar si existe el archivo
                    if (!file.exists())
                    {
                        // Output stream
                        OutputStream output =
                                new FileOutputStream(file);

                        byte data[] = new byte[1024];
                        long total = 0;

                        // Obtener datos del archivo mientras existan
                        while ((count = input.read(data)) != -1)
                        {
                            // Obtener cuanto se lleva descargado hasta el momento
                            total += count;

                            // Actualizar progress
                            publishProgress("" + (int) ((total * 100) / fileLength));

                            // Escribir en el archivo de salida
                            output.write(data, 0, count);
                        }

                        // Liberar la salida
                        output.flush();

                        // Cerrar archivos
                        output.close();
                        input.close();
                    }
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return "error";
            }

            return "ok";
        }
    }
}
