package com.binarium.chipred.facturacion;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.binarium.chipred.ChipREDConstants;
import com.binarium.chipred.R;

import java.util.ArrayList;

public class InvoicesArrayAdapter extends RecyclerView.Adapter<InvoicesArrayAdapter.InvoiceViewHolder> implements View.OnClickListener
{
    private final ArrayList<Invoice> invoices;
    private Context context;
    private View.OnClickListener listener;

    public InvoicesArrayAdapter(ArrayList<Invoice> invoices, Context context)
    {
        this.invoices = invoices;
        this.context = context;
    }

    @Override
    public int getItemCount()
    {
        return invoices.size();
    }

    @Override
    public InvoiceViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.invoice_layout,
                viewGroup, false);
        InvoiceViewHolder invoiceViewHolder = new InvoiceViewHolder(v);

        v.setOnClickListener(this);
        return invoiceViewHolder;
    }

    @Override
    public void onBindViewHolder(InvoiceViewHolder invoiceViewHolder, int i)
    {
        // Obtener factura
        Invoice invoice = invoices.get(i);

        // Crear variables para formatear valores
        String amountString, volumeString, dateString;
        amountString = ChipREDConstants.CR_AMOUNT_FORMAT.format(invoice.getAmount()) + " " +
                ChipREDConstants.CRC_CURRENCY_ISO;
        volumeString = ChipREDConstants.LITERS_FORMAT.format(invoice.getVolume()) + " LTS";
        dateString = invoice.getDate();

        // Escribir en textViews
        invoiceViewHolder.amount.setText(amountString);
        invoiceViewHolder.volume.setText(volumeString);
        invoiceViewHolder.date.setText(dateString);
        invoiceViewHolder.station.setText(invoice.getStationDesc());
        invoiceViewHolder.invoiceNumber.setText(invoice.getInvoiceNumber());

        // Evaluar tipo de comprobante
        if (invoice.getType().equals("01"))
        {
            invoiceViewHolder.invoiceType.setText(context.getResources().getString(R.string.factura_electronica));
        }
        else
        {
            invoiceViewHolder.invoiceType.setText(context.getResources().getString(R.string.tiquete_electronica));
        }

        // Dar color al elemento dependiendo del estado
        invoiceViewHolder.itemView.setBackgroundColor(ContextCompat.getColor(context,
                ChipREDConstants.getColorStatusHacienda(invoice.getStatus())));
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setOnClickListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onClick(View view)
    {
        if (listener != null)
        {
            listener.onClick(view);
        }
    }

    static class InvoiceViewHolder extends RecyclerView.ViewHolder
    {
        private TextView amount;
        private TextView volume;
        private TextView date;
        private TextView invoiceType;
        private TextView station;
        private TextView invoiceNumber;

        InvoiceViewHolder(View itemView)
        {
            super(itemView);
            amount = itemView.findViewById(R.id.invoice_amount);
            volume = itemView.findViewById(R.id.invoice_volume);
            date = itemView.findViewById(R.id.invoice_date);
            invoiceType = itemView.findViewById(R.id.invoice_type);
            station = itemView.findViewById(R.id.invoice_station);
            invoiceNumber = itemView.findViewById(R.id.invoice_number);
        }
    }
}

