package com.binarium.chipred.facturacion;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;

import androidx.annotation.NonNull;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipred.ChipREDConstants;
import com.binarium.chipred.ChipRedManager;
import com.binarium.chipred.DatePickerFragment;
import com.binarium.chipred.R;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

public class SearchInvoices extends AppCompatActivity
{
    // Constantes
    private static final String TAG = SearchInvoices.class.getSimpleName();
    private static final int INVOICES_LIMIT = 10;

    // Variables
    private boolean fetching = false;
    private boolean allInvoices = false;
    private int offset = 0;
    private String startDate;
    private String startDateString;
    private String endDate;
    private String endDateString;

    // Objetos
    private ArrayList<Invoice> invoices = new ArrayList<>();
    private ArrayAdapter<StationReference> stationsAdapter;
    private ArrayList<StationReference> stations = new ArrayList<>();
    private ChipRedManager chipRedManager;
    private InvoicesArrayAdapter invoicesArrayAdapter;
    private LinearLayoutManager linearLayoutManager;
    private StationReference selectedStation;

    // Views
    private Dialog filterDialog;
    private ProgressBar paginationProgressBar;
    private ProgressBar progressBar;
    private RecyclerView invoicesList;
    private TextView noInvoices;
    private MaterialBetterSpinner stationsSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //Agregar flecha para regresar
        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Definir título
        setTitle(getResources().getString(R.string.comprobantes));

        // Crear actividad
        createActivity();

        // Obtener estaciones
        getStations();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == android.R.id.home)
        {
            this.finish();
        }
        else if (id == R.id.search_invoices)
        {
            showSearchDialog();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        if (filterDialog != null && filterDialog.isShowing())
        {
            filterDialog.dismiss();
        }

        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.search_invoices_menu, menu);

        return true;
    }

    private void createActivity()
    {
        // Mostrar contenido
        setContentView(R.layout.activity_search_invoices);

        // Inicializar views
        initViews();
    }

    private void initViews()
    {
        // Referenciar recyclerView
        invoicesList = findViewById(R.id.invoices_rv);
        paginationProgressBar = findViewById(R.id.pagination_progress);
        progressBar = findViewById(R.id.progress_bar);
        noInvoices = findViewById(R.id.no_invoices);

        // Inicialzar valores de parámetros
        startDateString = getTodayDate();
        endDateString = getTodayDate();

        // Inicializar linearLayoutManager
        linearLayoutManager = new LinearLayoutManager(this);

        // Agregar scrollListener
        invoicesList.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = invoices.size();
                int firstVisibleItemPosition =
                        linearLayoutManager.findFirstVisibleItemPosition();

                if (totalItemCount <= visibleItemCount)
                {
                    allInvoices = true;
                }

                if (visibleItemCount + firstVisibleItemPosition == totalItemCount && !fetching && !allInvoices)
                {
                    Log.d(TAG, "Pedir más facturas");
                    paginationProgressBar.setVisibility(View.VISIBLE);

                    //Agregar offset
                    offset += INVOICES_LIMIT;

                    //Pedir más facturas
                    getInvoices();

                    //Indicar que se esta ejecutando el servicio
                    fetching = true;
                }
            }
        });
    }

    private void showSearchDialog()
    {
        // Crear builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // Definir título
        builder.setTitle("Buscar comprobantes");

        // Obtener y asignar layout
        View content = getLayoutInflater().inflate(R.layout.dialog_search_invoices, null);
        builder.setView(content);

        // Crear diálogo
        filterDialog = builder.create();
        filterDialog.setCancelable(false);
        filterDialog.show();

        // Obtener views
        final CheckBox allStationsCb = content.findViewById(R.id.all_stations_cb);
        Button returnButton = content.findViewById(R.id.return_button);
        Button searchButton = content.findViewById(R.id.search_button);
        final MaterialEditText startDate = content.findViewById(R.id.start_date);
        final MaterialEditText endDate = content.findViewById(R.id.end_date);

        // Agregar validaciones
        METValidator emptyValidator = new METValidator("Campo obligatorio")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                return !isEmpty;
            }
        };
        startDate.validateWith(emptyValidator);
        endDate.validateWith(emptyValidator);

        // Inicializar spinner
        stationsSpinner =
                content.findViewById(R.id.select_station_spinner);

        // Crear adaptador
        stationsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_expandable_list_item_1
                , new ArrayList<StationReference>());
        stationsSpinner.setAdapter(stationsAdapter);

        // Agregar clickListener para habilitar
        stationsSpinner.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                // Si se toca el spinner, se habilita automáticamente
                allStationsCb.setChecked(false);
                stationsSpinner.setAlpha(1f);

                // Mostrar diálogo para seleccionar estacion
                showSelectStationDialog();
            }
        });

        // Agregar listener a checkBox
        allStationsCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b)
            {
                float alphaVal = b ? 0.5f : 1f;
                stationsSpinner.setAlpha(alphaVal);

                if (b)
                {
                    selectedStation = null;
                }
            }
        });

        // Asignar listeners a Botones
        returnButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                filterDialog.dismiss();
                returnFromActivity();
            }
        });

        searchButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                filterDialog.dismiss();
                searchInvoices(startDate.getText().toString(), endDate.getText().toString());
            }
        });

        // Agregar datePickers
        View.OnClickListener datePickerListener = new View.OnClickListener()
        {
            @Override
            public void onClick(final View v)
            {
                DatePickerFragment fragment = DatePickerFragment.newInstance(new DatePickerDialog
                        .OnDateSetListener()
                {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2)
                    {
                        String gottenDate =
                                ChipREDConstants.twoDigits(i2) + "-" + ChipREDConstants.twoDigits(i1 + 1) + "-" + i;

                        MaterialEditText dateEt = (MaterialEditText) v;
                        dateEt.setText(gottenDate);

                        // Actualizar valores de fechas
                        startDateString = startDate.getText().toString();
                        endDateString = endDate.getText().toString();
                    }
                });

                fragment.show(getFragmentManager(), "datePicker");
            }
        };

        // Asignar datePickers
        startDate.setOnClickListener(datePickerListener);
        endDate.setOnClickListener(datePickerListener);

        // Definir días default
        startDate.setText(startDateString);
        endDate.setText(endDateString);

        // Evaluar si ya hay una estación seleccionada
        if (selectedStation != null)
        {
            allStationsCb.setChecked(false);
            stationsSpinner.setAlpha(1f);
            stationsSpinner.setText(selectedStation.getName());
        }
        else
        {
            //Definir todas las estaciones por default
            allStationsCb.setChecked(true);
            stationsSpinner.setAlpha(0.5f);
        }
    }

    private void showSelectStationDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Seleccione estación");

        // Crear arreglo de strings
        String[] stringArray = new String[stations.size()];
        for (int i = 0; i < stations.size(); i++)
        {
            stringArray[i] = stations.get(i).toString();
        }

        builder.setItems(stringArray,
                new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        // Definir estacion seleccionada
                        selectedStation = stations.get(i);

                        // Mostrar en texto
                        stationsSpinner.setText(selectedStation.getName());

                        // Ocultar diálogo
                        dialogInterface.dismiss();
                    }
                });

        builder.setNegativeButton("Regresar", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setStationsList(JSONObject response)
    {
        try
        {
            // Obtener arreglo de estaciones
            JSONArray stationArray = response.getJSONArray("data");

            // Iterar en el arreglo
            for (int i = 0; i < stationArray.length(); i++)
            {
                // Obtener objeto JSON
                JSONObject station = stationArray.getJSONObject(i);

                // Crear objeto
                StationReference stationReference = new StationReference();
                stationReference.setName(station.getString("nombre_estacion"));
                stationReference.setId(station.getString("id_estacion"));

                // Agregar objeto a la lista
                stations.add(stationReference);
            }

            // Ordenar lista por orden alfabético
            Collections.sort(stations, new Comparator<StationReference>()
            {
                @Override
                public int compare(StationReference stationReference, StationReference t1)
                {
                    return stationReference.getName().compareTo(t1.getName());
                }
            });

            // Mostrar diálogo para indicar parámetros de búsqueda
            showSearchDialog();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    private void getStations()
    {
        // Inicializar manejador de webServices
        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived()
        {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN)
            {
                //Ocultar progressBar
                progressBar.setVisibility(View.GONE);
                fetching = false;

                if (webServiceN == ChipRedManager.GET_STATIONS)
                {
                    Toast.makeText(SearchInvoices.this, crMessage, Toast.LENGTH_LONG).show();
                    showSearchDialog();
                }
                else
                {
                    paginationProgressBar.setVisibility(View.GONE);
                    if (!invoices.isEmpty())
                    {
                        allInvoices = true;
                        Log.d(TAG, "No hay más facturas qué pedir");
                    }
                    else
                    {
                        noInvoices.setVisibility(View.VISIBLE);
                        invoicesList.setVisibility(View.GONE);
                        Toast.makeText(SearchInvoices.this, crMessage, Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN)
            {
                //Ocultar progressBar
                progressBar.setVisibility(View.GONE);
                fetching = false;

                Toast.makeText(SearchInvoices.this, errorMessage, Toast.LENGTH_LONG).show();

                if (webServiceN == ChipRedManager.GET_STATIONS)
                {
                    showSearchDialog();
                }
                else
                {
                    noInvoices.setVisibility(View.VISIBLE);
                    invoicesList.setVisibility(View.GONE);
                    paginationProgressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN)
            {
                fetching = false;

                if (webServiceN == ChipRedManager.GET_STATIONS)
                {
                    // Obtener lista de estaciones
                    setStationsList(response);
                }
                else if (webServiceN == ChipRedManager.GET_INVOICES)
                {
                    // Obtener consumos
                    getInvoices(response);

                    //Ocultar progressBar
                    progressBar.setVisibility(View.GONE);
                    paginationProgressBar.setVisibility(View.GONE);
                }
            }
        }, this);

        // Obtener estaciones
        chipRedManager.getStations();
    }

    private void searchInvoices(String startDate, String endDate)
    {
        // Mostrar progress
        progressBar.setVisibility(View.VISIBLE);
        noInvoices.setVisibility(View.GONE);
        invoicesList.setVisibility(View.GONE);
        invoices.clear();
        offset = 0;

        // Ajustar formato de fechas
        SimpleDateFormat appDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        SimpleDateFormat chipReddf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        try
        {
            // Convertir fechas a objetos
            Date date1 = appDateFormat.parse(startDate);
            Date date2 = appDateFormat.parse(endDate);

            // Convertir objetos a formato de webservice
            startDate = chipReddf.format(date1);
            endDate = chipReddf.format(date2);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
            Toast.makeText(this, "Error al formatear fechas", Toast.LENGTH_LONG).show();
            return;
        }

        // Agregar horas a las fechas
        startDate += " 00:00:00";
        endDate += " 23:59:59";

        // Asignar fechas globales
        this.startDate = startDate;
        this.endDate = endDate;

        // Enviar petición para obtener facturas
        getInvoices();
    }

    private void getInvoices()
    {
        // Ejectuar servicio
        chipRedManager.getInvoices(startDate, endDate, offset, selectedStation != null ?
                selectedStation.getId() : "");
    }

    private void returnFromActivity()
    {
        Intent resultIntent = new Intent();
        setResult(0, resultIntent); // Everything Ok
        finish();
    }

    private String getTodayDate()
    {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());

        return c.get(Calendar.DAY_OF_MONTH) + "-" + ChipREDConstants.twoDigits(c.get(Calendar.MONTH) + 1) + "-" + c.get(Calendar.YEAR);
    }

    public void getInvoices(JSONObject response)
    {
        try
        {
            //Obtener los comprobantes
            JSONArray data = response.getJSONArray("data");
            for (int i = 0; i < data.length(); i++)
            {
                // Obtener objeto JSON
                JSONObject purchase = data.getJSONObject(i);

                // Convertir consumo a objeto factura
                Invoice invoice = InvoicesUtils.purchaseToInvoice(purchase);
                if (invoice != null)
                    // Agregar a la lista si se pudo convertir
                    invoices.add(invoice);
            }

            if (invoices.isEmpty())
            {
                // NO Mostrar lista
                noInvoices.setVisibility(View.VISIBLE);
                invoicesList.setVisibility(View.GONE);
            }
            else
            {
                // Mostrar lista
                noInvoices.setVisibility(View.GONE);
                invoicesList.setVisibility(View.VISIBLE);

                // Evaluar actualizacion o creación de adaptador
                if (invoicesArrayAdapter == null)
                {
                    // Crear adaptador
                    invoicesArrayAdapter = new InvoicesArrayAdapter(invoices, this);

                    // Asignar clickListener
                    invoicesArrayAdapter.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {
                            InvoiceFileSelector invoiceFileSelector =
                                    InvoiceFileSelector.newInstance(invoices.get(invoicesList.getChildAdapterPosition(view)).toJSON().toString());
                            invoiceFileSelector.show(getSupportFragmentManager(),
                                    "invoice_file_selector");
                        }
                    });

                    // Asignar adaptador
                    invoicesList.setAdapter(invoicesArrayAdapter);
                    invoicesList.setLayoutManager(linearLayoutManager);
                }
                else
                {
                    // Refrescar adaptador
                    invoicesArrayAdapter.notifyDataSetChanged();
                }
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            Log.e(TAG, e.getLocalizedMessage());
            Log.e(TAG, e.getMessage());
            Snackbar.make(findViewById(android.R.id.content), "Error al procesar " +
                            "lista de consumos",
                    Snackbar.LENGTH_LONG).show();
        }
    }
}