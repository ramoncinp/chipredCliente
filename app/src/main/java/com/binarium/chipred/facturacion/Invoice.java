package com.binarium.chipred.facturacion;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Invoice
{
    private double volume;
    private double amount;
    private String date;
    private String invoiceNumber;
    private String key;
    private String stationDesc;
    private String stationNumber;
    private String status;
    private String type;

    public Invoice()
    {
    }

    public Invoice(JSONObject jsonObject)
    {
        try
        {
            volume = jsonObject.getDouble("cantidad");
            amount = jsonObject.getDouble("costo");
            date = jsonObject.getString("fecha_hora");
            invoiceNumber = jsonObject.getString("numero_consecutivo");
            key = jsonObject.getString("clave");
            stationDesc = jsonObject.getString("nombre_estacion");
            stationNumber = jsonObject.getString("numero_estacion");
            status = jsonObject.getString("estado_hacienda");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public double getVolume()
    {
        return volume;
    }

    public void setVolume(double volume)
    {
        this.volume = volume;
    }

    public double getAmount()
    {
        return amount;
    }

    public void setAmount(double amount)
    {
        this.amount = amount;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
        convertDate(date);
    }

    public String getInvoiceNumber()
    {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber)
    {
        this.invoiceNumber = invoiceNumber;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getStationDesc()
    {
        return stationDesc;
    }

    public void setStationDesc(String stationDesc)
    {
        this.stationDesc = stationDesc;
    }

    public String getStationNumber()
    {
        return stationNumber;
    }

    public void setStationNumber(String stationNumber)
    {
        this.stationNumber = stationNumber;
    }

    public JSONObject toJSON()
    {
        try
        {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("cantidad", volume);
            jsonObject.put("costo", amount);
            jsonObject.put("fecha_hora", date);
            jsonObject.put("numero_consecutivo", invoiceNumber);
            jsonObject.put("clave", key);
            jsonObject.put("nombre_estacion", stationDesc);
            jsonObject.put("numero_estacion", stationNumber);
            jsonObject.put("estado_hacienda", status);

            return jsonObject;
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    private void convertDate(String date)
    {
        try
        {
            // Asegurar formato
            // yyyy-MM-ddTHH:mm:ss
            date = date.substring(0, 19);
            date = date.replace("T", " ");

            final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                    Locale.getDefault());
            final SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MMMM-yyyy HH:mm:ss",
                    Locale.getDefault());

            // Convertir a fecha la cadena que llegó
            Date mDate = dateFormat.parse(date);

            // Evaluar objeto
            if (mDate != null)
            {
                // Obtener fecha con formato correcto
                this.date = outputFormat.format(mDate).toUpperCase();
            }
        }
        catch (Exception e)
        {
            Log.e("Invoice", "Error al convertir fecha");
        }
    }
}
