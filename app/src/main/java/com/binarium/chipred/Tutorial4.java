package com.binarium.chipred;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.Toolbar;
import android.view.WindowManager;

import com.github.paolorotolo.appintro.AppIntro;

public class Tutorial4 extends AppIntro
{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        addSlide(TutGenericFragment.newInstance(
                R.layout.tut_layout,
                "Mis consumos",
                "Visualiza los litros de cada producto que has consumido",
                R.drawable.tutorial_frgmt_purchases1));

        addSlide(TutGenericFragment.newInstance(
                R.layout.tut_layout,
                "Mis consumos",
                "Selecciona diferentes periodos de tiempo ",
                R.drawable.tutorial_frgmt_purchases2));

        addSlide(TutGenericFragment.newInstance(
                R.layout.tut_layout,
                "Mis consumos",
                "Elige ver tus consumos de forma semanal o mensual",
                R.drawable.tutorial_frgmt_purchases3));

        addSlide(TutGenericFragment.newInstance(
                R.layout.tut_layout,
                "Mis consumos",
                "Observa el total de litros consumidos y presiona " +
                        "la flecha para ver cuánto consumiste de cada producto",
                R.drawable.tutorial_frgmt_purchases35));

        addSlide(TutGenericFragment.newInstance(
                R.layout.tut_layout,
                "Mis consumos",
                "Puedes observarlos también en forma de lista",
                R.drawable.tutorial_frgmt_purchases4));

        addSlide(TutGenericFragment.newInstance(
                R.layout.tut_layout,
                "Mis consumos",
                "Presiona un elemento de la lista para ver el detalle del consumo",
                R.drawable.tutorial_frgmt_purchases5));

        addSlide(TutGenericFragment.newInstance(
                R.layout.tut_layout,
                "Mis consumos",
                "Conoce cuánto dinero has invertido en cada producto",
                R.drawable.tutorial_frgmt_purchases6));

        addSlide(TutGenericFragment.newInstance(
                R.layout.tut_layout,
                "Mis consumos",
                "Toca cualquier color de la gráfica para enlistar las transacciones " +
                        "del producto",
                R.drawable.tutorial_frgmt_purchases7));

        setFadeAnimation();
        setIndicatorColor(
                getResources().getColor(R.color.colorAccent),
                getResources().getColor(R.color.lowRed)
        );

        setColorSkipButton(getResources().getColor(R.color.colorPrimaryDark));
        setColorDoneText(getResources().getColor(R.color.colorPrimaryDark));

        setSkipText("OMITIR");
        setDoneText("LISTO");

        setNextArrowColor(getResources().getColor(R.color.colorAccent));

        Toolbar toolbar = findViewById(R.id.toolbar);
        if (getSupportActionBar() == null) setSupportActionBar(toolbar);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
    }

    @Override
    public void onSkipPressed(Fragment currentFragment)
    {
        super.onSkipPressed(currentFragment);

        finishActivity();
    }

    @Override
    public void onDonePressed(Fragment currentFragment)
    {
        super.onDonePressed(currentFragment);

        finishActivity();
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment)
    {
        super.onSlideChanged(oldFragment, newFragment);

    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (getSupportActionBar() != null)
            getSupportActionBar().show();
    }

    private void finishActivity()
    {
        finish();
    }
}
