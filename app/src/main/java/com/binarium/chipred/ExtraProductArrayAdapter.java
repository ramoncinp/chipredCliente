package com.binarium.chipred;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class ExtraProductArrayAdapter extends RecyclerView.Adapter<ExtraProductArrayAdapter
        .ExtraProductViewHolder>
{
    private ArrayList<ExtraProduct> extraProducts;
    private Context context;

    public ExtraProductArrayAdapter(ArrayList<ExtraProduct> extraProducts, Context context)
    {
        this.extraProducts = extraProducts;
        this.context = context;
    }

    @NonNull
    @Override
    public ExtraProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .purchase_detail_element_extra_product, parent, false);

        return new ExtraProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ExtraProductViewHolder holder, int position)
    {
        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(context);
        String country = sharedPreferencesManager.getStringFromSP("pais", "mexico");

        ExtraProduct extraProduct = extraProducts.get(position);
        holder.description.setText(extraProduct.getDescription());

        if (country.equals("mexico"))
        {
            String qtyPriceString = String.valueOf(extraProduct.getQuantity()) + " x " +
                    ChipREDConstants.MX_AMOUNT_FORMAT.format(extraProduct.getPrice());
            holder.qtyPrice.setText(qtyPriceString);
            holder.subtotal.setText(ChipREDConstants.MX_AMOUNT_FORMAT.format(extraProduct.getSubTotal()) + " MXN");
        }
        else
        {
            String qtyPriceString = String.valueOf(extraProduct.getQuantity()) + " x " +
                    ChipREDConstants.CR_AMOUNT_FORMAT.format(extraProduct.getPrice());
            holder.qtyPrice.setText(qtyPriceString);
            holder.subtotal.setText(ChipREDConstants.MX_AMOUNT_FORMAT.format(extraProduct.getSubTotal()) + " CRC");
        }
    }

    @Override
    public int getItemCount()
    {
        return extraProducts.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class ExtraProductViewHolder extends RecyclerView.ViewHolder
    {
        private TextView description;
        private TextView qtyPrice;
        private TextView subtotal;

        public ExtraProductViewHolder(View itemView)
        {
            super(itemView);

            description = itemView.findViewById(R.id.purchase_detail_extra_product_desc);
            qtyPrice = itemView.findViewById(R.id.purchase_detail_extra_product_qty_price);
            subtotal = itemView.findViewById(R.id.purchase_detail_extra_product_subtotal);
        }
    }
}
