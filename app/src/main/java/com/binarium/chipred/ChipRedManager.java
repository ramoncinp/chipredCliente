package com.binarium.chipred;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class ChipRedManager
{
    public static int LOGIN_WB = 1;
    public static int NEWPASS_WB = 2;
    public static int GENERATE_WB = 3;
    public static int CHECK_BALANCE_WB = 4;
    public static int GET_BILLETS_WB = 5;
    public static int DELETE_BILLET_WB = 6;
    public static int ASK_PURCHASES_WB = 7;
    public static int ASK_PAYMENTS_WB = 8;
    public static int TEST_CONNECTION_WB = 9;
    public static int ADD_OR_MODIFY_CLIENT_WB = 10;
    public static int ADD_DEVICE_WB = 11;
    public static int CHECK_DEVICE_WB = 12;
    public static int SEARCH_CLIENT_WB = 13;
    public static int CHANGE_NIP_WB = 14;
    public static int GET_INVOICES = 15;
    public static int GET_LAST_PURCHASES = 16;
    public static int GET_STATIONS = 17;
    public static int GET_HACIENDA_MESSAGE = 18;
    public static int RESEND_CONFIRMATION_EMAIL = 19;
    public static int GET_ACCOUNT_TRANSACTIONS = 20;

    //Posibles respuestas
    public static String OK = "Ok";
    public static String ERROR = "Error";
    public static String AVISO = "Aviso";
    public static String LOGOUT = "Logout";

    //Valores a buscar de un cliente
    public final static int BYNAME = 1;
    public final static int BYEMAIL = 2;
    public final static int BYID = 3;
    public final static int BYTAXCODE = 4;
    public final static int BYPHONE = 5;

    private RequestQueue queue;
    private String URL;
    private OnMessageReceived messageReceived;
    private Context activityContext;
    private boolean local = BuildConfig.BUILD_TYPE.equals("debug");

    private SharedPreferencesManager sharedPreferencesManager;

    private String clientId;
    private String deviceId;

    public ChipRedManager(OnMessageReceived messageReceived, Context activityContext)
    {
        this.messageReceived = messageReceived;
        this.activityContext = activityContext;

        String ipServer = "192.168.0.50";
        String portServer = "9998";

        if (local)
        {
            URL = "http://" + ipServer + ":" + portServer + "/webservice_app/";
        }
        else
        {
            URL = "http://www.chipred.com/webservice_app/";
        }

        sharedPreferencesManager = new SharedPreferencesManager(activityContext);
        clientId = sharedPreferencesManager.getStringFromSP("id_cliente", "");
        deviceId = ChipREDConstants.getDeviceId(activityContext);
    }

    public void userLogIn(String user, String password)
    {
        if (queue == null)
        {
            queue = Volley.newRequestQueue(activityContext);
        }

        JSONObject request = new JSONObject();

        try
        {
            request.put("key", "login");
            request.put("email", user);
            request.put("password", password);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, URL, request, new
                Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        try
                        {
                            if (response.getString("response").contains("Error"))
                            {
                                messageReceived.chipRedError(response.getString("message"),
                                        LOGIN_WB);
                                VolleyLog.e("Error: ", response.getString("Error"));
                            }
                            else
                            {
                                messageReceived.showResponse(response, LOGIN_WB);
                            }
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (error.networkResponse == null)
                {
                    messageReceived.chipRedError(activityContext.getResources().getString(R
                            .string.logInError) + activityContext.getResources().getString(R
                            .string.serverErrorMessage), LOGIN_WB);
                }
            }
        });

        req.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy
                .DEFAULT_BACKOFF_MULT));

        queue.add(req);
    }

    public void addAuthenticatedDevice(String clientId)
    {
        if (queue == null)
        {
            queue = Volley.newRequestQueue(activityContext);
        }

        //Volver a tomar el deviceId
        deviceId = ChipREDConstants.getDeviceId(activityContext);

        final JSONObject request = new JSONObject();

        try
        {
            request.put("key", "registrar_dispositivo_autenticado");
            request.put("id_cliente", clientId);
            request.put("id_dispositivo", deviceId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, URL, request, new
                Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        try
                        {
                            String responseString = response.getString("response");
                            if (responseString.contains(OK))
                            {
                                //Enviar un mensaje a la actividad de respuesta exitosa
                                messageReceived.chipRedMessage(OK, ADD_DEVICE_WB);
                            }
                            else if (responseString.contains(ERROR))
                            {
                                //Notificar que existió un error
                                messageReceived.chipRedError(response.getString("message"),
                                        ADD_DEVICE_WB);
                            }
                            else if (responseString.contains(AVISO))
                            {
                                //Notificar algun suceso que no deje continuar el proceso
                                messageReceived.chipRedMessage(response.getString("message"),
                                        ADD_DEVICE_WB);
                            }
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (error.networkResponse == null)
                {
                    messageReceived.chipRedError("Error al registrar dispositivo", ADD_DEVICE_WB);
                }
            }
        });

        req.setRetryPolicy(new DefaultRetryPolicy(7000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(req);
    }

    public void validateAuthenticatedDevice(Response.Listener<JSONObject> listener, Response
            .ErrorListener errorListener)
    {
        if (queue == null)
        {
            queue = Volley.newRequestQueue(activityContext);
        }

        JSONObject request = new JSONObject();

        try
        {
            request.put("key", "validar_dispositivo_autenticado");
            request.put("id_cliente", clientId);
            request.put("id_dispositivo", deviceId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        if (listener == null)
        {
            listener = new Response.Listener<JSONObject>()
            {
                @Override
                public void onResponse(JSONObject response)
                {
                    try
                    {
                        if (response.getString("response").contains(LOGOUT))
                        {
                            Log.d("LogOut", "LogOut Request!");
                            messageReceived.chipRedMessage(LOGOUT, CHECK_DEVICE_WB);
                        }
                        else
                        {
                            messageReceived.chipRedMessage(OK, CHECK_DEVICE_WB);
                        }
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            };
        }

        if (errorListener == null)
        {
            errorListener = new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    messageReceived.chipRedMessage(ERROR, CHECK_DEVICE_WB);
                }
            };
        }

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, URL, request,
                listener, errorListener);

        req.setRetryPolicy(new DefaultRetryPolicy(7000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(req);
    }

    public void forgottenPassword(String email)
    {
        if (queue == null)
        {
            queue = Volley.newRequestQueue(activityContext);
        }

        JSONObject request = new JSONObject();

        try
        {
            request.put("key", "validar_email_rc");
            request.put("email", email);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, URL, request, new
                Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        try
                        {
                            if (response.getString("response").contains("Ok"))
                            {
                                messageReceived.chipRedMessage("Se ha enviado un correo" + " de " +
                                                "recuperación de contraseña a la dirección " +
                                                "proporcionada.",
                                        NEWPASS_WB);
                            }
                            else
                            {
                                messageReceived.chipRedError(response.getString("message"),
                                        NEWPASS_WB);
                                VolleyLog.e("Error: ", response.getString("Error"));
                            }
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (error.networkResponse == null)
                {
                    messageReceived.chipRedError("Error al recuperar contraseña\nNo responde " +
                            "servidor", NEWPASS_WB);
                }
            }
        });

        req.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy
                .DEFAULT_BACKOFF_MULT));

        queue.add(req);
    }

    public void changeBalanceNip(String clientId, String nip, String password, String email)
    {
        if (queue == null)
        {
            queue = Volley.newRequestQueue(activityContext);
        }

        JSONObject request = new JSONObject();

        try
        {
            request.put("key", "cambiar_nip");
            request.put("id_cliente", clientId);
            request.put("nip", nip);
            request.put("password", password);
            request.put("email", email);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, URL, request, new
                Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        try
                        {
                            messageReceived.chipRedMessage(response.getString("message"),
                                    CHANGE_NIP_WB);
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (error.networkResponse == null)
                {
                    messageReceived.chipRedError("Error al cambiar NIP\nNo responde " +
                            "servidor", CHANGE_NIP_WB);
                }
            }
        });

        req.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy
                .DEFAULT_BACKOFF_MULT));

        queue.add(req);
    }

    public void generateBillet(final String clientId, final String amount, final String deviceId,
                               boolean toAuth)
    {
        //Definir el errorResponse, ya que es comun
        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (error.networkResponse == null)
                {
                    messageReceived.chipRedError(activityContext.getResources().getString(R
                            .string.generateQrError) + activityContext.getResources().getString(R
                            .string.serverErrorMessage), GENERATE_WB);
                }
            }
        };

        if (toAuth)
        {
            validateAuthenticatedDevice(new Response.Listener<JSONObject>()
            {
                @Override
                public void onResponse(JSONObject response)
                {
                    try
                    {
                        if (!response.getString("response").contains(LOGOUT))
                        {
                            generateBillet(clientId, amount, deviceId, false);
                        }
                        else
                        {
                            Log.d("LogOut", "LogOut Request!");
                            messageReceived.chipRedMessage(LOGOUT, CHECK_DEVICE_WB);
                        }
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            }, errorListener);
        }
        else
        {
            if (queue == null)
            {
                queue = Volley.newRequestQueue(activityContext);
            }

            JSONObject request = new JSONObject();

            try
            {
                request.put("key", "generar_billete");
                request.put("id_cliente", clientId);
                request.put("denominacion", amount);
                request.put("origen", "ac_chipred");
                request.put("id_dispositivo", deviceId);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, URL, request, new
                    Response.Listener<JSONObject>()
                    {
                        @Override
                        public void onResponse(JSONObject response)
                        {
                            try
                            {
                                if (response.getString("response").contains("Error"))
                                {
                                    messageReceived.chipRedError(response.getString("message"),
                                            GENERATE_WB);
                                    VolleyLog.e("Error: ", response.getString("Error"));
                                }
                                else
                                {
                                    messageReceived.showResponse(response, GENERATE_WB);
                                }
                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }, errorListener);

            req.setRetryPolicy(new DefaultRetryPolicy(5000, 0, 0));
            queue.add(req);
        }
    }

    public void checkBalance(final String clientId, boolean toAuth)
    {
        //Definir el errorResponse, ya que es comun
        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (error.networkResponse == null)
                {
                    messageReceived.chipRedError(activityContext.getResources().getString(R
                            .string.checkBalanceError) + activityContext.getResources().getString
                            (R.string.serverErrorMessage), CHECK_BALANCE_WB);
                }
            }
        };

        if (toAuth)
        {
            validateAuthenticatedDevice(new Response.Listener<JSONObject>()
            {
                @Override
                public void onResponse(JSONObject response)
                {
                    try
                    {
                        if (!response.getString("response").contains(LOGOUT))
                        {
                            checkBalance(clientId, false);
                        }
                        else
                        {
                            Log.d("LogOut", "LogOut Request!");
                            messageReceived.chipRedMessage(LOGOUT, CHECK_DEVICE_WB);
                        }
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            }, errorListener);
        }
        else
        {
            if (queue == null)
            {
                queue = Volley.newRequestQueue(activityContext);
            }

            JSONObject request = new JSONObject();
            try
            {
                request.put("key", "cuentas_cliente");
                request.put("id_cliente", clientId);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, URL, request, new
                    Response.Listener<JSONObject>()
                    {
                        @Override
                        public void onResponse(JSONObject response)
                        {
                            try
                            {
                                if (response.getString("response").contains("Error"))
                                {
                                    messageReceived.chipRedError(response.getString("message"),
                                            CHECK_BALANCE_WB);
                                    VolleyLog.e("Error: ", response.getString("Error"));
                                }
                                else
                                {
                                    messageReceived.showResponse(response, CHECK_BALANCE_WB);
                                }
                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }, errorListener);

            req.setRetryPolicy(new DefaultRetryPolicy(7000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy
                    .DEFAULT_BACKOFF_MULT));
            queue.add(req);
        }
    }

    public void getGeneratedBillets(final String clientId, boolean toAuth)
    {
        //Definir el errorResponse, ya que es comun
        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (error.networkResponse == null)
                {
                    messageReceived.chipRedError(activityContext.getResources().getString(R
                            .string.getBilletsError) + activityContext.getResources().getString(R
                            .string.serverErrorMessage), GET_BILLETS_WB);
                }
            }
        };

        if (toAuth)
        {
            validateAuthenticatedDevice(new Response.Listener<JSONObject>()
            {
                @Override
                public void onResponse(JSONObject response)
                {
                    try
                    {
                        if (!response.getString("response").contains(LOGOUT))
                        {
                            getGeneratedBillets(clientId, false);
                        }
                        else
                        {
                            Log.d("LogOut", "LogOut Request!");
                            messageReceived.chipRedMessage(LOGOUT, CHECK_DEVICE_WB);
                        }
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            }, errorListener);
        }
        else
        {
            if (queue == null)
            {
                queue = Volley.newRequestQueue(activityContext);
            }

            JSONObject request = new JSONObject();

            try
            {
                request.put("key", "obtener_billetes_app");
                request.put("id_cliente", clientId);
                request.put("origen", "ac_chipred");
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, URL, request, new
                    Response.Listener<JSONObject>()
                    {
                        @Override
                        public void onResponse(JSONObject response)
                        {
                            try
                            {
                                if (response.getString("response").contains("Error"))
                                {
                                    messageReceived.chipRedError(response.getString("message"),
                                            GET_BILLETS_WB);
                                }
                                else
                                {
                                    messageReceived.showResponse(response, GET_BILLETS_WB);
                                }
                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }, errorListener);

            req.setRetryPolicy(new DefaultRetryPolicy(7000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy
                    .DEFAULT_BACKOFF_MULT)); //Valores en default

            queue.add(req);
        }
    }

    public void deleteBillet(final String clientId, final String billetId, boolean toAuth)
    {
        //Definir el errorResponse, ya que es comun
        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (error.networkResponse == null)
                {
                    messageReceived.chipRedError(activityContext.getResources().getString(R
                            .string.removeBilletError) + activityContext.getResources().getString
                            (R.string.serverErrorMessage), DELETE_BILLET_WB);
                }
            }
        };

        if (toAuth)
        {
            validateAuthenticatedDevice(new Response.Listener<JSONObject>()
            {
                @Override
                public void onResponse(JSONObject response)
                {
                    try
                    {
                        if (!response.getString("response").contains(LOGOUT))
                        {
                            deleteBillet(clientId, billetId, false);
                        }
                        else
                        {
                            Log.d("LogOut", "LogOut Request!");
                            messageReceived.chipRedMessage(LOGOUT, CHECK_DEVICE_WB);
                        }
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            }, errorListener);
        }
        else
        {
            if (queue == null)
            {
                queue = Volley.newRequestQueue(activityContext);
            }

            JSONObject request = new JSONObject();

            try
            {
                request.put("key", "cancelar_billete");
                request.put("id_cliente", clientId);
                request.put("id_billete", billetId);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, URL, request, new
                    Response.Listener<JSONObject>()
                    {
                        @Override
                        public void onResponse(JSONObject response)
                        {
                            try
                            {
                                if (response.getString("response").contains("Error"))
                                {
                                    messageReceived.chipRedError(response.getString("message"),
                                            DELETE_BILLET_WB);
                                }
                                else
                                {
                                    messageReceived.chipRedMessage("Billete cancelado",
                                            DELETE_BILLET_WB);
                                }
                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }, errorListener);

            req.setRetryPolicy(new DefaultRetryPolicy(7000, 0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)); //Valores en default

            queue.add(req);
        }
    }

    public void askPurchases(final String clientId, final Date startDate, final Date endDate,
                             boolean toAuth)
    {
        //Definir el errorResponse, ya que es comun
        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (error.networkResponse == null)
                {
                    messageReceived.chipRedError(activityContext.getResources().getString(R
                            .string.purchasesError) + activityContext.getResources().getString(R
                            .string.serverErrorMessage), ASK_PURCHASES_WB);
                }
            }
        };

        if (toAuth)
        {
            validateAuthenticatedDevice(new Response.Listener<JSONObject>()
            {
                @Override
                public void onResponse(JSONObject response)
                {
                    try
                    {
                        if (!response.getString("response").contains(LOGOUT))
                        {
                            askPurchases(clientId, startDate, endDate, false);
                        }
                        else
                        {
                            Log.d("LogOut", "LogOut Request!");
                            messageReceived.chipRedMessage(LOGOUT, CHECK_DEVICE_WB);
                        }
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            }, errorListener);
        }
        else
        {
            //yyyy-mm-dd hh:mm:ss
            SimpleDateFormat chipReddf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            String startDateString = chipReddf.format(startDate) + " 00:00:00";
            String endDateString = chipReddf.format(endDate) + " 23:59:59";

            if (queue == null)
            {
                queue = Volley.newRequestQueue(activityContext);
            }

            JSONObject request = new JSONObject();
            try
            {
                request.put("key", "consultar_consumos");
                request.put("id_cliente", clientId);
                request.put("fecha_inicial", startDateString);
                request.put("fecha_final", endDateString);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, URL, request, new
                    Response.Listener<JSONObject>()
                    {
                        @Override
                        public void onResponse(JSONObject response)
                        {
                            try
                            {
                                if (response.getString("response").contains("Error"))
                                {
                                    messageReceived.chipRedError(response.getString("message"),
                                            ASK_PURCHASES_WB);
                                    VolleyLog.e("Error: ", response.getString("Error"));
                                }
                                else if (response.getString("response").contains("Aviso"))
                                {
                                    messageReceived.chipRedMessage(response.getString("message"),
                                            ASK_PURCHASES_WB);
                                }
                                else
                                {
                                    messageReceived.showResponse(response, ASK_PURCHASES_WB);
                                }
                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }, errorListener);

            req.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy
                    .DEFAULT_BACKOFF_MULT));
            queue.add(req);
        }
    }

    public void askPayments(final String clientId, boolean toAuth)
    {
        //Definir el errorResponse, ya que es comun
        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (error.networkResponse == null)
                {
                    messageReceived.chipRedError(activityContext.getResources().getString(R
                            .string.paymentsError) + activityContext.getResources().getString(R
                            .string.serverErrorMessage), ASK_PAYMENTS_WB);
                }
            }
        };

        if (toAuth)
        {
            validateAuthenticatedDevice(new Response.Listener<JSONObject>()
            {
                @Override
                public void onResponse(JSONObject response)
                {
                    try
                    {
                        if (!response.getString("response").contains(LOGOUT))
                        {
                            askPayments(clientId, false);
                        }
                        else
                        {
                            Log.d("LogOut", "LogOut Request!");
                            messageReceived.chipRedMessage(LOGOUT, CHECK_DEVICE_WB);
                        }
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            }, errorListener);
        }
        else
        {
            if (queue == null)
            {
                queue = Volley.newRequestQueue(activityContext);
            }

            JSONObject request = new JSONObject();
            try
            {
                request.put("key", "consultar_ultimos_abonos");
                request.put("id_cliente", clientId);
                request.put("cantidad", "10"); //10 abonos por default
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, URL, request, new
                    Response.Listener<JSONObject>()
                    {
                        @Override
                        public void onResponse(JSONObject response)
                        {
                            try
                            {
                                if (response.getString("response").contains("Error"))
                                {
                                    messageReceived.chipRedError(response.getString("message"),
                                            ASK_PAYMENTS_WB);
                                    VolleyLog.e("Error: ", response.getString("Error"));
                                }
                                else
                                {
                                    messageReceived.showResponse(response, ASK_PAYMENTS_WB);
                                }
                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }, errorListener);

            req.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy
                    .DEFAULT_BACKOFF_MULT)); //Timeout de 8 segundos
            queue.add(req);
        }
    }

    public void getTransactions(final Date startDate, final Date endDate,
                                boolean toAuth)
    {
        //Definir el errorResponse, ya que es comun
        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (error.networkResponse == null)
                {
                    messageReceived.chipRedError(activityContext.getResources().getString(R
                            .string.purchasesError) + activityContext.getResources().getString(R
                            .string.serverErrorMessage), GET_ACCOUNT_TRANSACTIONS);
                }
            }
        };

        if (toAuth)
        {
            validateAuthenticatedDevice(new Response.Listener<JSONObject>()
            {
                @Override
                public void onResponse(JSONObject response)
                {
                    try
                    {
                        if (!response.getString("response").contains(LOGOUT))
                        {
                            getTransactions(startDate, endDate, false);
                        }
                        else
                        {
                            Log.d("LogOut", "LogOut Request!");
                            messageReceived.chipRedMessage(LOGOUT, CHECK_DEVICE_WB);
                        }
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            }, errorListener);
        }
        else
        {
            //yyyy-mm-dd hh:mm:ss
            SimpleDateFormat chipReddf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            String startDateString = chipReddf.format(startDate) + " 00:00:00";
            String endDateString = chipReddf.format(endDate) + " 23:59:59";

            if (queue == null)
            {
                queue = Volley.newRequestQueue(activityContext);
            }

            JSONObject request = new JSONObject();
            try
            {
                request.put("key", "consultar_movimientos_cliente");
                request.put("id_cliente", clientId);
                request.put("fecha_inicial", startDateString);
                request.put("fecha_final", endDateString);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, URL, request, new
                    Response.Listener<JSONObject>()
                    {
                        @Override
                        public void onResponse(JSONObject response)
                        {
                            try
                            {
                                if (response.getString("response").contains("Error"))
                                {
                                    messageReceived.chipRedError(response.getString("message"),
                                            GET_ACCOUNT_TRANSACTIONS);
                                    VolleyLog.e("Error: ", response.getString("Error"));
                                }
                                else if (response.getString("response").contains("Aviso"))
                                {
                                    messageReceived.chipRedMessage(response.getString("message"),
                                            GET_ACCOUNT_TRANSACTIONS);
                                }
                                else
                                {
                                    messageReceived.showResponse(response,
                                            GET_ACCOUNT_TRANSACTIONS);
                                }
                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }, errorListener);

            req.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy
                    .DEFAULT_BACKOFF_MULT));
            queue.add(req);
        }
    }

    public void getInvoices(String startDate, String endDate, int offset, String stationNumber)
    {
        //Definir el errorResponse, ya que es comun
        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (error.networkResponse == null)
                {
                    messageReceived.chipRedError(activityContext.getResources().getString(R
                            .string.invoicesError) + activityContext.getResources().getString(R
                            .string.serverErrorMessage), GET_INVOICES);
                }
            }
        };

        if (queue == null)
        {
            queue = Volley.newRequestQueue(activityContext);
        }

        JSONObject request = new JSONObject();
        try
        {
            request.put("key", "consultar_comprobantes_electronicos_cliente");
            request.put("id_cliente", clientId);
            request.put("pagina", offset);
            if (!startDate.isEmpty())
                request.put("fecha_inicial", startDate); //yyyy-mm-dd hh:mm:ss
            if (!endDate.isEmpty())
                request.put("fecha_final", endDate); //yyyy-mm-dd hh:mm:ss
            if (!stationNumber.isEmpty())
                request.put("numero_estacion", stationNumber);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Log.d("ChipRedManager", request.toString());

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, URL, request, new
                Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        try
                        {
                            if (response.getString("response").contains("Ok"))
                            {
                                messageReceived.showResponse(response, GET_INVOICES);
                            }
                            else if (response.getString("response").contains("Aviso"))
                            {
                                messageReceived.chipRedMessage(response.getString("message"),
                                        GET_INVOICES);
                            }
                            else
                            {
                                messageReceived.chipRedError(response.getString("message"),
                                        GET_INVOICES);
                            }
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }, errorListener);

        req.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy
                .DEFAULT_BACKOFF_MULT));

        queue.add(req);
    }

    public void getHaciendaMessage(String invoiceKey, String purchaseStationId)
    {
        //Definir el errorResponse, ya que es comun
        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (error.networkResponse == null)
                {
                    messageReceived.chipRedError(activityContext.getResources().getString(R
                            .string.haciendaMessageError) + activityContext.getResources().getString(R
                            .string.serverErrorMessage), GET_HACIENDA_MESSAGE);
                }
            }
        };

        if (queue == null)
        {
            queue = Volley.newRequestQueue(activityContext);
        }

        JSONObject request = new JSONObject();
        try
        {
            request.put("key", "consultar_mensaje_acerec");
            request.put("clave", invoiceKey);
            request.put("id_estacion", purchaseStationId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, URL, request, new
                Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        try
                        {
                            if (response.getString("response").contains("Ok"))
                            {
                                messageReceived.showResponse(response, GET_HACIENDA_MESSAGE);
                            }
                            else if (response.getString("response").contains("Aviso"))
                            {
                                messageReceived.chipRedMessage(response.getString("message"),
                                        GET_HACIENDA_MESSAGE);
                            }
                            else
                            {
                                messageReceived.chipRedError(response.getString("message"),
                                        GET_HACIENDA_MESSAGE);
                            }
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }, errorListener);

        req.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy
                .DEFAULT_BACKOFF_MULT));

        queue.add(req);
    }

    public void getStations()
    {
        //Definir el errorResponse, ya que es comun
        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (error.networkResponse == null)
                {
                    messageReceived.chipRedError(activityContext.getResources().getString(R
                            .string.stationsError) + activityContext.getResources().getString(R
                            .string.serverErrorMessage), GET_STATIONS);
                }
            }
        };

        if (queue == null)
        {
            queue = Volley.newRequestQueue(activityContext);
        }

        JSONObject request = new JSONObject();
        try
        {
            request.put("key", "obtener_estaciones");
            request.put("pais", "costa rica");
            request.put("consultar_facturas_clientes", true);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, URL, request, new
                Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        try
                        {
                            if (response.getString("response").contains("Ok"))
                            {
                                messageReceived.showResponse(response, GET_STATIONS);
                            }
                            else if (response.getString("response").contains("Aviso"))
                            {
                                messageReceived.chipRedMessage(response.getString("message"),
                                        GET_STATIONS);
                            }
                            else
                            {
                                messageReceived.chipRedError(response.getString("message"),
                                        GET_STATIONS);
                            }
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }, errorListener);

        req.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy
                .DEFAULT_BACKOFF_MULT));

        queue.add(req);
    }

    public void testConnection()
    {
        if (queue == null)
        {
            queue = Volley.newRequestQueue(activityContext);
        }

        JSONObject request = new JSONObject();

        try
        {
            request.put("key", "prueba_conexion");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, URL, request, new
                Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        messageReceived.chipRedMessage("Conexión exitosa", TEST_CONNECTION_WB);
                    }
                }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (error.networkResponse == null)
                {
                    messageReceived.chipRedError("Error al cerrar sesión", TEST_CONNECTION_WB);
                }
            }
        });

        req.setRetryPolicy(new DefaultRetryPolicy(7000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(req);
    }

    public void signUp(HashMap<String, String> signUpData)
    {
        if (queue == null)
        {
            queue = Volley.newRequestQueue(activityContext);
        }

        JSONObject request = new JSONObject();

        try
        {
            request.put("key", "tablet_alta_cliente");
            request.put("nombre", signUpData.get("nombre"));
            request.put("pais", "mexico");
            request.put("clave_fiscal", signUpData.get("clave_fiscal"));
            request.put("telefono", signUpData.get("telefono"));
            request.put("email", signUpData.get("email"));
            request.put("direccion", signUpData.get("direccion"));
            request.put("observaciones", "");
            request.put("combustible_billetes", "0");
            request.put("estatus", "PE");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, URL, request, new
                Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        try
                        {
                            if (response.getString("response").contains("Ok"))
                            {
                                messageReceived.chipRedMessage("Registrado",
                                        ADD_OR_MODIFY_CLIENT_WB);
                            }
                            else if (response.getString("response").contains("Aviso"))
                            {
                                messageReceived.chipRedMessage(response.getString("message"),
                                        ADD_OR_MODIFY_CLIENT_WB);
                            }
                            else
                            {
                                messageReceived.chipRedError("Error del servidor al registrar " +
                                                "cliente",
                                        ADD_OR_MODIFY_CLIENT_WB);
                            }
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (error.networkResponse == null)
                {
                    messageReceived.chipRedError("Error al registrar cliente\nNo responde " +
                            "servidor", ADD_OR_MODIFY_CLIENT_WB);
                }
            }
        });

        req.setRetryPolicy(new DefaultRetryPolicy(7000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(req);
    }

    public void addOrModifyClient(JSONObject request, final boolean add, String clientId)
    {
        final String keyWord;
        if (add)
        {
            keyWord = "registrar";
        }
        else
        {
            keyWord = "modificar";
        }

        if (queue == null)
        {
            queue = Volley.newRequestQueue(activityContext);
        }

        try
        {
            if (add)
            {
                request.put("key", "alta_cliente");
            }
            else
            {
                request.put("key", "modificar_datos_cliente");
                request.put("id_cliente", clientId);
            }
            request.put("combustible_billetes", "0");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, URL, request, new
                Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        Log.d("CRresponse", response.toString());
                        try
                        {
                            if (response.getString("response").contains("Ok"))
                            {
                                if (add)
                                {
                                    messageReceived.chipRedMessage("Registrado",
                                            ADD_OR_MODIFY_CLIENT_WB);
                                }
                                else
                                {
                                    messageReceived.chipRedMessage("Modificado",
                                            ADD_OR_MODIFY_CLIENT_WB);
                                }
                            }
                            else if (response.getString("response").contains("Aviso"))
                            {
                                messageReceived.chipRedMessage(response.getString("message"),
                                        ADD_OR_MODIFY_CLIENT_WB);
                            }
                            else
                            {
                                messageReceived.chipRedError(response.getString("message"),
                                        ADD_OR_MODIFY_CLIENT_WB);
                            }
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (error.networkResponse == null)
                {
                    messageReceived.chipRedError("Error al " + keyWord + " cliente\nNo responde " +
                            "servidor", ADD_OR_MODIFY_CLIENT_WB);
                }
            }
        });

        req.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy
                .DEFAULT_BACKOFF_MULT));

        queue.add(req);
    }

    public void searchClient(int key, String value)
    {
        String attributeToSearch = "id";

        switch (key)
        {
            case BYNAME:
                attributeToSearch = "nombre";
                break;

            case BYEMAIL:
                attributeToSearch = "email";
                break;

            case BYID:
                attributeToSearch = "id_cliente";
                break;

            case BYTAXCODE:
                attributeToSearch = "cedula";
                break;

            case BYPHONE:
                attributeToSearch = "telefono";
                break;
        }

        if (queue == null)
        {
            queue = Volley.newRequestQueue(activityContext);
        }

        final JSONObject request = new JSONObject();

        try
        {
            request.put("key", "buscar_cliente");
            request.put(attributeToSearch, value);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, URL, request, new
                Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        try
                        {
                            if (response.getString("response").contains("Ok"))
                            {
                                messageReceived.showResponse(response, SEARCH_CLIENT_WB);
                            }
                            else if (response.getString("response").contains("Aviso"))
                            {
                                messageReceived.chipRedMessage(response.getString("message"),
                                        SEARCH_CLIENT_WB);
                            }
                            else
                            {
                                messageReceived.chipRedError("Error del servidor al modificar " +
                                        "información" +
                                        " de cliente", SEARCH_CLIENT_WB);
                            }
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (error.networkResponse == null)
                {
                    messageReceived.chipRedError("Error al modificar información de cliente\nNo " +
                            "responde servidor", SEARCH_CLIENT_WB);
                }
            }
        });

        req.setRetryPolicy(new DefaultRetryPolicy(7000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(req);
    }

    public void resendConfirmationEmail(String clientId)
    {
        if (queue == null)
        {
            queue = Volley.newRequestQueue(activityContext);
        }

        final JSONObject request = new JSONObject();

        try
        {
            request.put("key", "reenviar_correo_confirmacion");
            request.put("id_cliente", clientId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, URL, request, new
                Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        try
                        {
                            if (response.getString("response").contains("Ok"))
                            {
                                messageReceived.chipRedMessage(response.getString("message"),
                                        RESEND_CONFIRMATION_EMAIL);
                            }
                            else
                            {
                                messageReceived.chipRedError(response.getString("message"),
                                        RESEND_CONFIRMATION_EMAIL);
                                VolleyLog.e("Error: ", response.getString("Error"));
                            }
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (error.networkResponse == null)
                {
                    messageReceived.chipRedError("Error al solicitar reenvío de corre de " +
                            "confirmación\nNo responde " +
                            "servidor", RESEND_CONFIRMATION_EMAIL);
                }
            }
        });

        req.setRetryPolicy(new DefaultRetryPolicy(20000, 0, DefaultRetryPolicy
                .DEFAULT_BACKOFF_MULT));

        queue.add(req);
    }

    public interface OnMessageReceived
    {
        void chipRedMessage(String crMessage, int webServiceN);

        void chipRedError(String errorMessage, int webServiceN);

        void showResponse(JSONObject response, int webServiceN);
    }
}