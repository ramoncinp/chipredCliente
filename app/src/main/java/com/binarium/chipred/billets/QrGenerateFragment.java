package com.binarium.chipred.billets;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.binarium.chipred.ChipREDConstants;
import com.binarium.chipred.ChipRedManager;
import com.binarium.chipred.GenericDialog;
import com.binarium.chipred.MainActivity;
import com.binarium.chipred.R;
import com.binarium.chipred.SharedPreferencesManager;
import com.binarium.chipred.Tutorial3;
import com.binarium.chipred.auth.Authenticator;
import com.jaredrummler.android.device.DeviceName;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;
import org.json.JSONException;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;

import static android.app.Activity.RESULT_OK;
import static com.binarium.chipred.ChipREDConstants.REQUEST_CODE_CONFIRM_DEVICE_CREDENTIALS;
import static com.binarium.chipred.MainActivity.DEVICEUSER;

public class QrGenerateFragment extends Fragment
{
    private ImageView imageView;
    private EditText textToEncode;
    private Display display;
    private Button generateQrButton;
    private Button billetListButton;
    private ProgressDialog mainProgressDialog;
    private TextView noAuth;

    private int QRcodeWidth;
    private int maxAmount, minAmount;
    private String currentAmount, currencySymbol;

    private Context context;

    private ChipRedManager chipRedManager;

    private SharedPreferencesManager sharedPreferencesManager;

    private String idClient;
    private String country;

    private Handler indicateOptionHandler;
    private Runnable indicateMenuItem = new Runnable()
    {
        @Override
        public void run()
        {
            if (getActivity() != null)
            {
                Menu menu = ((MainActivity) getActivity()).navigationView.getMenu();
                MenuItem menuItem = menu.findItem(R.id.listpurchases);

                boolean currentState = menuItem.isChecked();
                menuItem.setChecked(!currentState);

                indicateOptionHandler.postDelayed(indicateMenuItem, 500);
            }
        }
    };

    public QrGenerateFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle
            savedInstanceState)
    {
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ((MainActivity) getActivity()).setActionBarTitle(getActivity().getResources().getString(R
                .string.generateBilletNvItem));

        setRetainInstance(true);
        setHasOptionsMenu(true);

        context = getActivity().getBaseContext();
        display = getActivity().getWindowManager().getDefaultDisplay();
        mainProgressDialog = ((MainActivity) getActivity()).progressDialog;

        getDisplayDims();

        View v = inflater.inflate(R.layout.fragment_qr_generate, container, false);
        imageView = (ImageView) v.findViewById(R.id.qrCode);
        textToEncode = (EditText) v.findViewById(R.id.codetoencode);
        generateQrButton = (Button) v.findViewById(R.id.generate_qr);
        noAuth = v.findViewById(R.id.auth_error);
        TextView currencyTv = v.findViewById(R.id.billet_currency);

        generateQrButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                generateQr();
            }
        });
        generateQrButton.setBackground(getResources().getDrawable(R.drawable
                .round_button_color_primary));
        billetListButton = (Button) v.findViewById(R.id.goToBilletList);
        billetListButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Setear el item del menu correspondiente a lista de billetes
                NavigationView navigationView = ((MainActivity) getActivity()).navigationView;
                navigationView.getMenu().getItem(1).setChecked(true);

                final FragmentTransaction ft = getFragmentManager()
                        .beginTransaction();

                ft.replace(R.id.fragment_container, new BilletListFragment(), "BilletListFragment");
                ft.commit();
            }
        });

        sharedPreferencesManager = new SharedPreferencesManager(context);

        idClient = sharedPreferencesManager.getStringFromSP("id_cliente", "");
        country = sharedPreferencesManager.getStringFromSP("pais", "");
        if (country.equals("mexico"))
        {
            currencyTv.setText("$");
            currencySymbol = "$";
            maxAmount = 10000;
            minAmount = 10;
        }
        else
        {
            currencyTv.setText("₡");
            currencySymbol = "₡";
            maxAmount = 300000;
            minAmount = 600;
        }

        indicateOptionHandler = new Handler();
        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived()
        {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN)
            {
                //Toast.makeText(context, crMessage, Toast.LENGTH_LONG).show();
                if (crMessage.equals(ChipRedManager.LOGOUT))
                {
                    ((MainActivity) getActivity()).logOut(true);
                }
                else
                {
                    showSnackBar(crMessage);
                }
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN)
            {
                if (mainProgressDialog != null)
                {
                    mainProgressDialog.dismiss();
                }

                GenericDialog genericDialog = new GenericDialog(context.getResources().getString
                        (R.string.errorConexionTitulo), errorMessage, new Runnable()
                {
                    @Override
                    public void run()
                    {
                        //nada...
                    }
                }, null, getActivity());
                genericDialog.show();
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN)
            {
                getBilletInfo(response);
            }
        }, context);

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        if (!createAuthenticator())
        {
            showKeyboard(getActivity());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == REQUEST_CODE_CONFIRM_DEVICE_CREDENTIALS)
        {
            if (resultCode == RESULT_OK)
            {
                showKeyboard(getActivity());
            }
            else
            {
                setNoAuthViews();
            }
        }
    }

    private boolean createAuthenticator()
    {
        Authenticator authenticator = new Authenticator(this, new Authenticator
                .AuthenticatorInteface()
        {
            @Override
            public void onAuthenticated()
            {
                showKeyboard(getActivity());
            }

            @Override
            public void onCancelledAuthentication()
            {
                setNoAuthViews();
            }
        });

        return authenticator.excecuteAuth();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.fragment_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.help:
                hideKeyboard();
                Intent intent = new Intent(getActivity(), Tutorial3.class);
                startActivity(intent);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void setNoAuthViews()
    {
        LinearLayout content = getActivity().findViewById(R.id.generate_qr_content);
        content.setVisibility(View.GONE);
        noAuth.setVisibility(View.VISIBLE);
    }

    private void getDisplayDims()
    {
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);
        QRcodeWidth = outMetrics.widthPixels;
    }

    public void generateQr()
    {
        final Double numericAmount;
        boolean moreThanTwoDecimals = false;
        boolean success = false;
        final String idClient = sharedPreferencesManager.getStringFromSP("id_cliente", "");

        if (generateQrButton.getText().toString().toLowerCase().contains("generar nuevo billete"))
        {
            reloadViews();
        }
        else
        {
            mainProgressDialog = ProgressDialog.show(getActivity(), "", "Generando Billete...",
                    true, false);
            mainProgressDialog.show();

            currentAmount = textToEncode.getText().toString();

            try
            {
                if (!currentAmount.isEmpty() && currentAmount.length() < 8)
                {
                    numericAmount = Double.parseDouble(currentAmount);
                    if (currentAmount.contains("."))
                    {
                        //Checar si tiene mas de dos decimales
                        if (currentAmount.substring(currentAmount.indexOf(".") + 1).length() > 2)
                        {
                            moreThanTwoDecimals = true;
                        }
                    }

                    if (numericAmount == 0 || numericAmount >= maxAmount || moreThanTwoDecimals)
                    {
                        //Toast.makeText(context, "Valor inválido", Toast.LENGTH_SHORT).show();
                        showSnackBar("Valor inválido");
                    }
                    else
                    {
                        if (numericAmount < minAmount)
                        {
                            String stringValue = currencySymbol + String.valueOf(minAmount);
                            showSnackBar("Valor inválido\n" + "La denominación de billete mínima " +
                                    "" + "" + "es de " + stringValue);
                        }
                        else
                        {
                            currentAmount = currentAmount.replaceFirst("^0+(?!$)", "");
                            chipRedManager.generateBillet(idClient, currentAmount,
                                    getDeviceId(), true);
                            success = true;
                        }
                    }
                }
                else
                {
                    showSnackBar("Valor inválido");
                }
            }
            catch (IndexOutOfBoundsException e)
            {
                e.printStackTrace();
                showSnackBar("Valor inválido");
            }
            catch (NumberFormatException e)
            {
                e.printStackTrace();
                showSnackBar("Valor inválido");
            }

            if (!success)
            {
                View kb = ((MainActivity) getActivity()).getCurrentFocus();
                if (kb != null)
                {
                    InputMethodManager imm = (InputMethodManager) (context.getSystemService
                            (Context.INPUT_METHOD_SERVICE));
                    imm.hideSoftInputFromWindow(kb.getWindowToken(), 0);
                }

                textToEncode.setText("");

                if (mainProgressDialog != null)
                {
                    mainProgressDialog.dismiss();
                }
            }
        }
    }

    public void qrGenerated(Billet billet)
    {
        //Mostrar QR generado
        Picasso.with(getContext()).load(new File(billet.getPath())).into(imageView);

        if (mainProgressDialog != null)
        {
            mainProgressDialog.dismiss();
        }
        hideKeyboard();

        generateQrButton.setText("Generar nuevo billete");
        generateQrButton.setBackground(getResources().getDrawable(R.drawable
                .round_button_color_accent));
        generateQrButton.setTextColor(getResources().getColor(R.color.white));

        imageView.setVisibility(View.VISIBLE);
        billetListButton.setVisibility(View.VISIBLE);

        showSnackBar("Billete generado y almacenado");
    }

    public void hideKeyboard()
    {
        try
        {
            View kb = ((MainActivity) getActivity()).getCurrentFocus();
            InputMethodManager imm = (InputMethodManager) (context.getSystemService(Context
                    .INPUT_METHOD_SERVICE));
            if (imm != null && kb != null)
            {
                imm.hideSoftInputFromWindow(kb.getWindowToken(), 0);
            }
        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
        }
    }

    private void getBilletInfo(JSONObject response)
    {
        JSONObject billetInfo;
        try
        {
            //Obtener informacion de billete
            billetInfo = response.getJSONObject("data");

            //Crear objeto JSON para almacenar
            JSONObject mBillet = new JSONObject();
            mBillet.put("folio", billetInfo.get("folio"));
            mBillet.put("estatus", billetInfo.get("estatus"));
            mBillet.put("fecha_alta", ChipREDConstants.jsonDateFormat.format(new Date()));
            mBillet.put("fecha_uso", "00/00/00 00:00");
            mBillet.put("monto_uso", billetInfo.get("monto_uso"));
            mBillet.put("id_dispositivo", billetInfo.get("id_dispositivo"));
            mBillet.put("monto", billetInfo.get("monto"));
            mBillet.put("id", billetInfo.get("_id"));

            //Crear billete
            Billet billet = new Billet(mBillet, getContext());

            //Guardar informacion de billete
            BilletFileManager.addBilletToDocument(mBillet, getContext());

            //Generar su respectivo código QR
            BilletQRManager qrManager = new BilletQRManager(new WeakReference<>(getContext()),
                    QRcodeWidth, idClient);

            qrManager.setmInterface(new BilletQRManager.BilletQRManagerInterface()
            {
                @Override
                public void onBilletsCreated(ArrayList<Billet> billets)
                {
                    //Siempre es uno
                    qrGenerated(billets.get(0));
                }
            });

            ArrayList<Billet> billetsToCreate = new ArrayList<>();
            billetsToCreate.add(billet);
            qrManager.execute(billetsToCreate);
        }
        catch (JSONException e)
        {
            if (mainProgressDialog != null)
            {
                mainProgressDialog.dismiss();
            }

            e.printStackTrace();
            GenericDialog genericDialog = new GenericDialog("Error", "Hubo un error al procesar "
                    + "la información", null, null, getActivity());
            genericDialog.show();
        }
    }

    public void reloadViews()
    {
        showKeyboard(getActivity());

        imageView.setImageBitmap(null);
        textToEncode.setText("");

        generateQrButton.setText("Generar");
        generateQrButton.setBackground(getResources().getDrawable(R.drawable
                .round_button_color_primary));
        generateQrButton.setTextColor(getResources().getColor(R.color.white));

        billetListButton.setVisibility(View.GONE);
        imageView.setVisibility(View.GONE);
    }

    private String getDeviceId()
    {
        StringBuilder appInfo = new StringBuilder();

        try
        {
            DeviceName.DeviceInfo info = DeviceName.getDeviceInfo(context);
            appInfo.append(info.manufacturer);
            appInfo.append(" ");
            appInfo.append(info.marketName);

            if (sharedPreferencesManager.isContained(DEVICEUSER))
            {
                appInfo.append(" de ");
                appInfo.append(sharedPreferencesManager.getStringFromSP(DEVICEUSER, ""));
            }
        }
        catch (Exception e)
        {
            appInfo.append("Desconocido");
        }

        return appInfo.toString();
    }

    public static void showKeyboard(Context context)
    {
        try
        {
            ((InputMethodManager) (context).getSystemService(Context.INPUT_METHOD_SERVICE))
                    .toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager
                            .HIDE_IMPLICIT_ONLY);
        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
        }
    }

    private void showSnackBar(String message)
    {
        Snackbar.make(getActivity().findViewById(android.R.id.content), message, Snackbar
                .LENGTH_SHORT).show();
    }
}
