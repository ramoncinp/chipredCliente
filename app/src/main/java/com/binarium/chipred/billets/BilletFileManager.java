package com.binarium.chipred.billets;

import android.content.Context;
import android.util.Log;

import com.binarium.chipred.ChipREDConstants;
import com.binarium.chipred.SharedPreferencesManager;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class BilletFileManager
{
    private static final String TAG = BilletFileManager.class.getSimpleName();

    public static boolean saveBilletsDocument(Context context, JSONArray billets)
    {
        //Obtener el path
        String filePath = getBilletsDocumentPath(context);

        //Obtener archivo
        File billetsFile = new File(filePath);

        //Guardar documento json
        try
        {
            Writer output = new BufferedWriter(new FileWriter(billetsFile));
            output.write(billets.toString());
            output.close();
            return true;
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    public static JSONArray getBilletsDocument(Context context)
    {
        //Obtener el path
        String filePath = getBilletsDocumentPath(context);

        //Obtener archivo
        File billetsFile = new File(filePath);
        //Validar que exista
        if (!billetsFile.exists())
        {
            //No existe el archivo, regresar un arreglo vacío
            return new JSONArray();
        }
        else
        {
            //Leer archivo
            StringBuilder stringBuilder = new StringBuilder();
            try
            {
                BufferedReader br = new BufferedReader(new FileReader(billetsFile));
                String line;

                while ((line = br.readLine()) != null)
                {
                    stringBuilder.append(line);
                }

                br.close();

                //Convertir contenido a JSON
                return new JSONArray(stringBuilder.toString());
            }
            catch (IOException | JSONException e)
            {
                e.printStackTrace();
            }
        }

        return null;
    }

    public static void addBilletToDocument(JSONObject billet, Context context)
    {
        //Obtener documento
        JSONArray document = getBilletsDocument(context);

        if (document == null)
        {
            document = new JSONArray();
            document.put(billet);
        }
        else
        {
            //Agregar billete
            document.put(billet);
        }

        //Guardar cambios
        saveBilletsDocument(context, document);
    }

    public static boolean removeAllBillets(Context context)
    {
        //Obtener el path
        String path = context.getFilesDir() + ChipREDConstants.BILLETS_PATH;

        //Validar que exista el archivo
        File file = new File(path);
        if (file.isDirectory())
        {
            try
            {
                FileUtils.deleteDirectory(file);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        return false;
    }

    private static String getBilletsDocumentPath(Context context)
    {
        //Validar contexto
        if (context == null) return null;

        //Obtener id de cliente
        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(context);
        String idCliente = sharedPreferencesManager.getStringFromSP("id_cliente", "");

        //Si no se puede obtener el id de cliente, retornar nulo
        if (idCliente.equals("")) return null;

        //Validar que exista el directorio
        String path = context.getFilesDir() + ChipREDConstants.BILLETS_PATH;
        File dir = new File(path);
        if (!dir.exists())
        {
            //Crear directorio
            if (!dir.mkdir())
            {
                Log.e(TAG, "No se pudo crear el directorio con idCliente");
            }
        }

        //Retornar ruta del archivo
        return path + "billets.json";
    }

    public static String getQrPath(Context context, String billetId)
    {
        //Validar contexto
        if (context == null) return null;

        //Obtener id de cliente
        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(context);
        String idCliente = sharedPreferencesManager.getStringFromSP("id_cliente", "");

        //Si no se puede obtener el id de cliente, retornar nulo
        if (idCliente.equals("")) return null;

        //Validar que exista el directorio
        String dirPath = context.getFilesDir() + ChipREDConstants.BILLETS_QR_CODES_PATH;
        File dir = new File(dirPath);
        if (!dir.exists())
        {
            //Crear directorio
            if (!dir.mkdir())
            {
                Log.e(TAG, "No se pudo crear el directorio para qr's de cliente");
                return null;
            }
        }

        //Retornar ruta del archivo
        String filePath = dirPath + billetId + ".png";
        File qr = new File(filePath);
        if (qr.exists())
        {
            return filePath;
        }
        else
        {
            //No existe el archivo, regresar vacío
            return "";
        }
    }

    public static Boolean removeQr(Context context, String billetId)
    {
        //Validar contexto
        if (context == null) return null;

        //Obtener id de cliente
        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(context);
        String idCliente = sharedPreferencesManager.getStringFromSP("id_cliente", "");

        //Si no se puede obtener el id de cliente, retornar nulo
        if (idCliente.equals("")) return null;

        //Validar que exista el directorio
        String dirPath =
                context.getFilesDir() + ChipREDConstants.BILLETS_QR_CODES_PATH + billetId + ".png";
        File qrFile = new File(dirPath);
        if (qrFile.exists())
        {
            return qrFile.delete();
        }

        return null;
    }
}
