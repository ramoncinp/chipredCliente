package com.binarium.chipred.billets;

import android.animation.ObjectAnimator;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.binarium.chipred.R;
import com.zaihuishou.expandablerecycleradapter.viewholder.AbstractExpandableAdapterItem;

public class BilletUserItem extends AbstractExpandableAdapterItem implements View.OnLongClickListener
{
    private TextView userName;
    private ImageView arrow;
    private BilletUser billetUser;
    private TextView userDevice;

    private View.OnLongClickListener longClickListener;

    @Override
    public int getLayoutResId()
    {
        return R.layout.billet_user_layout;
    }

    @Override
    public void onBindViews(final View root)
    {
        /**
         * control item expand and unexpand
         */
        root.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                doExpandOrUnexpand();
            }
        });

        userName = root.findViewById(R.id.billetUser);
        userDevice = root.findViewById(R.id.userDevice);
        arrow = root.findViewById(R.id.indicator);
    }

    @Override
    public void onExpansionToggled(boolean expanded)
    {
        float start, target;
        if (expanded)
        {
            start = 0f;
            target = 90f;
        }
        else
        {
            start = 90f;
            target = 0f;
        }
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(arrow, View.ROTATION, start, target);
        objectAnimator.setDuration(300);
        objectAnimator.start();
    }

    @Override
    public void onSetViews()
    {
        arrow.setImageResource(0);
        arrow.setImageResource(R.drawable.ic_arrow_drop_down_black_24dp);
    }

    @Override
    public void onUpdateViews(Object model, int position)
    {
        super.onUpdateViews(model, position);
        onSetViews();
        onExpansionToggled(getExpandableListItem().isExpanded());
        billetUser = (BilletUser) model;
        userName.setText(billetUser.getUserName());
        userDevice.setText(billetUser.getDeviceName());
    }

    public void setLongClickListener (View.OnLongClickListener longClickListener)
    {
        this.longClickListener = longClickListener;
    }

    @Override
    public boolean onLongClick(View v)
    {
        if (longClickListener != null)
        {
            longClickListener.onLongClick(v);
        }
        return true;
    }

    public BilletUser getBilletUser()
    {
        return billetUser;
    }
}
