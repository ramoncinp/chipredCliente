package com.binarium.chipred.billets;

import java.util.Comparator;

public class BilletUserComparator implements Comparator<BilletUser>
{
    public int compare(BilletUser left, BilletUser right)
    {
        return left.getUserName().toUpperCase().compareTo(right.getUserName().toUpperCase());
    }
}
