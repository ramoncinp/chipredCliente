package com.binarium.chipred.billets;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.snackbar.Snackbar;

import androidx.fragment.app.Fragment;
import androidx.core.content.FileProvider;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipred.ChipREDConstants;
import com.binarium.chipred.ChipRedManager;
import com.binarium.chipred.DebouncedOnClickListener;
import com.binarium.chipred.FullScreenQR;
import com.binarium.chipred.GenericDialog;
import com.binarium.chipred.MainActivity;
import com.binarium.chipred.R;
import com.binarium.chipred.SharedPreferencesManager;
import com.binarium.chipred.Tutorial2;
import com.binarium.chipred.auth.Authenticator;
import com.squareup.picasso.Picasso;
import com.zaihuishou.expandablerecycleradapter.adapter.BaseExpandableAdapter;
import com.zaihuishou.expandablerecycleradapter.viewholder.AbstractAdapterItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static android.app.Activity.RESULT_OK;
import static com.binarium.chipred.ChipREDConstants.REQUEST_CODE_CONFIRM_DEVICE_CREDENTIALS;

public class BilletListFragment extends Fragment implements ChipRedManager.OnMessageReceived
{
    //Constantes
    private static final String TAG = BilletFileManager.class.getSimpleName();
    private final int ITEM_TYPE_BILLET_USER = 1;
    private final int ITEM_TYPE_BILLET_ITEM = 2;
    private final int SHARE_INTENT = 3;

    //Views
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView noBillets;
    private TextView noAuth;

    //Variables
    private String idClient;

    //Objetos
    private ChipRedManager chipRedManager;

    //Listas
    private ArrayList<Billet> billets = new ArrayList<>();
    private ArrayList<BilletUser> billetUsers = new ArrayList<>();

    public BilletListFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View content = inflater.inflate(R.layout.fragment_billet_list, container, false);
        noBillets = content.findViewById(R.id.no_billets);
        progressBar = content.findViewById(R.id.progress_bar);
        recyclerView = content.findViewById(R.id.billet_list);
        swipeRefreshLayout = content.findViewById(R.id.swipeRefresh);
        noAuth = content.findViewById(R.id.auth_error);

        // Inflate the layout for this fragment
        return content;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        //Inicializar views
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ((MainActivity) getActivity()).setActionBarTitle("Mis billetes electrónicos");

        //Indicar que tiene menú
        setHasOptionsMenu(true);

        //Obtener idCliente
        SharedPreferencesManager sharedPreferencesManager =
                new SharedPreferencesManager(getContext());
        idClient = sharedPreferencesManager.getStringFromSP("id_cliente", "");

        //Validar idCliente
        if (idClient.equals(""))
        {
            Toast.makeText(getContext(), "Error al obtener id de cliente", Toast.LENGTH_SHORT).show();
        }
        else
        {
            //Evaluar si esta actividad la autenticación
            if (!excecuteAuth())
            {
                initBilletList();
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.fragment_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == R.id.help)
        {
            Intent intent = new Intent(getActivity(), Tutorial2.class);
            startActivity(intent);
            return true;
        }
        else
        {
            return super.onOptionsItemSelected(item);
        }
    }

    private void initBilletList()
    {
        //Inicializar chipRedManager
        chipRedManager = new ChipRedManager(this, getContext());
        //Obtener lista de billetes
        chipRedManager.getGeneratedBillets(idClient, true);

        //Agregar listener de swipeRefresh
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                //Obtener lista de billetes
                chipRedManager.getGeneratedBillets(idClient, true);
            }
        });
    }

    private boolean excecuteAuth()
    {
        Authenticator authenticator = new Authenticator(this, new Authenticator
                .AuthenticatorInteface()
        {
            @Override
            public void onAuthenticated()
            {
                initBilletList();
            }

            @Override
            public void onCancelledAuthentication()
            {
                onFailedAuth();
            }
        });
        return authenticator.excecuteAuth();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == REQUEST_CODE_CONFIRM_DEVICE_CREDENTIALS)
        {
            if (resultCode == RESULT_OK)
            {
                initBilletList();
            }
            else
            {
                onFailedAuth();
            }
        }

        if (requestCode == SHARE_INTENT)
        {
            Log.d(TAG, "Result code -> " + resultCode);
        }
    }

    @Override
    public void chipRedMessage(String crMessage, int webServiceN)
    {
        Log.v(TAG, "chipRedMessage -> " + crMessage);
        swipeRefreshLayout.setRefreshing(false);

        //Evaluar actividad padre
        if (ChipREDConstants.checkNullParentActivity(getActivity(),
                BilletListFragment.this))
            return;

        if (crMessage.equals(ChipRedManager.LOGOUT))
        {
            ((MainActivity) getActivity()).logOut(true);
        }
        else
        {
            if (crMessage.contains("Billete cancelado"))
            {
                //Refrescar lista de billetes sin autorizar para ser más ágil
                chipRedManager.getGeneratedBillets(idClient, false);

                //Toast.makeText(context, crMessage, Toast.LENGTH_LONG).show();
                Snackbar.make(getActivity().findViewById(android.R.id.content), crMessage,
                        Snackbar.LENGTH_LONG)
                        .show();
            }
        }
    }

    @Override
    public void chipRedError(String errorMessage, int webServiceN)
    {
        Log.v(TAG, "chipRedError -> " + errorMessage);
        swipeRefreshLayout.setRefreshing(false);

        //Evaluar actividad padre
        if (ChipREDConstants.checkNullParentActivity(getActivity(),
                BilletListFragment.this))
            return;

        if (errorMessage.contains("Error al cancelar billete"))
        {
            GenericDialog genericDialog = new GenericDialog("Error de conexión",
                    errorMessage, new Runnable()
            {
                @Override
                public void run()
                {
                    //nada...
                }
            }, null, getActivity());
            genericDialog.show();

            progressBar.setVisibility(View.GONE);
            swipeRefreshLayout.setVisibility(View.VISIBLE);
        }
        else if (errorMessage.contains("Error en consulta de billetes"))
        {
            //Error de conexion
            showLocalBillets();
        }
        else if (errorMessage.contains("usado"))
        {
            //Billete a cancelar ya fue usado
            progressBar.setVisibility(View.GONE);
            swipeRefreshLayout.setVisibility(View.VISIBLE);

            Snackbar.make(getActivity().findViewById(android.R.id.content), errorMessage,
                    Snackbar.LENGTH_LONG)
                    .show();
        }
        else
        {
            //Error interno en chipRed
            showLocalBillets();
        }
    }

    @Override
    public void showResponse(JSONObject response, int webServiceN)
    {
        Log.v(TAG, "chipRedResponse -> " + response.toString());

        //Evaluar actividad padre
        if (ChipREDConstants.checkNullParentActivity(getActivity(),
                BilletListFragment.this))
            return;

        parseResponse(response);
        setLastUpdateDate();

        swipeRefreshLayout.setRefreshing(false);
    }

    private void parseResponse(JSONObject response)
    {
        JSONArray data;

        try
        {
            //Obtener arreglo con los billetes
            data = response.getJSONArray("data");

            //Analizar datos para almacenar
            evaluateSavedBillets(data);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    private void evaluateSavedBillets(JSONArray data)
    {
        //Limpiar lista
        billets.clear();

        //Obtener documento de billetes de la memoria local
        JSONArray file = BilletFileManager.getBilletsDocument(getContext());
        int qrsToGenerate = 0;

        //No había archivo local
        if (file == null)
        {
            file = new JSONArray();
        }

        //Actualizar billetes
        JSONArray finalArray = getAndUpdateBillets(file, data);

        //Validar si existen los qr's necesarios
        for (int i = 0; i < finalArray.length(); i++)
        {
            try
            {
                //Crear billete
                Billet billet = new Billet(finalArray.getJSONObject(i), getContext());

                //Obtener id de billete
                String billetId = billet.getId();

                //Buscar en el arreglo de los billetes almacenados si se encuentra
                String qrPath = BilletFileManager.getQrPath(getContext(), billetId);
                if (qrPath != null)
                {
                    if (qrPath.equals(""))
                    {
                        //Agregar a la lista de qr's a generar
                        qrsToGenerate++;
                    }

                    //Definir valor de path
                    billet.setPath(qrPath);
                }

                //Agregar billete a la lista
                billets.add(billet);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

        //Crear códigos QR pendientes
        if (qrsToGenerate > 0)
        {
            //Crear instancia del generador de QR's
            BilletQRManager qrManager =
                    new BilletQRManager(new WeakReference<>(getContext()), 300,
                            idClient);

            qrManager.setmInterface(new BilletQRManager.BilletQRManagerInterface()
            {
                @Override
                public void onBilletsCreated(ArrayList<Billet> billets)
                {
                    //Mostrar billetes
                    showBilletList();
                }
            });

            //Iniciar proceso de creacion de QR's
            qrManager.execute(billets);
        }
        else
        {
            //Si no hay billetes por generar, mostrar
            showBilletList();
        }

    }

    private JSONArray getAndUpdateBillets(JSONArray localArray, JSONArray gottenArray)
    {
        boolean saveChanges = false;
        JSONArray finalArray = new JSONArray();

        try
        {
            //Obtener cada elemento del arreglo local y buscarlo en el obtenido del servicio
            for (int i = 0; i < localArray.length(); i++)
            {
                //Obtener elemento
                JSONObject localJsonBillet = localArray.getJSONObject(i);

                //Obtener id del elemento
                String localBilletId;
                if (localJsonBillet.has("id")) localBilletId = localJsonBillet.getString("id");
                else localBilletId = localJsonBillet.getString("_id");

                //Obtener status del elemento
                String localStatus = localJsonBillet.getString("estatus");

                //Si ya se acabó no hay más que agregar...
                if (gottenArray.length() == 0)
                {
                    saveChanges = true;
                    break;
                }

                //Buscarlo en el arreglo obtenido del servicio
                for (int j = 0; j < gottenArray.length(); j++)
                {
                    //Obtener elemento
                    JSONObject jsonBillet = gottenArray.getJSONObject(j);

                    //Obtener status del elemento
                    String status = jsonBillet.getString("estatus");

                    //Buscar el elemento local en el archivo de la nube
                    if (localBilletId.equals(jsonBillet.getString("id")))
                    {
                        //Billete usado
                        if (localStatus.equals(ChipREDConstants.BILLETE_ACTIVO) && status.equals(ChipREDConstants.BILLETE_USADO))
                        {
                            //Actualizar valores
                            localJsonBillet.put("estatus", jsonBillet.get("estatus"));
                            localJsonBillet.put("monto_uso", jsonBillet.get("monto_uso"));
                            localJsonBillet.put("fecha_uso", jsonBillet.get("fecha_uso"));

                            //Indicar que se tienen que guardar cambios
                            saveChanges = true;
                        }
                        else if (localStatus.equals(ChipREDConstants.BILLETE_ACTIVO) && status.equals(ChipREDConstants.BILLETE_CANCELADO)
                                || localStatus.equals(ChipREDConstants.BILLETE_CANCELADO)
                                || status.equals(ChipREDConstants.BILLETE_CANCELADO))
                        {
                            //Transicion de activo a cancelado
                            //Eliminar objeto y QR correspondiente
                            BilletFileManager.removeQr(getContext(), localBilletId);

                            //Indicar que se tienen que guardar cambios
                            saveChanges = true;

                            //Remover elemento encontrado de la lista de la nube
                            gottenArray.remove(j);

                            //Terminar ciclo
                            break;
                        }

                        //Evaluar fecha de uso
                        if (localStatus.equals(ChipREDConstants.BILLETE_USADO) && isOneMonthOld(jsonBillet.getString("fecha_uso")))
                        {
                            //Fecha de uso mayor a 30 días
                            //Eliminar objeto y QR correspondiente
                            BilletFileManager.removeQr(getContext(), localBilletId);

                            //Indicar que se tienen que guardar cambios
                            saveChanges = true;

                            //Remover elemento encontrado de la lista de la nube
                            gottenArray.remove(j);

                            //Terminar ciclo
                            break;
                        }

                        //Agregar a la lista final
                        finalArray.put(localJsonBillet);

                        //Remover elemento encontrado de la lista de la nube
                        gottenArray.remove(j);
                        break;
                    }

                    //No se encontró billete en la nube... remover
                    if (j == gottenArray.length() - 1)
                    {
                        //Eliminar objeto y QR correspondiente
                        BilletFileManager.removeQr(getContext(), localBilletId);

                        //Indicar que se tienen que guardar cambios
                        saveChanges = true;
                    }
                }
            }

            //Se terminó de analizar arreglo local, agregar faltantes y filtrar billetes cancelados
            //y vencidos
            if (gottenArray.length() != 0)
            {
                for (int i = 0; i < gottenArray.length(); i++)
                {
                    boolean put;

                    //Obtener billete
                    JSONObject billet = gottenArray.getJSONObject(i);

                    //Filtrar estatus
                    put = !billet.getString("estatus").equals(ChipREDConstants.BILLETE_CANCELADO);
                    put &= !isOneMonthOld(billet.getString("fecha_uso"));

                    //Analizar status y si no esta cancelado, agregar
                    if (put)
                    {
                        finalArray.put(billet);
                        saveChanges = true;
                    }
                }
            }

            //Actualizar
            if (saveChanges)
                BilletFileManager.saveBilletsDocument(getContext(), finalArray);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            //Si algo salió mal, mostrar arreglo de la nube
            return gottenArray;
        }

        return finalArray;
    }

    private void showLocalBillets()
    {
        //Mostrar utlima actualización
        showLastUpdate();

        //Limpiar lista
        billets.clear();

        //Ocultar progressBar
        progressBar.setVisibility(View.GONE);

        //Obtener documento de billetes de la memoria local
        JSONArray file = BilletFileManager.getBilletsDocument(getContext());

        if (file == null)
        {
            //Mostrar texto de no billetes
            noBillets.setVisibility(View.VISIBLE);
        }
        else
        {
            for (int i = 0; i < file.length(); i++)
            {
                try
                {
                    Billet billet = new Billet(file.getJSONObject(i), getContext());

                    //Buscar en el arreglo de los billetes almacenados si se encuentra
                    String qrPath = BilletFileManager.getQrPath(getContext(), billet.getId());
                    if (qrPath != null)
                    {
                        //Definir valor de path
                        billet.setPath(qrPath);
                    }

                    //Agregar billete a la lista
                    billets.add(billet);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }

            showBilletList();
        }
    }

    private void showBilletList()
    {
        //Ocultar progressBar
        progressBar.setVisibility(View.GONE);

        //Validar lista
        if (billets.isEmpty())
        {
            //Mostrar texto de no billetes
            noBillets.setVisibility(View.VISIBLE);
            return;
        }

        //Ordenarlos por fecha
        Collections.sort(billets, new BilletDateComparator());

        //Enlistar usuarios
        sortBilletsByUser();

        //Evaluar cantidad de usuarios para elegir adaptador
        if (billetUsers.size() > 1)
        {
            //Ordenar usuarios de manera alfabética
            Collections.sort(billetUsers, new BilletUserComparator());

            BaseExpandableAdapter baseExpandableAdapter = new BaseExpandableAdapter(billetUsers)
            {
                @NonNull
                @Override
                public AbstractAdapterItem<Object> getItemView(Object type)
                {
                    int itemType = (int) type;

                    switch (itemType)
                    {
                        case ITEM_TYPE_BILLET_USER:
                            return new BilletUserItem();

                        case ITEM_TYPE_BILLET_ITEM:
                            final BilletItem billetItem = new BilletItem(getActivity());

                            billetItem.setOnClickListener(new DebouncedOnClickListener(1000)
                            {
                                @Override
                                public void onDebouncedClick(View v)
                                {
                                    //Obtener id de dispositivo del billete y si es igual
                                    //al del dispositvo usado actualmente, mostrar QR
                                    Billet billet = billetItem.getBillet();
                                    evaluateBilletUser(billet);
                                }
                            });

                            billetItem.setOnLongClickListener(new View.OnLongClickListener()
                            {
                                @Override
                                public boolean onLongClick(View v)
                                {
                                    //Tomar referencia
                                    Billet billet = billetItem.getBillet();

                                    //Remover billete
                                    removeItem(billet.getId());
                                    return true;
                                }
                            });

                            return billetItem;
                    }

                    return null;
                }

                @Override
                public Object getItemViewType(Object t)
                {
                    if (t instanceof BilletUser)
                    {
                        return ITEM_TYPE_BILLET_USER;
                    }
                    else if (t instanceof Billet)
                    {
                        return ITEM_TYPE_BILLET_ITEM;
                    }
                    return -1;
                }
            };

            recyclerView.setAdapter(baseExpandableAdapter);
        }
        else
        {
            //Crear adaptador
            BilletArrayAdapter billetArrayAdapter = new BilletArrayAdapter(billets, getContext());
            billetArrayAdapter.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    Billet selectedBillet = billets.get(recyclerView.getChildAdapterPosition(view));
                    evaluateBilletUser(selectedBillet);
                }
            });
            billetArrayAdapter.setOnLongClickListener(new View.OnLongClickListener()
            {
                @Override
                public boolean onLongClick(View view)
                {
                    //Obtener referencia
                    Billet selectedBillet = billets.get(recyclerView.getChildAdapterPosition(view));

                    //Remover billete
                    removeItem(selectedBillet.getId());
                    return true;
                }
            });
            recyclerView.setAdapter(billetArrayAdapter);
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        //Mostrar lista
        recyclerView.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setVisibility(View.VISIBLE);
    }

    private void showQrInDialog(final Billet billet)
    {
        //Obtener medidas de display
        DisplayMetrics outMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(outMetrics);
        int qrCodeWidth = outMetrics.widthPixels;

        final View alertDialog = getLayoutInflater().inflate(R.layout.qrview, null);

        final ImageView imageView = alertDialog.findViewById(R.id.qrview);
        final ImageView shareIcon = alertDialog.findViewById(R.id.share_billet_icon);

        Picasso.with(getContext()).load(new File(billet.getPath())).into(imageView);
        imageView.requestLayout();
        imageView.getLayoutParams().height = qrCodeWidth - 100;
        imageView.getLayoutParams().width = qrCodeWidth - 100;
        imageView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getActivity(), FullScreenQR.class);

                intent.putExtra("imagePath", billet.getPath());
                startActivity(intent);
                getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim
                        .fade_out);
            }
        });

        shareIcon.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                // Ocultar icono de compartir
                shareIcon.setVisibility(View.GONE);

                // Convertir view a Bitmap
                Bitmap bitmap = createBitmapFromView(alertDialog, 300, 0);

                // Regresar icono de compartir
                shareIcon.setVisibility(View.VISIBLE);

                // Guardar Bitmap y dar como referencia su path
                String savedImagePath = saveImage(bitmap);

                // Compartir QR
                shareQr(savedImagePath);
            }
        });

        TextView textView = alertDialog.findViewById(R.id.amountTv);
        textView.setText(billet.getAmount());

        TextView textViewId = alertDialog.findViewById(R.id.deviceIdTv);
        String deviceIdText = "Generado en " + billet.getDeviceId();
        textViewId.setText(deviceIdText);

        TextView billetNumber = alertDialog.findViewById(R.id.billet_number);
        String numberText = "Folio: " + billet.getBilletNumber();
        billetNumber.setText(numberText);

        AlertDialog.Builder alertadd = new AlertDialog.Builder(getActivity());
        alertadd.setView(alertDialog);
        alertadd.setNegativeButton("Cerrar", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });

        alertadd.setTitle("Mi Billete");
        alertadd.setCancelable(false);
        alertadd.show();
    }

    /**
     * Creates a bitmap from the supplied view.
     *
     * @param view   The view to get the bitmap.
     * @param width  The width for the bitmap.
     * @param height The height for the bitmap.
     * @return The bitmap from the supplied drawable.
     */
    public @NonNull
    static Bitmap createBitmapFromView(@NonNull View view, int width, int height)
    {
        if (width > 0 && height > 0)
        {
            view.measure(View.MeasureSpec.makeMeasureSpec(convertDpToPixels(width),
                    View.MeasureSpec.EXACTLY),
                    View.MeasureSpec.makeMeasureSpec(convertDpToPixels(height),
                            View.MeasureSpec.EXACTLY));
        }
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(),
                view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Drawable background = view.getBackground();

        if (background != null)
        {
            background.draw(canvas);
        }
        view.draw(canvas);

        return bitmap;
    }

    static int convertDpToPixels(float dp)
    {
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dp, Resources.getSystem().getDisplayMetrics()));
    }

    private String saveImage(Bitmap image)
    {
        String filePath = "";

        // Obtener directorio del album para QR's
        String path = getActivity().getFilesDir() + ChipREDConstants.BILLETS_QR_CODES_PATH;
        File dir = new File(path);
        if (!dir.exists())
        {
            //Crear directorio
            if (!dir.mkdir())
            {
                Log.e(TAG, "No se pudo crear el directorio de billetes para cliente");
            }
        }

        //Definir ruta de la nueva imagen
        path = path + "shared_billet.png";

        try
        {
            //Instanciar el archivo para la imagen
            FileOutputStream fosImage = new FileOutputStream(path);
            image.compress(Bitmap.CompressFormat.PNG, 100, fosImage);

            //Cerrar archivo
            fosImage.flush();
            fosImage.close();

            //Retornar path de la imagen creada
            return path;
        }
        catch (FileNotFoundException e)
        {
            Log.d(TAG, "File not found: " + e.getMessage());
        }
        catch (IOException e)
        {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }

        return filePath;
    }

    private void sortBilletsByUser()
    {
        //Limpiar lista de usuarios
        billetUsers.clear();

        //Enlistar los usuarios encontrados
        for (Billet mBillet : billets)
        {
            boolean exists = false;

            String billetUser = mBillet.getDeviceId();

            //Checar si esta el usuario de billete
            for (BilletUser billetUserToFind : billetUsers)
            {
                //Si ya existe ese usuario en la lista
                if (billetUser.equals(billetUserToFind.getName()))
                {
                    billetUserToFind.addBillet(mBillet);
                    exists = true;
                    break;
                }
            }

            if (!exists)
            {
                //Si no estaba en la lista, crear nuevo usuario
                BilletUser newBilletUser = new BilletUser(billetUser);

                //Agregarle el billete
                newBilletUser.addBillet(mBillet);

                //Agregar usuario
                billetUsers.add(newBilletUser);
            }
        }
    }

    private void evaluateBilletUser(Billet billet)
    {
        String thisDeviceId =
                ChipREDConstants.getDeviceId(getContext());

        if (thisDeviceId.endsWith(billet.getDeviceId()))
        {
            //Mostrar QR
            showQrInDialog(billet);
        }
        else
        {
            GenericDialog genericDialog = new GenericDialog(
                    "Aviso", "El billete seleccionado no le " +
                    "pertenece a este dispositivo",
                    new Runnable()
                    {
                        @Override
                        public void run()
                        {

                        }
                    }, null, getContext());
            genericDialog.show();
        }
    }

    void removeItem(final String billetId)
    {
        final Activity activity = getActivity();
        final AlertDialog.Builder dialog = new AlertDialog.Builder(activity);

        dialog.setTitle("Cancelar billete");
        dialog.setMessage("¿Estás seguro de que quieres cancelar este billete?");
        dialog.setPositiveButton("Sí", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.dismiss();
                chipRedManager.deleteBillet(idClient, billetId, true);

                //Ocultar lista
                swipeRefreshLayout.setVisibility(View.GONE);

                //Mostrar progressBar
                progressBar.setVisibility(View.VISIBLE);
            }
        });

        dialog.setNegativeButton("No", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }

    private void setLastUpdateDate()
    {
        SharedPreferencesManager sharedPreferencesManager =
                new SharedPreferencesManager(getContext());

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault());
        String date = dateFormat.format(new Date());

        sharedPreferencesManager.putStringInSP(idClient + ChipREDConstants.LAST_UPDATE_DATE,
                date);
    }

    private void showLastUpdate()
    {
        SharedPreferencesManager sharedPreferencesManager =
                new SharedPreferencesManager(getContext());

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
                Locale.getDefault());
        String now = dateFormat.format(new Date());
        String lastUpdateMessage, lastUpdateDate;

        lastUpdateDate =
                sharedPreferencesManager.getStringFromSP(idClient + ChipREDConstants.LAST_UPDATE_DATE, "");

        if (lastUpdateDate == null || lastUpdateDate.isEmpty())
        {
            return;
        }
        else
        {
            lastUpdateMessage = "Última actualización ";
            try
            {
                Date rightNow = dateFormat.parse(now);
                Date theLastUpdate = dateFormat.parse(lastUpdateDate);

                long diffInMs = rightNow.getTime() - theLastUpdate.getTime();
                long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMs);

                if (diffInSec < 86400) //Menor a un día?
                {
                    lastUpdateMessage += "hace ";
                    if (diffInSec < 60) //Menor a un minuto?
                    {
                        lastUpdateMessage += String.valueOf(diffInSec);
                        lastUpdateMessage += "s";
                    }
                    else if (diffInSec < 3600) //Menor a una hora?
                    {
                        lastUpdateMessage += String.valueOf(diffInSec / 60);
                        lastUpdateMessage += "m";
                    }
                    else
                    {
                        lastUpdateMessage += String.valueOf((int) (diffInSec / 3600));
                        lastUpdateMessage += "h";
                    }
                }
                else
                {
                    lastUpdateMessage += ": " + lastUpdateDate;
                }
            }
            catch (ParseException e)
            {
                e.printStackTrace();
            }

            String message = "No se pudieron actualizar los billetes";
            message += "\n";
            message += lastUpdateMessage;

            Snackbar.make(getActivity().findViewById(android.R.id.content), message,
                    Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    public boolean isOneMonthOld(String currentDateFormat)
    {
        //Formato de entrada "dd-mm-yyyy hh-MM"
        Date today = new Date();
        Date billetUsedDate;

        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm", Locale.getDefault());
        try
        {
            billetUsedDate = format.parse(currentDateFormat);
        }
        catch (ParseException e)
        {
            return false;
        }

        long millisecondsSinceToday = today.getTime() - billetUsedDate.getTime();

        int daysPassed = (int) (millisecondsSinceToday / (24 * 60 * 60 * 1000));

        if (daysPassed > 30)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void shareQr(String path)
    {
        try
        {
            File qrFile = new File(path);

            Uri contentUri = FileProvider.getUriForFile(getContext(), "com.binarium.chipred" +
                    ".fileprovider", qrFile);
            if (contentUri != null)
            {
                Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                shareIntent.setType("image/*");
                shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                shareIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                startActivityForResult(Intent.createChooser(shareIntent, "Compartir Billete " +
                        "electrónico.." +
                        "."), SHARE_INTENT);
            }
        }
        catch (NullPointerException | IllegalArgumentException e)
        {
            e.printStackTrace();
        }
    }

    private void onFailedAuth()
    {
        progressBar.setVisibility(View.GONE);
        noAuth.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setVisibility(View.GONE);
    }
}
