package com.binarium.chipred.billets;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.binarium.chipred.ChipREDConstants;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class BilletQRManager extends AsyncTask<ArrayList<Billet>, Void, ArrayList<Billet>>
{
    private static final String TAG = BilletQRManager.class.getSimpleName();

    //Variables auxiliares
    private WeakReference<Context> contextRef;
    private int qrWidth;
    private String clientId;
    private BilletQRManagerInterface mInterface;

    public BilletQRManager(WeakReference<Context> contextRef, int qrWidth, String clientId)
    {
        this.contextRef = contextRef;
        this.qrWidth = qrWidth;
        this.clientId = clientId;
    }

    public void setmInterface(BilletQRManagerInterface mInterface)
    {
        this.mInterface = mInterface;
    }

    @Override
    protected ArrayList<Billet> doInBackground(ArrayList<Billet>... arrayLists)
    {
        //Recibir lista de billetes a crear
        ArrayList<Billet> billetsToCreate = arrayLists[0];

        //Crear lista de salida
        ArrayList<Billet> generatedQrs = new ArrayList<>();

        for (int i = 0; i < billetsToCreate.size(); i++)
        {
            //Obtener billete actual
            Billet billet = billetsToCreate.get(i);

            //Validar que no tenga un path
            if (billet.getPath().equals(""))
            {
                try
                {
                    //Definir datos en QR
                    String data = clientId + "|" + billet.getId();

                    //Generar código QR
                    MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
                    BitMatrix bitMatrix = multiFormatWriter.encode(data,
                            BarcodeFormat.QR_CODE,
                            qrWidth, qrWidth);

                    BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                    Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);

                    //Almacenar
                    if (bitmap != null)
                    {
                        //Guardar QR generado.
                        billet.setPath(saveBillet(bitmap, billet.getId()));

                        //Agregar a la lista de QR's generados
                        generatedQrs.add(billet);
                    }
                }
                catch (WriterException e)
                {
                    e.printStackTrace();
                }
            }
        }

        return generatedQrs;
    }

    private String saveBillet(Bitmap image, String billetId)
    {
        //Obtener contexto
        Context context = contextRef.get();

        // Obtener directorio del album para QR's
        String path = context.getFilesDir() + ChipREDConstants.BILLETS_QR_CODES_PATH;
        File dir = new File(path);
        if (!dir.exists())
        {
            //Crear directorio
            if (!dir.mkdir())
            {
                Log.e(TAG, "No se pudo crear el directorio de billetes para cliente");
            }
        }

        //Definir ruta de la nueva imagen
        path = path + billetId + ".png";

        try
        {
            //Instanciar el archivo para la imagen
            FileOutputStream fosImage = new FileOutputStream(path);
            image.compress(Bitmap.CompressFormat.PNG, 100, fosImage);

            //Cerrar archivo
            fosImage.flush();
            fosImage.close();

            //Retornar path de la imagen creada
            return path;
        }
        catch (FileNotFoundException e)
        {
            Log.d(TAG, "File not found: " + e.getMessage());
        }
        catch (IOException e)
        {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }

        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<Billet> billets)
    {
        super.onPostExecute(billets);
        mInterface.onBilletsCreated(billets);
    }

    public interface BilletQRManagerInterface
    {
        void onBilletsCreated(ArrayList<Billet> billets);
    }
}
