package com.binarium.chipred.billets;

import com.binarium.chipred.billets.Billet;

import java.util.Comparator;

public class BilletDateComparator implements Comparator<Billet>
{
    public int compare(Billet left, Billet right)
    {
        return right.getBilletDate().compareTo(left.getBilletDate());
    }
}
