package com.binarium.chipred.billets;

import com.binarium.chipred.billets.Billet;
import com.zaihuishou.expandablerecycleradapter.model.ExpandableListItem;

import java.util.ArrayList;
import java.util.List;

public class BilletUser implements ExpandableListItem
{
    private boolean mExpanded = false;
    private String name;
    private List<Billet> billets;

    public BilletUser(String name)
    {
        this.name = name;
    }

    @Override
    public List<?> getChildItemList()
    {
        return billets;
    }

    @Override
    public boolean isExpanded()
    {
        return mExpanded;
    }

    @Override
    public void setExpanded(boolean isExpanded)
    {
        mExpanded = isExpanded;
    }

    @Override
    public String toString()
    {
        return "Company{" +
                "name='" + name + '\'' +
                '}';
    }

    public String getUserName()
    {
        return name.substring(name.indexOf("de ") + 3);
    }

    public String getName()
    {
        return name;
    }

    public String getDeviceName()
    {
        return name.substring(0, name.lastIndexOf("de"));
    }

    public void setmBillets(List<Billet> billets)
    {
        this.billets = billets;
    }

    public void addBillet(Billet billetToAdd)
    {
        if (billets == null)
        {
            billets = new ArrayList<>();
            billets.add(billetToAdd);
        }
        else
        {
            billets.add(billetToAdd);
        }
    }
}
