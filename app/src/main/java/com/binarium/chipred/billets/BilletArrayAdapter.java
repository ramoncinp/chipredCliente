package com.binarium.chipred.billets;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.binarium.chipred.ChipREDConstants;
import com.binarium.chipred.R;

import java.util.ArrayList;

public class BilletArrayAdapter extends RecyclerView.Adapter<BilletArrayAdapter.BilletViewHolder>
        implements View.OnClickListener, View.OnLongClickListener
{
    private final ArrayList<Billet> billets;
    private Context context;
    private View.OnClickListener listener;
    private View.OnLongClickListener longListener;

    public BilletArrayAdapter(ArrayList<Billet> billets, Context context)
    {
        this.billets = billets;
        this.context = context;
    }

    @Override
    public int getItemCount()
    {
        return billets.size();
    }

    @Override
    public BilletViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.billet_card,
                viewGroup, false);
        BilletViewHolder bvh = new BilletViewHolder(v);

        v.setOnClickListener(this);
        v.setOnLongClickListener(this);

        return bvh;
    }

    @Override
    public void onBindViewHolder(BilletViewHolder billetViewHolder, int i)
    {
        //Obtener billete
        Billet billet = billets.get(i);

        billetViewHolder.qrPreview.getContext();
        int id = context.getResources().getIdentifier("cash", "drawable", context.getPackageName());

        billetViewHolder.amount.setText(billet.getAmount());
        billetViewHolder.date.setText(ChipREDConstants.monthNumberToAbreviation(billet.getDay()));
        billetViewHolder.time.setText(billet.getTime());
        billetViewHolder.qrPreview.setImageResource(id);

        String billetStatus = billet.getStatus();
        if (billetStatus == null)
        {
            billetViewHolder.status.setText(context.
                    getResources().
                    getString(R.string.billeteActivo));
        }
        else if (billetStatus.equals("AC"))
        {
            billetViewHolder.status.setText(context.
                    getResources().
                    getString(R.string.billeteActivo));
            billetViewHolder.usedAmount.setVisibility(View.GONE);

            billetViewHolder.status.setTextColor(
                    ContextCompat.getColor(context, R.color.green_money));

            billetViewHolder.billetCv.setCardBackgroundColor(
                    ContextCompat.getColor(context, R.color.activo));

            billetViewHolder.amount.setTextColor(context.getResources().getColor(R.color.black));
        }
        else if (billetStatus.equals("US"))
        {
            billetViewHolder.status.setText(context.
                    getResources().
                    getString(R.string.billeteCanjeado));

            billetViewHolder.status.setTextColor(
                    ContextCompat.getColor(context, R.color.darkBlue));

            billetViewHolder.billetCv.setCardBackgroundColor(
                    ContextCompat.getColor(context, R.color.canjeado));

            billetViewHolder.usedAmount.setVisibility(View.VISIBLE);
            billetViewHolder.usedAmount.setText(billet.getAmount());
            billetViewHolder.amount.setText(billet.getUsedAmount());
            billetViewHolder.amount.setTextColor(context.getResources().getColor(R.color.colorPrimary));

            billetViewHolder.date.setText(ChipREDConstants.monthNumberToAbreviation(billet.getUsedDay()));
            billetViewHolder.date.setTextColor(ContextCompat.getColor(context,
                    R.color.green_money));
            billetViewHolder.time.setText(billet.getUsedTime());
            billetViewHolder.time.setTextColor(ContextCompat.getColor(context,
                    R.color.green_money));
        }
        else if (billetStatus.equals("CA"))
        {
            billetViewHolder.status.setText(context.
                    getResources().
                    getString(R.string.billeteCancelado));
            billetViewHolder.usedAmount.setVisibility(View.GONE);

            billetViewHolder.status.setTextColor(
                    ContextCompat.getColor(context, R.color.red));

            billetViewHolder.billetCv.setCardBackgroundColor(
                    ContextCompat.getColor(context, R.color.cancelado));

            billetViewHolder.amount.setTextColor(context.getResources().getColor(R.color.black));
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setOnClickListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    public void setOnLongClickListener(View.OnLongClickListener longListener)
    {
        this.longListener = longListener;
    }

    @Override
    public void onClick(View view)
    {
        if (listener != null)
        {
            listener.onClick(view);
        }
    }

    @Override
    public boolean onLongClick(View view)
    {
        if (longListener != null)
        {
            longListener.onLongClick(view);
        }
        return false;
    }

    public static class BilletViewHolder extends RecyclerView.ViewHolder
    {
        private CardView billetCv;
        private TextView amount;
        private TextView date;
        private TextView time;
        private TextView status;
        private TextView usedAmount;
        private ImageView qrPreview;

        public BilletViewHolder(View itemView)
        {
            super(itemView);

            billetCv = itemView.findViewById(R.id.billetCv);
            amount = itemView.findViewById(R.id.amount);
            date = itemView.findViewById(R.id.date);
            time = itemView.findViewById(R.id.time);
            status = itemView.findViewById(R.id.status);
            usedAmount = itemView.findViewById(R.id.usedAmount);
            qrPreview = itemView.findViewById(R.id.qrpreview);
        }
    }
}
