package com.binarium.chipred.billets;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.binarium.chipred.ChipREDConstants;
import com.binarium.chipred.R;
import com.zaihuishou.expandablerecycleradapter.viewholder.AbstractAdapterItem;

public class BilletItem extends AbstractAdapterItem implements View.OnClickListener, View
        .OnLongClickListener
{
    //Declarar views de la tarjeta
    private CardView billetCv;
    private TextView amount;
    private TextView date;
    private TextView time;
    private TextView status;
    private TextView usedAmount;
    private ImageView qrPreview;

    private Billet billet;

    private int position;

    private View.OnClickListener onClickListener;
    private View.OnLongClickListener onLongClickListener;

    private Context context;

    public BilletItem(Context context)
    {
        this.context = context;
    }

    @Override
    public int getLayoutResId()
    {
        return R.layout.billet_card;
    }

    @Override
    public void onBindViews(View root)
    {
        //Definir views
        billetCv = root.findViewById(R.id.billetCv);
        amount = root.findViewById(R.id.amount);
        date = root.findViewById(R.id.date);
        time = root.findViewById(R.id.time);
        status = root.findViewById(R.id.status);
        usedAmount = root.findViewById(R.id.usedAmount);
        qrPreview = root.findViewById(R.id.qrpreview);
    }

    @Override
    public void onSetViews()
    {

    }

    @Override
    public void onUpdateViews(Object model, int position)
    {
        if (model instanceof Billet)
        {
            Billet billet = (Billet) model;
            this.billet = billet;
            this.position = position;

            //Setear Views
            qrPreview.getContext();
            int id = context.getResources().getIdentifier("cash", "drawable", context
                    .getPackageName());

            amount.setText(billet.getAmount());
            date.setText(ChipREDConstants.monthNumberToAbreviation(billet.getDay()));
            time.setText(billet.getTime());
            qrPreview.setImageResource(id);

            String billetStatus = billet.getStatus();
            if (billetStatus == null)
            {
                status.setText(context.
                        getResources().
                        getString(R.string.billeteActivo));
            }
            else if (billetStatus.equals("AC"))
            {
                status.setText(context.
                        getResources().
                        getString(R.string.billeteActivo));
                usedAmount.setVisibility(View.GONE);

                status.setTextColor(ContextCompat.getColor(context, R.color.green_money));

                billetCv.setCardBackgroundColor(ContextCompat.getColor(context, R.color.activo));

                amount.setTextColor(context.getResources().getColor(R.color.black));
            }
            else if (billetStatus.equals("US"))
            {
                status.setText(context.
                        getResources().
                        getString(R.string.billeteCanjeado));

                status.setTextColor(ContextCompat.getColor(context, R.color.darkBlue));

                billetCv.setCardBackgroundColor(ContextCompat.getColor(context, R.color.canjeado));

                usedAmount.setVisibility(View.VISIBLE);
                usedAmount.setText(billet.getAmount());
                amount.setText(billet.getUsedAmount());
                amount.setTextColor(context.getResources().getColor(R.color.colorPrimary));

                date.setText(ChipREDConstants.monthNumberToAbreviation(billet.getUsedDay()));
                date.setTextColor(ContextCompat.getColor(context, R.color.green_money));
                time.setText(billet.getUsedTime());
                time.setTextColor(ContextCompat.getColor(context, R.color.green_money));
            }
            else if (billetStatus.equals("CA"))
            {
                status.setText(context.
                        getResources().
                        getString(R.string.billeteCancelado));
                usedAmount.setVisibility(View.GONE);

                status.setTextColor(ContextCompat.getColor(context, R.color.red));

                billetCv.setCardBackgroundColor(ContextCompat.getColor(context, R.color.cancelado));

                amount.setTextColor(context.getResources().getColor(R.color.black));
            }

            billetCv.setOnClickListener(onClickListener);
            billetCv.setOnLongClickListener(onLongClickListener);
        }
    }

    public void setOnClickListener(View.OnClickListener onClickListener)
    {
        this.onClickListener = onClickListener;
    }

    public void setOnLongClickListener(View.OnLongClickListener onLongClickListener)
    {
        this.onLongClickListener = onLongClickListener;
    }

    @Override
    public void onClick(View view)
    {
        if (onClickListener != null)
        {
            onClickListener.onClick(view);
        }
    }

    public Billet getBillet()
    {
        return this.billet;
    }

    @Override
    public boolean onLongClick(View v)
    {
        if (onLongClickListener != null)
        {
            onLongClickListener.onLongClick(v);
        }
        return true;
    }

    public int getPosition()
    {
        return this.position;
    }
}
