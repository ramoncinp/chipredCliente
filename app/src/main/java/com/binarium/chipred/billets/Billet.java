package com.binarium.chipred.billets;

import android.content.Context;

import com.binarium.chipred.ChipREDConstants;
import com.binarium.chipred.SharedPreferencesManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Billet
{
    private String amount;
    private String time;
    private String day;
    private String path;
    private String id;
    private String status;
    private String deviceId;
    private String usedAmount;
    private String usedDay;
    private String usedTime;
    private String currencySymbol;
    private String billetNumber;
    private DecimalFormat currencyFormat;

    public Billet(String amount, String day, String time, String path, String id, Context context)
    {
        this.amount = amount;
        this.day = day;
        this.time = time;
        this.path = path;
        this.id = id;

        //Definir formatos de moneda según el país
        SharedPreferencesManager sharedPreferencesManager;
        sharedPreferencesManager = new SharedPreferencesManager(context);

        //Obtener el país
        String country = sharedPreferencesManager.getStringFromSP("pais", "mexico");

        //Evaluar el país
        if (country.equals("mexico"))
        {
            currencySymbol = ChipREDConstants.MXN_CURRENCY_SYMBOL;
            currencyFormat = ChipREDConstants.MX_AMOUNT_FORMAT;
        }
        else
        {
            currencySymbol = ChipREDConstants.CRC_CURRENCY_SYMBOL;
            currencyFormat = ChipREDConstants.CR_AMOUNT_FORMAT;
        }

        usedAmount = "0";
    }

    public Billet(JSONObject jsonObject, Context context)
    {
        try
        {
            //Manejar las dos respuestas para obtener id
            if (jsonObject.has("id"))
            {
                id = jsonObject.getString("id");
            }
            else
            {
                id = jsonObject.getString("_id");
            }

            status = jsonObject.getString("estatus");
            amount = jsonObject.getString("monto");
            usedAmount = jsonObject.getString("monto_uso");
            billetNumber = jsonObject.getString("folio");

            if (jsonObject.has("fecha_alta"))
            {
                String creationDate = jsonObject.getString("fecha_alta");

                //Definir fecha de creación
                day = creationDate.substring(0, creationDate.indexOf(' '));
                day = replaceDateSeparators(day);
                time = creationDate.substring(creationDate.indexOf(' ') + 1);
            }

            if (jsonObject.has("fecha_uso"))
            {
                String usedDate = jsonObject.getString("fecha_uso");

                //Definir fecha de uso
                usedDay = usedDate.substring(0, usedDate.indexOf(' '));
                usedDay = replaceDateSeparators(usedDay);
                usedTime = usedDate.substring(usedDate.indexOf(' ') + 1);
            }

            if (jsonObject.has("id_dispositivo")) deviceId = jsonObject.getString("id_dispositivo");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        //Definir formatos de moneda según el país
        SharedPreferencesManager sharedPreferencesManager;
        sharedPreferencesManager = new SharedPreferencesManager(context);

        //Obtener el país
        String country = sharedPreferencesManager.getStringFromSP("pais", "mexico");

        //Evaluar el país
        if (country.equals("mexico"))
        {
            currencySymbol = ChipREDConstants.MXN_CURRENCY_SYMBOL;
            currencyFormat = ChipREDConstants.MX_AMOUNT_FORMAT;
        }
        else
        {
            currencySymbol = ChipREDConstants.CRC_CURRENCY_SYMBOL;
            currencyFormat = ChipREDConstants.CR_AMOUNT_FORMAT;
        }
    }

    public String getBilletNumber()
    {
        return billetNumber;
    }

    public void setBilletNumber(String billetNumber)
    {
        this.billetNumber = billetNumber;
    }

    public String getTime()
    {
        return time;
    }

    public String getDay()
    {
        return day;
    }

    public String getAmount()
    {
        Double amountValue = Double.parseDouble(amount);
        return currencySymbol + currencyFormat.format(amountValue);
    }

    public String getUsedAmount()
    {
        Double amountValue = Double.parseDouble(usedAmount);
        return currencySymbol + currencyFormat.format(amountValue);
    }

    public String getPath()
    {
        if (path == null) return "";
        else return path;
    }

    public String getId()
    {
        return id;
    }

    public String getStatus()
    {
        return status;
    }

    public String getDeviceId()
    {
        return deviceId;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public void setDeviceId(String deviceId)
    {
        this.deviceId = deviceId;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setUsedAmount(String usedAmount)
    {
        this.usedAmount = usedAmount;
    }

    public Date getBilletDate()
    {
        String date = this.day + " " + this.time;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        Date convertedDate;
        try
        {
            convertedDate = dateFormat.parse(date);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
            return null;
        }
        return convertedDate;
    }

    public void setUsedDay(String usedDay)
    {
        this.usedDay = usedDay.replace("-", "/");
    }

    public void setUsedTime(String usedTime)
    {
        this.usedTime = usedTime.replace("-", "/");
    }

    public String getUsedDay()
    {
        return usedDay;
    }

    public String getUsedTime()
    {
        return usedTime;
    }

    public void setPath(String path)
    {
        this.path = path;
    }

    private String replaceDateSeparators(String date)
    {
        try
        {
            if (date.contains("-"))
            {
                return date.replace("-", "/");
            }
        }
        catch (Exception e)
        {
            return date;
        }

        return date;
    }
}
