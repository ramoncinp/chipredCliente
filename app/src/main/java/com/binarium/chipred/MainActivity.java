package com.binarium.chipred;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.binarium.chipred.utils.DateUtils;
import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipred.billets.BilletFileManager;
import com.binarium.chipred.billets.BilletListFragment;
import com.binarium.chipred.billets.QrGenerateFragment;
import com.binarium.chipred.config.SettingsActivity;
import com.binarium.chipred.login.LogInActivity;
import com.google.firebase.messaging.FirebaseMessaging;
import com.jaredrummler.android.device.DeviceName;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;

import org.json.JSONObject;

import java.util.Date;

import static com.binarium.chipred.ChipREDConstants.twoDigits;
import static com.binarium.chipred.login.LogInActivity.hideSoftKeyboard;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener
{
    /**
     * Constantes
     **/
    private final static int LOGIN_ACTIVITY = 0;
    public final static int GO_TO_PREFERENCES = 10;
    public final static int HANDLER_DELAY = 250;

    public final static String LOGGED = "logged";
    public final static String DEVICEUSER = "deviceUser";
    public final static String PURCHASES_FRAGMENT = "PurchasesFragment";

    /**
     * Tipos de datos
     **/
    private boolean showingLogOutDialog = false;
    private int currentFragment;
    private String idClient;

    /**
     * Objetos
     **/
    private Bundle savedInstanceState;
    private ChipRedManager chipRedManager;
    public ProgressDialog progressDialog;
    public NavigationView navigationView = null;

    Toolbar toolbar = null;
    DrawerLayout drawer;
    SharedPreferencesManager sharedPreferencesManager;

    /**
     * Fragments
     **/
    ClientInfoFragment clientFragment;
    QrGenerateFragment qrGenerateFragment;
    BilletListFragment billetList;
    PurchasesFragment purchasesFragment;
    PaymentFragment paymentFragment;
    ReportsFragment reportsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;

        boolean logged;

        //Inicializar manejador de sharedPreferences
        sharedPreferencesManager = new SharedPreferencesManager(this);

        //Obtener id de cliente almacenado en memoria
        idClient = sharedPreferencesManager.getStringFromSP("id_cliente", "");
        Log.d("MainActivity", "clientId: " + idClient);

        //sharedPreferencesManager.remove(DEVICEUSER);

        //Obtener variable que contiene el estado de la sesión
        logged = sharedPreferencesManager.isContained(LOGGED);

        // Obtener país de cliente
        String country = sharedPreferencesManager.getStringFromSP("pais", "");

        //Si la sesión ha sido iniciada y no existe un nombre de usuario
        if (logged && !sharedPreferencesManager.isContained(DEVICEUSER) && country.equals("mexico"))
        {
            //Pedir nombre de usuario
            askForUserName(false, null, null);
        }
        else
        {
            //Inicializar actividad
            initOnCreate();
        }

        //Si la sesion esta iniciada y hay un id de cliente valido
        if (logged && !idClient.equals(""))
        {
            //Suscribirse al tema con el idCliente para recibir notificaciones
            FirebaseMessaging.getInstance().subscribeToTopic(idClient);
            //Crear canal de notificaciones para Android O
            new NotificationChannelManager(this).createDefaultNotificationChannel();
        }

        //Inicializar objeto que manda los servicios a ChipRED
        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived()
        {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN)
            {
                if (crMessage.equals(ChipRedManager.LOGOUT))
                {
                    logOut(true);
                }
                else
                {
                    if (webServiceN == ChipRedManager.ADD_DEVICE_WB)
                    {
                        if (crMessage.contains(ChipRedManager.OK))
                        {
                            restartActivity();
                        }
                        else
                        {
                            //No fue valido el nombre de usuario
                            sharedPreferencesManager.remove(DEVICEUSER);
                            askForUserName(false, null, crMessage);
                        }
                    }
                    else if (webServiceN == ChipRedManager.CHECK_DEVICE_WB)
                    {
                        Log.d("CHECK_DEVICE_WB", crMessage);
                    }
                    else
                    {
                        //Cuando la sesión se cerró exitosamente
                        if (progressDialog != null)
                        {
                            progressDialog.dismiss();
                        }
                        logOut(false);
                    }
                }
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN)
            {
                if (webServiceN == ChipRedManager.ADD_DEVICE_WB)
                {
                    askForUserName(false, null, errorMessage);
                }
                else
                {
                    //Cuando la sesión no se pudo cerrar
                    if (progressDialog != null)
                    {
                        progressDialog.dismiss();
                    }

                    GenericDialog genericDialog = new GenericDialog(
                            "Error de conexión",
                            errorMessage,
                            new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    //nada...
                                }
                            },
                            null,
                            MainActivity.this
                    );
                    genericDialog.show();
                }
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN)
            {

            }
        }, this);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        super.onSaveInstanceState(savedInstanceState);

        //Guardar el id del fragment actual
        savedInstanceState.putInt("currentFragment", currentFragment);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);

        currentFragment = savedInstanceState.getInt("currentFragment");

        if (currentFragment == R.id.client_info && clientFragment == null && navigationView != null)
        {
            setDefaultFragment();
        }
    }

    @Override
    public void onBackPressed()
    {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        }
        else
        {
            drawer.openDrawer(GravityCompat.START);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item)
    {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        //Cerrar menu cuando se elige una opcion
        drawer.closeDrawer(GravityCompat.START);

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.preferences)
        {
            Intent preferencesIntent = new Intent(this, SettingsActivity.class);
            startActivityForResult(preferencesIntent, GO_TO_PREFERENCES);
        }
        else if (id == R.id.generate_qr)
        {
            qrGenerateFragment = null;
            qrGenerateFragment = new QrGenerateFragment();
            FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager()
                            .beginTransaction().replace(R.id.fragment_container,
                            qrGenerateFragment);
            fragmentTransaction.commit();
        }
        else if (id == R.id.logout)
        {
            GenericDialog logOutDialog = new GenericDialog(
                    getApplicationContext().getResources().getString(R.string.logOutNvItem),
                    "¿Seguro que quieres cerrar sesión?",
                    new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            progressDialog = null;
                            progressDialog = ProgressDialog.show(MainActivity.this,
                                    "",
                                    "Cerrando sesión",
                                    true,
                                    false
                            );

                            chipRedManager.testConnection();
                        }
                    },
                    new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            //nada...
                        }
                    },
                    MainActivity.this);
            logOutDialog.show();
        }
        else if (id == R.id.client_info)
        {
            clientFragment = null;
            clientFragment = new ClientInfoFragment();
            FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager()
                            .beginTransaction().replace(R.id.fragment_container, clientFragment);
            fragmentTransaction.commit();
        }
        else if (id == R.id.billetList)
        {
            billetList = null;
            billetList = new BilletListFragment();
            FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager()
                            .beginTransaction().replace(R.id.fragment_container, billetList);
            fragmentTransaction.commit();
        }
        else if (id == R.id.listpurchases)
        {
            purchasesFragment = null;
            purchasesFragment = new PurchasesFragment();
            FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager()
                            .beginTransaction().replace(R.id.fragment_container, purchasesFragment,
                            PURCHASES_FRAGMENT);
            fragmentTransaction.commit();
        }
        else if (id == R.id.listpayments)
        {
            paymentFragment = null;
            paymentFragment = new PaymentFragment();
            FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager()
                            .beginTransaction().replace(R.id.fragment_container, paymentFragment);

            fragmentTransaction.commit();
        }
        else if (id == R.id.reports)
        {
            reportsFragment = null;
            reportsFragment = new ReportsFragment();

            FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, reportsFragment);

            fragmentTransaction.commit();
        }
        else if (id == R.id.main_tut)
        {
            Intent intent = new Intent(MainActivity.this, Tutorial1.class);
            startActivity(intent);
        }

        currentFragment = id;

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {
            //Regreso de iniciar sesión
            case 0:
                sharedPreferencesManager.putBooleanInSP(LOGGED, true);

            case GO_TO_PREFERENCES:
                currentFragment = R.id.client_info;
                restartActivity();
                break;
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        checkNotifications();
    }

    private void checkNotifications()
    {
        if (sharedPreferencesManager.isContained("notifications"))
        {
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) notificationManager.cancelAll();

            sharedPreferencesManager.remove("notifications");
            sharedPreferencesManager.putIntInSP("SingleNotifications", 5);
        }
    }

    public void setActionBarTitle(String title)
    {
        setTitle(title);
    }

    public void initOnCreate()
    {
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        )
        {
            @Override
            public void onDrawerStateChanged(int newState)
            {
                if (newState == DrawerLayout.STATE_SETTLING ||
                        newState == DrawerLayout.STATE_DRAGGING)
                {
                    if (MainActivity.this.getRequestedOrientation() ==
                            ActivityInfo.SCREEN_ORIENTATION_SENSOR)
                    {
                        Log.d("Orientation: ", "Tratar de hacerla portrait");
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    }

                    if (navigationView.getMenu().findItem(R.id.generate_qr).isChecked()
                            && currentFragment != R.id.billetList)
                        if (qrGenerateFragment != null)
                            qrGenerateFragment.hideKeyboard();

                    invalidateOptionsMenu();
                }
                else
                {
                    //Si esta seleccionada la opcion de consumos, volver al modo "Sensor"
                    if (navigationView.getMenu().getItem(3).isChecked())
                    {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
                    }
                }
            }

            public void onDrawerClosed(View view)
            {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView)
            {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        requestDeviceNames();

        if (!sharedPreferencesManager.isContained(LOGGED))
        {
            Intent intent = new Intent(MainActivity.this, LogInActivity.class);
            startActivityForResult(intent, LOGIN_ACTIVITY);
        }
        else
        {
            View header = navigationView.getHeaderView(0);
            TextView clientMail = header.findViewById(R.id.client_email);
            TextView deviceUserTv = header.findViewById(R.id.deviceUserTv);

            clientMail.setText(sharedPreferencesManager.getStringFromSP("username", ""));
            if (sharedPreferencesManager.isContained(DEVICEUSER))
            {
                String helloMessage;

                helloMessage = "¡Hola ";
                helloMessage += sharedPreferencesManager.getStringFromSP(DEVICEUSER, "");
                helloMessage += "!";
                deviceUserTv.setText(helloMessage);
            }
            else
            {
                deviceUserTv.setText("Cliente ChipRED");
            }

            Intent receive = getIntent();
            int fragmentToOpen = receive.getIntExtra("fragment", 0);

            if (receive.getExtras() != null)
            {
                if (fragmentToOpen == 1001)
                {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("list", true);

                    purchasesFragment = null;
                    purchasesFragment = new PurchasesFragment();
                    purchasesFragment.setArguments(bundle);
                    FragmentTransaction fragmentTransaction =
                            getSupportFragmentManager()
                                    .beginTransaction().replace(R.id.fragment_container,
                                    purchasesFragment,
                                    PURCHASES_FRAGMENT);
                    fragmentTransaction.commit();

                    navigationView.setCheckedItem(R.id.listpurchases);
                }
                else if (fragmentToOpen == 1002)
                {
                    onNavigationItemSelected(navigationView.getMenu().getItem(4));
                    navigationView.setCheckedItem(R.id.listpayments);
                }
                else
                {
                    setDefaultFragment();
                }
            }
            else
            {
                if (savedInstanceState == null)
                {
                    setDefaultFragment();
                }
            }
        }
    }

    public void requestDeviceNames()
    {
        DeviceName.with(this).request(new DeviceName.Callback()
        {
            @Override
            public void onFinished(DeviceName.DeviceInfo info, Exception error)
            {
                //Nada
            }
        });
    }

    public void askForUserName(final boolean confirmation,
                               final String initialUsername,
                               String errorMessage)
    {
        LayoutInflater inflater = getLayoutInflater();
        View alertDialog = inflater.inflate(R.layout.input_user_name, null);

        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        final EditText input = alertDialog.findViewById(R.id.userNameInput);
        final TextView message = alertDialog.findViewById(R.id.ask_user_name_message);
        final TextView errorMessageTv = alertDialog.findViewById(R.id.ask_user_name_errormessage);
        final AlertDialog dialog;

        input.setHint("Ej. PEDRO");
        input.setFilters(new InputFilter[]{
                new InputFilter.LengthFilter(12),
                new InputFilter.AllCaps()
        }); //Max 12 chars y AllCaps
        input.setInputType(EditorInfo.TYPE_TEXT_VARIATION_PERSON_NAME);

        if (errorMessage == null)
        {
            errorMessageTv.setVisibility(View.GONE);
        }
        else
        {
            errorMessageTv.setVisibility(View.VISIBLE);
            errorMessageTv.setText(errorMessage);
        }

        if (confirmation) message.setText("Confirme su nombre de usuario");
        else message.setText("Ingrese su nombre de usuario");

        builder.setTitle("Usuario del dispositivo");
        builder.setCancelable(false);
        builder.setView(alertDialog);

        builder.setPositiveButton("Ingresar",
                new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        String theName = input.getText().toString();

                        if (theName.isEmpty())
                        {
                            askForUserName(false,
                                    null,
                                    "Ingrese un nombre válido");
                        }
                        else
                        {
                            if (confirmation)
                            {
                                if (theName.equals(initialUsername))
                                {
                                    String userName = input.getText().toString();

                                    sharedPreferencesManager.putStringInSP(DEVICEUSER,
                                            userName);
                                    hideSoftKeyboard(MainActivity.this, input);

                                    chipRedManager.addAuthenticatedDevice(idClient);
                                }
                                else
                                {
                                    askForUserName(false,
                                            null,
                                            "No coincide la confirmación");
                                }
                            }
                            else
                            {
                                askForUserName(true, theName, null);
                            }
                        }
                        dialogInterface.dismiss();
                    }
                });

        dialog = builder.create();

        if (dialog.getWindow() != null)
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        dialog.show();
    }

    public void setDefaultFragment()
    {
        navigationView.getMenu().getItem(0).setChecked(true);

        if (clientFragment == null)
        {
            clientFragment = new ClientInfoFragment();
            FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager()
                            .beginTransaction().replace(R.id.fragment_container, clientFragment);
            fragmentTransaction.commit();
        }
    }

    public void deleteAllBillets()
    {
        //Eliminar archivo de billetes
        BilletFileManager.removeAllBillets(this);
    }

    public void logOut(boolean withDialog)
    {
        if (!showingLogOutDialog)
        {
            //Des-suscribirse de las notificaciones de chipRED
            FirebaseMessaging.getInstance().unsubscribeFromTopic(idClient);

            //Eliminar la "key" que contiene el valor del estado de la sesion
            sharedPreferencesManager.remove(LOGGED);

            //Eliminar todos los billetes
            deleteAllBillets();

            if (withDialog)
            {
                GenericDialog logOutDialog = new GenericDialog(
                        "Sesión terminada",
                        "El dispositivo no está autorizado",
                        new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                showingLogOutDialog = false;
                                restartActivity();
                            }
                        },
                        null,
                        this
                );
                logOutDialog.setPositiveText("Ok");
                logOutDialog.setCancelable(false);
                logOutDialog.show();

                showingLogOutDialog = true;
            }
            else
            {
                restartActivity();
            }
        }
    }

    public void restartActivity()
    {
        //Reiniciar actividad
        finish();
        startActivity(getIntent());
    }
}
