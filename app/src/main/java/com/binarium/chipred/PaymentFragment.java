package com.binarium.chipred;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PaymentFragment extends Fragment
{
    private Context context;

    private SharedPreferencesManager sharedPreferencesManager;
    private String clientId, country;
    private ChipRedManager chipRedManager;
    private ProgressDialog progressDialog;
    private ArrayList<Payment> payments;
    private SwipeRefreshLayout swipeLayoutToUpdate;
    private ImageView noServer;
    private RecyclerView rv;

    public PaymentFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState)
    {
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ((MainActivity) getActivity()).setActionBarTitle(
                getActivity().getResources().getString(R.string.paymentsNvItem)
        );

        setHasOptionsMenu(true);

        context = ((MainActivity) getActivity()).getBaseContext();
        sharedPreferencesManager = ((MainActivity) getActivity()).sharedPreferencesManager;
        clientId = sharedPreferencesManager.getStringFromSP("id_cliente", "");
        country = sharedPreferencesManager.getStringFromSP("pais", "");

        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived()
        {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN)
            {
                //Evaluar actividad padre
                if (ChipREDConstants.checkNullParentActivity(getActivity(),
                        PaymentFragment.this))
                    return;

                if (crMessage.equals(ChipRedManager.LOGOUT))
                {
                    ((MainActivity) getActivity()).logOut(true);
                }
                else
                {
                    noServer.setVisibility(View.GONE);

                    Snackbar.make(getActivity().findViewById(android.R.id.content),
                            crMessage,
                            Snackbar.LENGTH_LONG).show();
                    //Toast.makeText(context, crMessage, Toast.LENGTH_LONG).show();
                    stopRefreshView();
                }
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN)
            {
                //Evaluar actividad padre
                if (ChipREDConstants.checkNullParentActivity(getActivity(),
                        PaymentFragment.this))
                    return;

                if (errorMessage.contains("Error al consultar abonos"))
                {
                    GenericDialog genericDialog = new GenericDialog(
                            "Error de conexión",
                            errorMessage,
                            new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    //nada... :)
                                }
                            },
                            null,
                            getActivity()
                    );
                    genericDialog.show();
                    noServer.setVisibility(View.VISIBLE);
                }
                else
                {
                    listPayments(null);
                }
                stopRefreshView();
                if (progressDialog != null) progressDialog.dismiss();
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN)
            {
                //Evaluar actividad padre
                if (ChipREDConstants.checkNullParentActivity(getActivity(),
                        PaymentFragment.this))
                    return;

                noServer.setVisibility(View.GONE);
                listPayments(response);
                stopRefreshView();
            }
        }, context);

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_payment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        noServer = getActivity().findViewById(R.id.noServerToPayments);
        setRefreshView();

        progressDialog = ProgressDialog.show(getActivity(), "Mis abonos",
                "Obteniento lista de abonos...", true, false);

        chipRedManager.askPayments(clientId, true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.fragment_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.help:
                Intent intent = new Intent(getActivity(), Tutorial5.class);
                startActivity(intent);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void setRefreshView()
    {
        final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) getActivity()
                .findViewById(R.id.swipePayments);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                swipeLayoutToUpdate = swipeRefreshLayout;
                chipRedManager.askPayments(clientId, true);
            }
        });
    }

    public void stopRefreshView()
    {
        if (swipeLayoutToUpdate != null)
        {
            swipeLayoutToUpdate.setRefreshing(false);
            swipeLayoutToUpdate = null;
        }
    }

    private void listPayments(JSONObject response)
    {
        payments = new ArrayList<>();

        if (response != null) getPayments(response);

        readyToListPayments();

        if (progressDialog != null)
        {
            progressDialog.dismiss();
        }
    }

    private void readyToListPayments()
    {
        TextView emptyTv = (TextView) getActivity().findViewById(R.id.emptyPayments);
        rv = (RecyclerView) getActivity().findViewById(R.id.paymentsRv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);

        if (!payments.isEmpty())
        {
            final PaymentArrayAdapter arrayAdapter = new PaymentArrayAdapter(payments, context);

            arrayAdapter.setOnClickListener(new DebouncedOnClickListener(1000)
            {
                @Override
                public void onDebouncedClick(View v)
                {
                    Payment payment = payments.get(rv.getChildAdapterPosition(v));
                    showPaymentDetails(payment);
                }
            });

            rv.setAdapter(arrayAdapter);
            rv.setLayoutManager(linearLayoutManager);
            rv.setHasFixedSize(true);

            rv.setVisibility(View.VISIBLE);
            emptyTv.setVisibility(View.GONE);
        }
        else
        {
            rv.setVisibility(View.GONE);
            emptyTv.setVisibility(View.VISIBLE);
        }
    }

    private void getPayments(JSONObject response)
    {
        JSONArray paymentArray;

        try
        {
            //Obtener arreglo de abonos
            paymentArray = response.getJSONArray("data");

            for (int i = 0; i < paymentArray.length(); i++)
            {
                //Analizar cada consumo y añadirlo a la lista
                JSONObject payment = paymentArray.getJSONObject(i);

                double saldoAnterior, saldoActual, costo;
                String fechaHora, hora, fecha;
                String station, account;

                fechaHora = payment.getString("fecha");
                hora = fechaHora.substring(fechaHora.indexOf(' ') + 1);
                fecha = fechaHora.substring(0, fechaHora.indexOf(' '));
                costo = payment.getDouble("cantidad");
                saldoAnterior = payment.getDouble("saldo_anterior");
                saldoActual = payment.getDouble("saldo_nuevo");

                account = payment.getJSONObject("cliente")
                        .getJSONObject("cuentas")
                        .getString("descripcion");

                station = payment.getJSONObject("estacion").getString("nombre_estacion");

                Payment mPayment = new Payment(costo, hora, fecha);
                mPayment.setAccount(account);
                mPayment.setStation(station);
                mPayment.setLastBalance(saldoAnterior);
                mPayment.setNewBalance(saldoActual);
                mPayment.setCountry(country);

                payments.add(mPayment);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    private void showPaymentDetails(Payment payment)
    {
        Activity activity = getActivity();
        LayoutInflater inflater = getLayoutInflater();

        View alertDialog = inflater.inflate(R.layout.payment_detail_list, null);
        RecyclerView paymentDetails = alertDialog.findViewById(R.id.payment_detail_list);

        //Agregar contexto al objeto "Payment"
        payment.setContext(activity);

        DetailArrayAdapter detailArrayAdapter = new DetailArrayAdapter(
                payment.getPaymentDetails(), activity
        );
        paymentDetails.setAdapter(detailArrayAdapter);
        paymentDetails.setLayoutManager(new LinearLayoutManager(activity));
        paymentDetails.setHasFixedSize(true);

        AlertDialog.Builder alertadd = new AlertDialog.Builder(activity);
        alertadd.setView(alertDialog);
        alertadd.setNegativeButton("Cerrar", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });
        alertadd.show();
    }
}
