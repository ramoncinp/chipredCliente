package com.binarium.chipred;

import android.content.Context;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Payment
{
    private double amount;
    private double lastBalance;
    private double newBalance;
    private String time;
    private String date;
    private String station;
    private String account;
    private String country;
    private Context context;

    public Payment(double amount, String time, String date)
    {
        this.amount = amount;
        this.time = time;
        this.date = date;
    }

    public void setAmount(double amount)
    {
        this.amount = amount;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public void setTime(String time)
    {
        this.time = time;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public void setStation(String station)
    {
        this.station = station;
    }

    public void setAccount(String account)
    {
        this.account = account;
    }

    public void setLastBalance(double lastBalance)
    {
        this.lastBalance = lastBalance;
    }

    public void setNewBalance(double newBalance)
    {
        this.newBalance = newBalance;
    }

    public double getAmount()
    {
        return amount;
    }

    public String getTime()
    {
        return time;
    }

    public String getDate()
    {
        return date;
    }

    public String getStation()
    {
        return station;
    }

    public String getAccount()
    {
        return account;
    }

    public double getLastBalance()
    {
        return lastBalance;
    }

    public double getNewBalance()
    {
        return newBalance;
    }

    public void setContext(Context context)
    {
        this.context = context;
    }

    public ArrayList<DetailItem> getPaymentDetails()
    {
        String currencyCode;
        DecimalFormat decimalFormat;
        if (country.equals("mexico"))
        {
            currencyCode = ChipREDConstants.MXN_CURRENCY_ISO;
            decimalFormat = ChipREDConstants.MX_AMOUNT_FORMAT;
        }
        else
        {
            currencyCode = ChipREDConstants.CRC_CURRENCY_ISO;
            decimalFormat = ChipREDConstants.CR_AMOUNT_FORMAT;
        }

        ArrayList<DetailItem> detailitems = new ArrayList<>();
        //Cantidad del abono
        DetailItem paymentQuantity = new DetailItem();
        paymentQuantity.setKey("Cantidad");
        paymentQuantity.setValue(decimalFormat.format(this.amount) + " " + currencyCode);
        paymentQuantity.setImageResource(context.getResources().getIdentifier("ic_coins",
                "mipmap", context.getPackageName()));
        detailitems.add(paymentQuantity);

        //Saldo Nuevo
        DetailItem newBalance = new DetailItem();
        newBalance.setKey("Saldo nuevo");
        newBalance.setValue(decimalFormat.format(this.newBalance) + " " + currencyCode);
        if (country.equals("mexico"))
        {
            newBalance.setImageResource(context.getResources().getIdentifier("ic_payment",
                    "drawable", context.getPackageName()));
        }
        else
        {
            newBalance.setImageResource(context.getResources().getIdentifier("ic_colon_crc",
                    "drawable", context.getPackageName()));
        }

        detailitems.add(newBalance);

        //Saldo Anterior
        DetailItem lastBalance = new DetailItem();
        lastBalance.setKey("Saldo anterior");
        lastBalance.setValue(decimalFormat.format(this.lastBalance) + " " + currencyCode);
        if (country.equals("mexico"))
        {
            lastBalance.setImageResource(context.getResources().getIdentifier("ic_money_black",
                    "drawable", context.getPackageName()));
        }
        else
        {
            lastBalance.setImageResource(context.getResources().getIdentifier("ic_colon_crc",
                    "drawable", context.getPackageName()));
        }
        detailitems.add(lastBalance);

        //FechaHora
        DetailItem date = new DetailItem();
        date.setKey("Fecha");
        date.setValue(this.date + " - " + this.time);
        date.setImageResource(context.getResources().getIdentifier("ic_date_black", "drawable",
                context.getPackageName()));
        detailitems.add(date);

        //Cuenta
        DetailItem account = new DetailItem();
        account.setKey("Cuenta");
        account.setValue(this.account);
        account.setImageResource(context.getResources().getIdentifier("ic_account_type",
                "mipmap", context.getPackageName()));
        detailitems.add(account);

        //Estación
        DetailItem station = new DetailItem();
        station.setKey("Estación");
        station.setValue(this.station);
        station.setImageResource(context.getResources().getIdentifier("ic_station", "drawable",
                context.getPackageName()));
        detailitems.add(station);

        return detailitems;
    }
}
