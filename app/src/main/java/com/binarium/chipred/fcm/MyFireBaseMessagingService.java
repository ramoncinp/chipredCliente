package com.binarium.chipred.fcm;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import android.util.Log;

import com.binarium.chipred.ChipREDConstants;
import com.binarium.chipred.MainActivity;
import com.binarium.chipred.R;
import com.binarium.chipred.SharedPreferencesManager;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.ArrayList;
import java.util.Map;

public class MyFireBaseMessagingService extends FirebaseMessagingService
{
    public static final String TAG = "ChipREDNofications";

    private boolean notificationChannel = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O;
    private boolean bundledNotifications = Build.VERSION.SDK_INT >= 24;

    private static final int CHIP_RED = 0;
    private static final int PURCHASES_GROUP = 1;
    private static final int PAYMENTS_GROUP = 2;
    private static final int SIGN_IN_GROUP = 3;
    private static final int GENERAL_GROUP = 4;

    private String username;

    private NotificationManagerCompat notificationManager;
    private SharedPreferencesManager sharedPreferencesManager;

    @Override
    public void onNewToken(@NonNull String s)
    {
        super.onNewToken(s);
        Log.v("NewToken", s);
    }

    /**
     * @param remoteMessage: Objeto que contiene la notificacion recibida
     *                       <p>
     *                       El servidor hace un request con la siguiente estructura:
     *                       <p>
     *                       URL: https://fcm.googleapis.com/fcm/send
     *                       Headers:
     *                       Content-type: application/json
     *                       Authorization: key=<serverKey>
     *                       <p>
     *                       Body:
     *                       {
     *                       "to": "/topics/<idCliente>",
     *                       "data":{
     *                       "title":"<TituloNotificacion>",
     *                       "text":"<Mensaje>"
     *                       }
     *                       }
     */

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage)
    {
        super.onMessageReceived(remoteMessage);
        Log.d(TAG, "Mesaje recibido: " + remoteMessage.getData().toString());
        Map<String, String> data = remoteMessage.getData();

        String notificationTitle = data.get("title");
        String notificationDesc = data.get("text");

        if (notificationDesc != null && notificationTitle != null)
        {
            sharedPreferencesManager = new SharedPreferencesManager(this);
            username = sharedPreferencesManager.getStringFromSP("username", "");
            notificationManager = NotificationManagerCompat.from(this);

            if (bundledNotifications)
            {
                showNotification(notificationTitle, notificationDesc);
            }
            else
            {
                showSimpleNotification(notificationTitle, notificationDesc);
            }
        }

        Log.d("Notificaciones", notificationTitle);
        Log.d("Notificaciones", notificationDesc);
    }

    private void showNotification(String title, String body)
    {
        ArrayList<String> notifications;

        if (!sharedPreferencesManager.isContained("notifications"))
        {
            notifications = new ArrayList<>();
        }
        else
        {
            notifications = sharedPreferencesManager.getStringSet("notifications");
        }
        //Agregar notificacion actual a la lista
        notifications.add(title + ": " + body);

        NotificationCompat.Builder summaryBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.icon_chipred).setAutoCancel(false).setShowWhen(true)
                .setWhen(System.currentTimeMillis()).setGroupSummary(true).setSubText(username)
                .setGroup("chipRed");

        if (notifications.size() == 1)
        {
            summaryBuilder.setContentTitle(title);
            summaryBuilder.setContentText(body);
            summaryBuilder.setContentIntent(setIntentToSummary(title, false));
        }
        else
        {
            summaryBuilder.setContentTitle(String.valueOf(notifications.size()) + " nuevos " +
                    "movimientos");
            summaryBuilder.setContentText("Deslice hacia abajo para ver el detalle");
            summaryBuilder.setContentIntent(setIntentToSummary(title, true));
        }

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

        inboxStyle.setBigContentTitle("Movimientos: ");
        for (String notification : notifications)
        {
            inboxStyle.addLine(notification);
        }

        summaryBuilder.setStyle(inboxStyle);
        summaryBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE |
                Notification.DEFAULT_LIGHTS);
        notificationManager.notify(CHIP_RED, summaryBuilder.build());

        //Obtener el dato de que tipo de notificacion es y contar las de su mismo tipo
        ArrayList<String> sameNotificationList = new ArrayList<>();
        String notificationType = getNotificationTypeString(title);

        for (String notification : notifications)
        {
            if (getNotificationConcept(title) == getNotificationConcept(notification))
            {
                sameNotificationList.add(notification);
            }
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this).setSmallIcon(R
                .drawable.icon_chipred).setGroup("chipRed");

        builder.setContentIntent(getPendingIntent(title));
        builder.setLargeIcon(drawableToBitmap(getNotificationTypeDrawable(title)));

        NotificationCompat.InboxStyle inInboxStyle = new NotificationCompat.InboxStyle();

        if (sameNotificationList.size() == 1)
        {
            builder.setContentTitle(title);
            builder.setContentText(body);

            inInboxStyle.addLine(body);
        }
        else
        {
            builder.setContentTitle(String.valueOf(sameNotificationList.size()) + " " +
                    notificationType);
            builder.setContentText("Deslice hacia abajo para ver el detalle");

            inInboxStyle.setBigContentTitle(String.valueOf(sameNotificationList.size()) + " " +
                    notificationType);

            for (String notification : sameNotificationList)
            {
                inInboxStyle.addLine(notification.substring(notification.indexOf(":") + 2));
            }
        }
        builder.setStyle(inInboxStyle);
        if (notificationChannel) builder.setChannelId(ChipREDConstants.GENERAL_NOTIFICATIONS_ID);

        notificationManager.notify(getNotificationConcept(title), builder.build());
        sharedPreferencesManager.putStringSet("notifications", notifications);
    }

    public void showSimpleNotification(String title, String body)
    {
        ArrayList<String> notifications;
        if (!sharedPreferencesManager.isContained("notifications"))
        {
            //Crear una nueva lista de notificaciones pendientes por revisar
            notifications = new ArrayList<>();
        }
        else
        {
            //Agregar notificacion a la lista de pendientes por cancelar
            notifications = sharedPreferencesManager.getStringSet
                    ("notifications");
        }

        //Agregar notificacion actual a la lista
        notifications.add(title + ": " + body);
        sharedPreferencesManager.putStringSet("notifications", notifications);

        Notification notification = new NotificationCompat.Builder(this, ChipREDConstants
                .GENERAL_NOTIFICATIONS_ID).setSmallIcon(R.drawable.icon_chipred).setContentTitle
                (title).setContentText(body).setAutoCancel(false).setShowWhen(true).setWhen
                (System.currentTimeMillis()).setLargeIcon(drawableToBitmap
                (getNotificationTypeDrawable(title))).setStyle(new NotificationCompat
                .BigTextStyle().bigText(body).setBigContentTitle(getNotificationTypeString(title)
        ).setSummaryText(username)).setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(getPendingIntent(title)).setDefaults(Notification.DEFAULT_SOUND
                        | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS).build();

        int counter = sharedPreferencesManager.getIntFromSP("SingleNotifications", 5);
        notificationManager.notify(counter, notification);
        sharedPreferencesManager.putIntInSP("SingleNotifications", counter + 1);
    }

    private Drawable getNotificationTypeDrawable(String title)
    {
        if (getNotificationConcept(title) == PURCHASES_GROUP)
        {
            return getResources().getDrawable(R.drawable.ic_shopping_cart_black_24dp);
        }
        else if (getNotificationConcept(title) == PAYMENTS_GROUP)
        {
            return getResources().getDrawable(R.drawable.ic_payment);
        }
        else if (getNotificationConcept(title) == SIGN_IN_GROUP)
        {
            return getResources().getDrawable(R.drawable.ic_user);
        }
        else
        {
            return getResources().getDrawable(R.drawable.icon_chipred);
        }
    }

    private String getNotificationTypeString(String title)
    {
        if (getNotificationConcept(title) == PURCHASES_GROUP)
        {
            return "Consumos";
        }
        else if (getNotificationConcept(title) == PAYMENTS_GROUP)
        {
            return "Abonos";
        }
        else if (getNotificationConcept(title) == SIGN_IN_GROUP)
        {
            return "Inicios de Sesión";
        }
        else
        {
            return "Mensajes";
        }
    }

    private PendingIntent getPendingIntent(String title)
    {
        int requestCode = getNotificationConcept(title) + 1000;
        Intent resultIntent = new Intent(this, MainActivity.class);
        resultIntent.putExtra("fragment", requestCode);
        resultIntent.setAction("fragment");

        return PendingIntent.getActivity(this, requestCode, resultIntent, PendingIntent
                .FLAG_UPDATE_CURRENT);
    }

    public PendingIntent setIntentToSummary(String notificationTitle, boolean defaultFragment)
    {
        int requestCode;

        if (defaultFragment)
        {
            requestCode = GENERAL_GROUP;
        }
        else
        {
            requestCode = getNotificationConcept(notificationTitle) + 1000;
        }

        Intent resultIntent = new Intent(this, MainActivity.class);
        resultIntent.putExtra("fragment", requestCode);
        resultIntent.setAction("fragment");

        return PendingIntent.getActivity(this, requestCode, resultIntent, PendingIntent
                .FLAG_UPDATE_CURRENT);
    }

    public int getNotificationConcept(String keyWord)
    {
        if (keyWord.contains("Nuevo inicio"))
        {
            return SIGN_IN_GROUP;
        }
        else if (keyWord.contains("Nuevo Abono"))
        {
            return PAYMENTS_GROUP;
        }
        else if (keyWord.contains("Nuevo Consumo"))
        {
            return PURCHASES_GROUP;
        }
        else
        {
            return GENERAL_GROUP;
        }
    }

    public static Bitmap drawableToBitmap(Drawable drawable)
    {
        if (drawable instanceof BitmapDrawable)
        {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable
                .getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }
}
