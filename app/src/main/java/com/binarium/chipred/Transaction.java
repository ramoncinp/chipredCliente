package com.binarium.chipred;

import java.util.Date;

public class Transaction
{
    public static final int CONSUMO = 0;
    public static final int ABONO = 1;

    private Date date;
    private double amount;
    private double newBalance;
    private double LastBalance;
    private int type;
    private String dateTime;
    private String stationNumber;
    private String vehiclePlates;
    private String vehiclePlatesUrl;

    public Transaction()
    {
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public double getAmount()
    {
        return amount;
    }

    public void setAmount(double amount)
    {
        this.amount = amount;
    }

    public double getNewBalance()
    {
        return newBalance;
    }

    public void setNewBalance(double newBalance)
    {
        this.newBalance = newBalance;
    }

    public double getLastBalance()
    {
        return LastBalance;
    }

    public void setLastBalance(double lastBalance)
    {
        LastBalance = lastBalance;
    }

    public String getDateTime()
    {
        return dateTime;
    }

    public void setDateTime(String dateTime)
    {
        this.dateTime = dateTime;
    }

    public String getStationNumber()
    {
        return stationNumber;
    }

    public void setStationNumber(String stationNumber)
    {
        this.stationNumber = stationNumber;
    }

    public int getType()
    {
        return type;
    }

    public void setType(int type)
    {
        this.type = type;
    }

    public String getVehiclePlates()
    {
        return vehiclePlates;
    }

    public void setVehiclePlates(String vehiclePlates)
    {
        this.vehiclePlates = vehiclePlates;
    }

    public String getVehiclePlatesUrl()
    {
        return vehiclePlatesUrl;
    }

    public void setVehiclePlatesUrl(String vehiclePlatesUrl)
    {
        this.vehiclePlatesUrl = vehiclePlatesUrl;
    }
}
