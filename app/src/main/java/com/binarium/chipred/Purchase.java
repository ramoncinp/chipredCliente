package com.binarium.chipred;

import android.content.Context;

import java.util.ArrayList;

public class Purchase
{
    private double volume;
    private double amount;
    private String time;
    private String date;
    private String fuelType;
    private String productColor;
    private String station;
    private String account;
    private String deviceId;
    private String country;
    private String vehiclePlates;
    private String vehiclePlatesImage;

    private ArrayList<ExtraProduct> extraProducts;
    private Context context;

    public Purchase(double volume, double amount, String time, String date)
    {
        this.volume = volume;
        this.amount = amount;
        this.time = time;
        this.date = date;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public void setVolume(double volume)
    {
        this.volume = volume;
    }

    public void setAmount(double amount)
    {
        this.amount = amount;
    }

    public void setTime(String time)
    {
        this.time = time;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public void setFuelType(String fuelType)
    {
        this.fuelType = fuelType;
    }

    public void setStation(String station)
    {
        this.station = station;
    }

    public String getDeviceId()
    {
        return deviceId;
    }

    public String getAccount()
    {
        return account;
    }

    public void setAccount(String account)
    {
        this.account = account;
    }

    public void setDeviceId(String deviceId)
    {
        this.deviceId = deviceId;
    }

    public double getVolume()
    {
        return volume;
    }

    public double getAmount()
    {
        return amount;
    }

    public String getTime()
    {
        return time;
    }

    public String getDate()
    {
        return date;
    }

    public String getFuelType()
    {
        return fuelType;
    }

    public String getStation()
    {
        return station;
    }

    public void setContext(Context context)
    {
        this.context = context;
    }

    public ArrayList<ExtraProduct> getExtraProducts()
    {
        return extraProducts;
    }

    public double getExtraProductsTotal()
    {
        double total = 0;
        for (ExtraProduct extraProduct : extraProducts)
        {
            total += extraProduct.getSubTotal();
        }
        return total;
    }

    public void setExtraProducts(ArrayList<ExtraProduct> extraProducts)
    {
        this.extraProducts = extraProducts;
    }

    public String getProductColor()
    {
        return productColor;
    }

    public void setProductColor(String productColor)
    {
        this.productColor = productColor;
    }

    public String getVehiclePlates()
    {
        return vehiclePlates;
    }

    public void setVehiclePlates(String vehiclePlates)
    {
        this.vehiclePlates = vehiclePlates;
    }

    public String getVehiclePlatesImage()
    {
        return vehiclePlatesImage;
    }

    public void setVehiclePlatesImage(String vehiclePlatesImage)
    {
        this.vehiclePlatesImage = vehiclePlatesImage;
    }

    public ArrayList<DetailItem> getPurchaseDetails()
    {
        String currencyCode;
        if (country.equals("mexico"))
        {
            currencyCode = ChipREDConstants.MXN_CURRENCY_ISO;
        }
        else
        {
            currencyCode = ChipREDConstants.CRC_CURRENCY_ISO;
        }

        ArrayList<DetailItem> detailitems = new ArrayList<>();
        //Volumen
        DetailItem volume = new DetailItem();
        volume.setKey("Cantidad");
        volume.setValue(ChipREDConstants.LITERS_FORMAT.format(this.volume) + " L");
        volume.setImageResource(context.getResources().getIdentifier("ic_bought_volume",
                "drawable", context.getPackageName()));
        detailitems.add(volume);

        //Costo
        DetailItem amount = new DetailItem();
        String amountDrawable;
        if (country.equals("mexico"))
        {
            amountDrawable = "ic_payment";
            amount.setValue(ChipREDConstants.MX_AMOUNT_FORMAT.format(this.amount) + " " + currencyCode);
        }
        else
        {
            amountDrawable = "ic_colon_crc";
            amount.setValue(ChipREDConstants.CR_AMOUNT_FORMAT.format(this.amount) + " " + currencyCode);
        }
        amount.setKey("Costo");
        amount.setImageResource(context.getResources().getIdentifier(amountDrawable, "drawable",
                context.getPackageName()));
        detailitems.add(amount);

        //Producto
        DetailItem product = new DetailItem();
        product.setKey("Producto");
        product.setValue(this.fuelType);
        product.setImageResource(context.getResources().getIdentifier("ic_dispenser_black",
                "drawable", context.getPackageName()));
        detailitems.add(product);

        //FechaHora
        DetailItem date = new DetailItem();
        date.setKey("Fecha");
        date.setValue(this.date + " - " + this.time);
        date.setImageResource(context.getResources().getIdentifier("ic_date_black", "drawable",
                context.getPackageName()));
        detailitems.add(date);

        //Cuenta
        DetailItem account = new DetailItem();
        account.setKey("Cuenta");
        account.setValue(this.account);
        account.setImageResource(context.getResources().getIdentifier("ic_account_type",
                "mipmap", context.getPackageName()));
        detailitems.add(account);

        //Estación
        DetailItem station = new DetailItem();
        station.setKey("Estación");
        station.setValue(this.station);
        station.setImageResource(context.getResources().getIdentifier("ic_station", "drawable",
                context.getPackageName()));
        detailitems.add(station);

        //Id de dispositivo, si esque existe
        DetailItem deviceIdItem = new DetailItem();
        deviceIdItem.setKey("Id de Dispositivo");
        deviceIdItem.setValue(this.deviceId == null || this.deviceId.isEmpty() ? "Billete " +
                "impreso" : this.deviceId);
        deviceIdItem.setImageResource(context.getResources().getIdentifier("ic_user",
                "drawable", context.getPackageName()));
        detailitems.add(deviceIdItem);

        //Placas de vehículo, si esque existe
        DetailItem vehiclePlates = new DetailItem();
        vehiclePlates.setKey("Vehículo");
        vehiclePlates.setValue(this.vehiclePlates == null || this.vehiclePlates.isEmpty() ? "Gen" +
                "érico" : this.vehiclePlates);
        vehiclePlates.setImageResource(context.getResources().getIdentifier("ic_car",
                "drawable", context.getPackageName()));
        detailitems.add(vehiclePlates);

        return detailitems;
    }
}
