package com.binarium.chipred;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class ProductPurchasesList
{
    private ArrayList<Float> purchases;
    private ArrayList<String> dates;
    private String productName;
    private String country;
    private String currencySymbol;
    private DecimalFormat decimalFormat;

    public ProductPurchasesList(String productName)
    {
        this.productName = productName;
        purchases = new ArrayList<>();
        dates = new ArrayList<>();
    }

    public void addPurchase(float amount, String date)
    {
        purchases.add(amount);
        dates.add(date);
    }

    public StringBuilder getPurchases()
    {
        StringBuilder data = new StringBuilder();

        for (int i = 0; i < purchases.size(); i++)
        {
            data.append("• ");
            data.append(currencySymbol);
            data.append(decimalFormat.format(purchases.get(i)));
            data.append("\t\t\t");

            data.append("\n");
        }

        return data;
    }

    public String getProductName()
    {
        return productName;
    }

    public int getSize()
    {
        return purchases.size();
    }

    public String getPurchaseAmount(int index)
    {
        return decimalFormat.format(purchases.get(index));
    }

    public String getDate(int index)
    {
        return dates.get(index);
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
        if (country.equals("mexico"))
        {
            currencySymbol = "$";
            decimalFormat = ChipREDConstants.MX_AMOUNT_FORMAT;
        }
        else
        {
            currencySymbol = "₡";
            decimalFormat = ChipREDConstants.CR_AMOUNT_FORMAT;
        }
    }
}
