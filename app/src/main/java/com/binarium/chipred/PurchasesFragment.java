package com.binarium.chipred;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.material.snackbar.Snackbar;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class PurchasesFragment extends Fragment
{
    /**
     * Variables
     **/
    private boolean noPurchases;
    private boolean serverUnavailable;
    private boolean serviceAnswered;
    private boolean showList;
    private int selectedView;
    private String clientId, country;

    /**
     * Views
     **/
    private ConstraintLayout chartLayout;
    private ConstraintLayout pieLayout;
    private FloatingActionMenu floatingActionMenu;
    private ImageView noServer;
    private ImageView arrowLeft;
    private ImageView arrowRight;
    private LinearLayout content;
    public Menu purchasesMenu;
    private RecyclerView rv;
    private TextView emptyTv;
    private TextView dateTitleTv;

    /**
     * Listas
     **/
    private ArrayList<Purchase> purchases;
    private HashMap<String, ArrayList<Purchase>> purchasesByProduct;

    /**
     * Objetos
     **/
    private Context context;
    private SharedPreferencesManager sharedPreferencesManager;
    private ChipRedManager chipRedManager;
    private ProgressDialog progressDialog;
    private SwipeRefreshLayout swipeLayoutToUpdate;

    /**
     * Graficas
     **/
    private LineChart chart;
    private PieChart pieChart;
    private DatePointer datePointer;

    private Handler indicateOptionHandler;
    private Runnable indicateMenuItem = new Runnable()
    {
        @Override
        public void run()
        {
            if (getActivity() != null)
            {
                Menu menu = ((MainActivity) getActivity()).navigationView.getMenu();
                MenuItem menuItem = menu.findItem(R.id.listpayments);

                boolean currentState = menuItem.isChecked();
                menuItem.setChecked(!currentState);

                indicateOptionHandler.postDelayed(indicateMenuItem, 500);
            }
        }
    };

    private final static int ANIMATION_DELAY = 800;

    public PurchasesFragment()
    {
        // Required empty public constructor
    }

    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

        setHasOptionsMenu(true);
        setRetainInstance(true);

        context = ((MainActivity) getActivity()).getBaseContext();
        sharedPreferencesManager = ((MainActivity) getActivity()).sharedPreferencesManager;
        clientId = sharedPreferencesManager.getStringFromSP("id_cliente", "");
        country = sharedPreferencesManager.getStringFromSP("pais", "");

        indicateOptionHandler = new Handler();
        datePointer = new DatePointer();

        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived()
        {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN)
            {
                //Evaluar actividad padre
                if (ChipREDConstants.checkNullParentActivity(getActivity(),
                        PurchasesFragment.this))
                    return;

                if (crMessage.equals(ChipRedManager.LOGOUT))
                {
                    ((MainActivity) getActivity()).logOut(true);
                }
                else
                {
                    noServer.setVisibility(View.GONE);
                    content.setVisibility(View.VISIBLE);
                    floatingActionMenu.setVisibility(View.VISIBLE);

                    //Toast.makeText(context, crMessage, Toast.LENGTH_SHORT).show();
                    Snackbar.make(getActivity().findViewById(android.R.id.content), crMessage,
                            Snackbar.LENGTH_SHORT).show();

                    stopRefreshView();
                    if (progressDialog != null) progressDialog.dismiss();

                    purchases = new ArrayList<>();
                    readyToListPurchases();

                    serverUnavailable = false;
                    serviceAnswered = true;
                }
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN)
            {
                //Evaluar actividad padre
                if (ChipREDConstants.checkNullParentActivity(getActivity(),
                        PurchasesFragment.this))
                    return;

                if (errorMessage.contains("Error al consultar consumos"))
                {
                    GenericDialog genericDialog = new GenericDialog("Error de conexión",
                            errorMessage, new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            //nada.. :)
                        }
                    }, null, getActivity());
                    genericDialog.show();
                    setServerUnavailableViews();
                }
                else
                {
                    listPurchases(null);
                }

                stopRefreshView();
                if (progressDialog != null) progressDialog.dismiss();
                serviceAnswered = true;
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN)
            {
                //Evaluar actividad padre
                if (ChipREDConstants.checkNullParentActivity(getActivity(),
                        PurchasesFragment.this))
                    return;

                noServer.setVisibility(View.GONE);
                content.setVisibility(View.VISIBLE);
                floatingActionMenu.setVisibility(View.VISIBLE);
                serverUnavailable = false;

                listPurchases(response);
                stopRefreshView();
                serviceAnswered = true;
            }
        }, context);

        askForPurchases();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_purchases, container, false);

        Bundle bundle = getArguments();
        if (bundle != null)
        {
            showList = getArguments().getBoolean("list");
        }
        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putInt("SelectedMenuItemIndex", selectedView);
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.purchase_menu, menu);
        purchasesMenu = menu;
        super.onCreateOptionsMenu(menu, inflater);

        if (showList)
        {
            onOptionsItemSelected(purchasesMenu.findItem(R.id.purchaseList));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (noPurchases)
        {
            chartLayout.setVisibility(View.GONE);
            pieLayout.setVisibility(View.GONE);
            rv.setVisibility(View.GONE);
            emptyTv.setVisibility(View.VISIBLE);

            if (item.getItemId() == R.id.help)
            {
                Intent intent = new Intent(getActivity(), Tutorial4.class);
                startActivity(intent);
            }

            return true;
        }
        else
        {
            floatingActionMenu.setVisibility(View.VISIBLE);
            // Handle item selection
            switch (item.getItemId())
            {
                case R.id.purchaseChart:
                    rv.setVisibility(View.GONE);
                    chartLayout.setVisibility(View.VISIBLE);
                    pieLayout.setVisibility(View.GONE);
                    setAxisYDesc();
                    chart.animateY(ANIMATION_DELAY);

                    selectedView = 0;

                    return true;

                case R.id.purchaseList:
                    chartLayout.setVisibility(View.GONE);
                    rv.setVisibility(View.VISIBLE);
                    pieLayout.setVisibility(View.GONE);

                    selectedView = 1;

                    return true;

                case R.id.purchasePieChart:
                    chartLayout.setVisibility(View.GONE);
                    rv.setVisibility(View.GONE);
                    pieLayout.setVisibility(View.VISIBLE);
                    pieChart.animateXY(ANIMATION_DELAY, ANIMATION_DELAY);

                    selectedView = 2;

                    return true;

                case R.id.help:
                    Intent intent = new Intent(getActivity(), Tutorial4.class);
                    startActivity(intent);

                    return true;

                default:
                    return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null)
        {
            selectedView = savedInstanceState.getInt("SelectedMenuItemIndex", 0);
            Log.d("SelectedView: ", String.valueOf(selectedView));
        }

        ((MainActivity) getActivity()).setActionBarTitle(getActivity().getResources().getString(R
                .string.purchasesNvItem));

        rv = getActivity().findViewById(R.id.purchasesRv);
        chart = getActivity().findViewById(R.id.chart);
        chartLayout = getActivity().findViewById(R.id.chartLayout);
        pieLayout = getActivity().findViewById(R.id.pieLayout);
        pieChart = getActivity().findViewById(R.id.pieChart);
        noServer = getActivity().findViewById(R.id.noServerToPurchases);
        emptyTv = getActivity().findViewById(R.id.emptyPurchases);
        swipeLayoutToUpdate = getActivity().findViewById(R.id.swipePurchases);
        dateTitleTv = getActivity().findViewById(R.id.purchase_date_title);
        floatingActionMenu = getActivity().findViewById(R.id.menu_purchase_dates);
        content = getActivity().findViewById(R.id.purchases_content);

        floatingActionMenu.setOnMenuToggleListener(new FloatingActionMenu.OnMenuToggleListener()
        {
            @Override
            public void onMenuToggle(boolean opened)
            {
                if (opened)
                {
                    FloatingActionButton weekButton = getActivity().findViewById(R.id.fabWeek);
                    FloatingActionButton monthButton = getActivity().findViewById(R.id.fabMonth);

                    int green = getActivity().getResources().getColor(R.color.green_money);
                    int darkGreen = getActivity().getResources().getColor(R.color.dark_green);
                    int red = getActivity().getResources().getColor(R.color.colorPrimary);
                    int darkRed = getActivity().getResources().getColor(R.color.colorPrimaryDark);

                    if (datePointer.getDateRangeType() == DatePointer.BYWEEK)
                    {
                        weekButton.setColorNormal(green);
                        weekButton.setColorPressed(darkGreen);
                        monthButton.setColorNormal(red);
                        monthButton.setColorPressed(darkRed);
                    }
                    else
                    {
                        weekButton.setColorNormal(red);
                        weekButton.setColorPressed(darkRed);
                        monthButton.setColorNormal(green);
                        monthButton.setColorPressed(darkGreen);
                    }
                }
            }
        });

        FloatingActionButton weekButton = getActivity().findViewById(R.id.fabWeek);
        weekButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (datePointer.getDateRangeType() != DatePointer.BYWEEK)
                {
                    datePointer.setDateRangeType(DatePointer.BYWEEK);
                    setDateTitle();
                    askForPurchases();
                }
                floatingActionMenu.close(true);
            }
        });
        FloatingActionButton monthButton = getActivity().findViewById(R.id.fabMonth);
        monthButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (datePointer.getDateRangeType() != DatePointer.BYMONTH)
                {
                    datePointer.setDateRangeType(DatePointer.BYMONTH);
                    setDateTitle();
                    askForPurchases();
                }
                floatingActionMenu.close(true);
            }
        });

        arrowLeft = getActivity().findViewById(R.id.arrowToLeft);
        arrowRight = getActivity().findViewById(R.id.arrowToRight);
        arrowLeft.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                datePointer.setOnePeriodBefore();
                setDateTitle();
                askForPurchases();
            }
        });

        arrowRight.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                datePointer.setOnePeriodLater();
                setDateTitle();
                askForPurchases();
            }
        });

        setDateTitle();
        setRefreshView();

        if (serverUnavailable)
        {
            setServerUnavailableViews();
        }
        else
        {
            if (purchasesMenu != null)
            {
                if (serviceAnswered)
                {
                    readyToListPurchases();
                    onOptionsItemSelected(purchasesMenu.getItem(selectedView));
                }
                else
                {
                    askForPurchases();
                }
            }
        }
    }

    public void askForPurchases()
    {
        progressDialog = ProgressDialog.show(getActivity(), "Mis consumos", "Obteniento " +
                "consumos...", true, false);

        chipRedManager.askPurchases(clientId, datePointer.getFirstDateOfCurrentPeriod(),
                datePointer.getLastDayOfCurrentPeriod(), true);
    }

    public void setRefreshView()
    {
        swipeLayoutToUpdate.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                //swipeLayoutToUpdate = swipeRefreshLayout;
                chipRedManager.askPurchases(clientId, datePointer.getFirstDateOfCurrentPeriod(),
                        datePointer.getLastDayOfCurrentPeriod(), true);
            }
        });
    }

    public void stopRefreshView()
    {
        if (swipeLayoutToUpdate != null)
        {
            swipeLayoutToUpdate.setRefreshing(false);
        }
    }

    public void setLitersChart()
    {
        final String[] chartXValuesArray;
        final HashMap<String, Float> totalLitersByProduct = new HashMap<>();
        float totalLiters = 0;

        if (datePointer.getDateRangeType() == DatePointer.BYWEEK)
        {
            //Definir valores de días de la semana
            chartXValuesArray = datePointer.getDaysOfTheWeek();
        }
        else
        {
            //Definir valores de días del mes
            chartXValuesArray = datePointer.getDaysOfTheMonth();
        }

        //Crear lista de "dataSets"
        List<ILineDataSet> dataSets = new ArrayList<>();

        //Crear objeto calendario
        Calendar c = Calendar.getInstance();

        //Obtener el número de la semana actual
        c.setTime(new Date());

        //Iterar en HashMap para crear cada lista de "Entries"
        Iterator it = purchasesByProduct.entrySet().iterator();
        while (it.hasNext())
        {
            //Obtener la lista de producto
            HashMap.Entry productList = (HashMap.Entry) it.next();
            String description = (String) productList.getKey();
            ArrayList<Purchase> selectedPurchases = purchasesByProduct.get(description);

            //Obtener color
            String color = selectedPurchases.get(0).getProductColor();

            //Creear una lista de "Entries"
            List<Entry> productData = new ArrayList<>();

            //Inicializar listas de datos
            for (int i = 0; i < datePointer.getDateRangeLength() + 1; i++)
            {
                productData.add(new Entry(i, 0));
            }

            //Iterar en cada consumo de la lista seleccionada
            for (Purchase purchase : selectedPurchases)
            {
                //Convertir litros de consumo a flotante
                Float purchaseVolume = (float) purchase.getVolume();
                //Obtener fecha de consumo en objeto de tiempo
                c.setTime(getPurchaseDate(purchase.getDate()));

                int chartXValue;
                if (datePointer.getDateRangeType() == DatePointer.BYWEEK)
                {
                    //Obtener número del día de la semana
                    chartXValue = c.get(Calendar.DAY_OF_WEEK);

                    //Ajuste para tomar el Domingo como el día 7 y los demás empezando desde el 1
                    if (chartXValue > 1) chartXValue -= 1;
                    else chartXValue = 7;
                }
                else
                {
                    //Obtener número del día del mes
                    chartXValue = c.get(Calendar.DAY_OF_MONTH);
                }

                //Agregar valor a la columna correspondiente
                addDataToChartColumn(purchaseVolume, productData, chartXValue);

                //Verificar que el hashmap de totales de litros ya tenga el producto
                if (!totalLitersByProduct.containsKey(description))
                {
                    //Inicializar valor
                    totalLitersByProduct.put(description, 0f);
                }

                //Acumular valor de volumen en su tipo de producto
                totalLitersByProduct.put(description, totalLitersByProduct.get(description) +
                        purchaseVolume);
                //Acumular el valor de volumen en total general
                totalLiters += purchaseVolume;
            }

            //Crear dataSet del tipo de producto y añadirlo a su lista
            LineDataSet productDataSet = new LineDataSet(productData, description);
            productDataSet.setColor(Color.parseColor(color));
            productDataSet.setLineWidth(5);
            productDataSet.setValueTextColor(Color.parseColor(color));
            productDataSet.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);

            dataSets.add(productDataSet);
        }

        LineData lineData = new LineData(dataSets);
        lineData.setDrawValues(true);
        lineData.setValueTextColor(R.color.black);
        lineData.setValueTextSize(10f);

        ChartTimeAxisFormatter formatter = new ChartTimeAxisFormatter();
        formatter.setFormatData(chartXValuesArray);

        XAxis xAxis = chart.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(formatter);
        xAxis.setTextSize(10f);

        YAxis left = chart.getAxisLeft();
        left.setAxisMinimum(0f);
        left.setTextSize(10f);

        YAxis right = chart.getAxisRight();
        right.setEnabled(false);

        chart.setData(lineData);

        Description description = new Description();
        description.setText("");
        chart.setDescription(description);
        chart.setLogEnabled(false);

        setAxisYDesc();
        chart.animateY(ANIMATION_DELAY);

        if (datePointer.getDateRangeType() == DatePointer.BYMONTH) chart.setVisibleXRange(31, 31);
        else chart.setVisibleXRange(7, 7);
        chart.setTouchEnabled(false);

        //Obtener String del total de litros
        String totalLitersString = "Total: " + ChipREDConstants.LITERS_FORMAT.format(totalLiters)
                + "L";

        //Mostrar valor en TextViews
        TextView totalLitersTv = getActivity().findViewById(R.id.totalLiters);
        SpannableString underLineText = new SpannableString("Total: " + ChipREDConstants
                .LITERS_FORMAT.format(totalLiters) + "L");

        underLineText.setSpan(new UnderlineSpan(), 0, underLineText.length(), 0);
        totalLitersTv.setText(totalLitersString);

        LinearLayout totalLitersButton = getActivity().findViewById(R.id.totalLitersLayout);
        totalLitersButton.setOnClickListener(new DebouncedOnClickListener(1000)
        {
            @Override
            public void onDebouncedClick(View v)
            {
                showTotalLitersByProduct(totalLitersByProduct);
            }
        });
    }

    private void showTotalLitersByProduct(HashMap<String, Float> totalLitersByProduct)
    {
        Activity activity = getActivity();
        LayoutInflater inflater = getLayoutInflater();

        View alertDialog = inflater.inflate(R.layout.total_liters_dialog, null);
        ListView listView = alertDialog.findViewById(R.id.total_liters_by_product_list);

        //Crear listas con los valores de cada producto
        ArrayList<String> keys = new ArrayList<>();
        ArrayList<String> values = new ArrayList<>();
        ArrayList<String> colors = new ArrayList<>();

        for (Map.Entry<String, Float> entry : totalLitersByProduct.entrySet())
        {
            float productLitersValue = entry.getValue();
            if (productLitersValue > 0)
            {
                //Obtener color del producto
                String color = purchasesByProduct.get(entry.getKey()).get(0).getProductColor();

                keys.add(entry.getKey());
                values.add(ChipREDConstants.LITERS_FORMAT.format(productLitersValue) + " L");
                colors.add(color);
            }
        }

        listView.setAdapter(new TwoRowAdapter(getContext(), keys, values, 16, colors));

        AlertDialog.Builder alertadd = new AlertDialog.Builder(activity);
        alertadd.setView(alertDialog);
        alertadd.setNegativeButton("Cerrar", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });

        alertadd.setTitle("Resumen");
        alertadd.show();
    }

    private void setMoneyPieChart()
    {
        //Obtener instancia del textView para mostrar el total
        TextView totalAmount = getActivity().findViewById(R.id.totalAmount);
        //Variable para acumular el total
        float total = 0;

        //Crear objeto calendario
        Calendar c = Calendar.getInstance();

        //Obtener el número de la semana actual
        c.setTime(new Date());

        //Crear lista para los colores de los productos
        List<Integer> colors = new ArrayList<>();

        //Crear lista de "Entries" para los productos
        PieDataSet set = new PieDataSet(null, "");

        Iterator it = purchasesByProduct.entrySet().iterator();
        while (it.hasNext())
        {
            //Crear variable para total de producto en dinero
            float productAmount = 0;

            //Obtener la lista de producto
            HashMap.Entry productList = (HashMap.Entry) it.next();

            //Obtener nombre del producto
            String productDesc = (String) productList.getKey();

            //Crear objeto de lista de consumos
            ProductPurchasesList productPurchasesList = new ProductPurchasesList(productDesc);

            //Especificar pais
            productPurchasesList.setCountry(country);

            //Iterar en todos los consumos de un tipo de producto
            for (Purchase purchase : purchasesByProduct.get(productDesc))
            {
                //Convertir litros de consumo a flotante
                Float purchaseVolume = (float) purchase.getAmount();
                //Obtener fecha de consumo en objeto de tiempo
                c.setTime(getPurchaseDate(purchase.getDate()));
                //Añadir a la lista
                productPurchasesList.addPurchase(purchaseVolume, purchase.getDate());
                //Añadir valor al totoal del producto
                productAmount += purchaseVolume;
            }

            //Añadir total de producto a total general
            total += productAmount;

            //Obtener color del producto
            String color = purchasesByProduct.get(productDesc).get(0).getProductColor();

            //Agregar a la lista de "Entries"
            PieEntry pieEntry = new PieEntry(productAmount, productDesc);
            pieEntry.setData(productPurchasesList);
            set.addEntry(pieEntry);

            //Agregar a la lista de colores
            colors.add(Color.parseColor(color));
        }

        //Agregar la lista completa de colores y demás parámetros
        set.setColors(colors);
        set.setValueTextSize(12f);
        set.setValueFormatter(new SliceFormat());

        PieData data = new PieData(set);
        data.setValueTextColor(getActivity().getResources().getColor(R.color.white));

        Description description = new Description();
        description.setText("");

        pieChart.setData(data);
        pieChart.setDescription(description);
        pieChart.setEntryLabelTextSize(0);

        String totalString;
        if (country.equals("mexico"))
        {
            totalString = "Total: $" + ChipREDConstants.MX_AMOUNT_FORMAT.format(total);
        }
        else
        {
            totalString = "Total: ₡" + ChipREDConstants.CR_AMOUNT_FORMAT.format(total);
        }

        totalAmount.setText(totalString);
        pieChart.animateXY(ANIMATION_DELAY, ANIMATION_DELAY);
        setGestureListener();
    }

    private void addDataToChartColumn(float value, List<Entry> productEntries, int listIndex)
    {
        Entry entry;

        entry = productEntries.get(listIndex);
        entry.setY(entry.getY() + value);
        productEntries.remove(listIndex);
        productEntries.add(listIndex, entry);
    }

    private void listPurchases(JSONObject response)
    {
        purchases = new ArrayList<>();

        if (response != null) getPurchases(response);
        readyToListPurchases();

        if (progressDialog != null)
        {
            progressDialog.dismiss();
        }
    }

    private void getPurchases(JSONObject response)
    {
        //Inicializar HashMap para agrupar consumos por producto
        purchasesByProduct = new HashMap<>();

        JSONArray purchaseArray;
        try
        {
            //Obtener arreglo de consumos
            purchaseArray = response.getJSONObject("data").getJSONArray("consumos");
            Log.d("Purchases", purchaseArray.toString(2));

            for (int i = 0; i < purchaseArray.length(); i++)
            {
                //Analizar cada consumo y añadirlo a la lista
                JSONObject purchase = purchaseArray.getJSONObject(i);

                double costo, cantidad;
                String fechaHora, hora, fecha, productColor;
                String fuelType, station, account, vehiclePlates, vehiclePlatesUrl;
                ArrayList<ExtraProduct> extraProducts = null;

                fechaHora = purchase.getString("fecha");
                hora = fechaHora.substring(fechaHora.indexOf(' ') + 1);
                fecha = fechaHora.substring(0, fechaHora.indexOf(' '));
                cantidad = purchase.getDouble("cantidad");
                costo = purchase.getDouble("costo");
                fuelType = purchase.getJSONObject("estacion").getJSONObject("productos")
                        .getString("descripcion");
                productColor = purchase.getJSONObject("estacion").getJSONObject("productos")
                        .getString("color");
                account = purchase.getJSONObject("cliente").getJSONObject("cuentas").getString
                        ("descripcion");
                station = purchase.getJSONObject("estacion").getString("nombre_estacion");

                //Obtener productos extra, si es que existen
                if (purchase.has("productos_extra"))
                {
                    JSONArray extraProductsArray = purchase.getJSONArray("productos_extra");
                    if (extraProductsArray.length() != 0)
                    {
                        extraProducts = new ArrayList<>();
                        for (int x = 0; x < extraProductsArray.length(); x++)
                        {
                            JSONObject extraProductJson = extraProductsArray.getJSONObject(x);
                            ExtraProduct extraProduct = new ExtraProduct();
                            extraProduct.setCode(extraProductJson.getString("codigo"));
                            extraProduct.setDescription(extraProductJson.getString("descripcion"));
                            extraProduct.setPrice(extraProductJson.getDouble("precio"));
                            extraProduct.setQuantity(extraProductJson.getInt("cantidad"));
                            extraProduct.setSubTotal(extraProductJson.getDouble("total"));
                            extraProducts.add(extraProduct);
                        }
                    }
                }

                Purchase mPurchase = new Purchase(cantidad, costo, hora, fecha);
                mPurchase.setFuelType(fuelType);
                mPurchase.setStation(station);
                mPurchase.setAccount(account);
                mPurchase.setExtraProducts(extraProducts);
                mPurchase.setProductColor(productColor);

                if (purchase.has("reservacion"))
                {
                    mPurchase.setVehiclePlates(purchase.getJSONObject("reservacion").getString(
                            "placas"));
                    mPurchase.setVehiclePlatesImage(purchase.getJSONObject("reservacion").getString(
                            "url_placas"));
                }

                if (purchase.has("id_dispositivo_genero_vale"))
                {
                    mPurchase.setDeviceId(purchase.getString("id_dispositivo_genero_vale"));
                }

                purchases.add(mPurchase);
                sortPurchaseByProduct(mPurchase);
            }

            //Obtener info de primer consumo
            String fechaHora, fecha;

            JSONObject firsPurchase = response.getJSONObject("data").getJSONObject
                    ("primer_consumo");
            fechaHora = firsPurchase.getString("fecha");
            fecha = fechaHora.substring(0, fechaHora.indexOf(' '));

            //Almacenar registro de primer venta en objeto "datePointer"
            datePointer.setFirstPurchaseDate(getPurchaseDate(fecha));
            setDateTitle();
            Log.d("FechaPrimerConsumo", fecha);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    private void readyToListPurchases()
    {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);

        if (!purchases.isEmpty())
        {
            final PurchaseArrayAdapter arrayAdapter = new PurchaseArrayAdapter(purchases, context);
            setLitersChart();
            setMoneyPieChart();

            arrayAdapter.setOnClickListener(new DebouncedOnClickListener(1000)
            {
                @Override
                public void onDebouncedClick(View v)
                {
                    Purchase purchase = purchases.get(rv.getChildAdapterPosition(v));
                    showPurcahseDetails(purchase);
                }
            });

            rv.setAdapter(arrayAdapter);
            rv.setLayoutManager(linearLayoutManager);
            rv.setHasFixedSize(true);

            noPurchases = false;
            onOptionsItemSelected(purchasesMenu.getItem(selectedView));
        }
        else
        {
            chartLayout.setVisibility(View.GONE);
            pieLayout.setVisibility(View.GONE);
            rv.setVisibility(View.GONE);
            emptyTv.setVisibility(View.VISIBLE);

            noPurchases = true;
        }
    }

    private void showPurcahseDetails(Purchase purchase)
    {
        Activity activity = getActivity();
        LayoutInflater inflater = getLayoutInflater();

        View alertDialog = inflater.inflate(R.layout.purchase_detail_list, null);
        RecyclerView detailList = alertDialog.findViewById(R.id.purchase_detail_list);
        RecyclerView extraProductList = alertDialog.findViewById(R.id
                .purchase_detail_extra_products_list);
        RecyclerView total = alertDialog.findViewById(R.id.purchase_detail_total_transaction);
        TextView extraProdcutsTv = alertDialog.findViewById(R.id.extra_product_text_view);
        TextView totalTransactionTv = alertDialog.findViewById(R.id.extra_product_total);

        //Agregar contexto al consumo
        purchase.setContext(activity);

        //Indicar pais
        purchase.setCountry(country);

        //Crear adaptador de detalles de consumo
        DetailArrayAdapter detailArrayAdapter = new DetailArrayAdapter(purchase
                .getPurchaseDetails(), activity);

        //Asignarlo al recyclerView
        detailList.setAdapter(detailArrayAdapter);
        detailList.setLayoutManager(new LinearLayoutManager(activity));
        detailList.setHasFixedSize(true);

        //Validar si hay placas por mostrar
        if (purchase.getVehiclePlatesImage() != null && !purchase.getVehiclePlatesImage().isEmpty())
        {
            TextView vehicleTitleTv = alertDialog.findViewById(R.id.vehicle_title_text_view);
            vehicleTitleTv.setVisibility(View.VISIBLE);

            ImageView vehiclePlatesIv = alertDialog.findViewById(R.id.vehicle_plates_image_view);
            vehiclePlatesIv.setVisibility(View.VISIBLE);

            // Mostrar imagen
            Picasso.with(getActivity()).load(purchase.getVehiclePlatesImage()).into(vehiclePlatesIv);
        }

        ArrayList<ExtraProduct> extraProducts = purchase.getExtraProducts();
        if (extraProducts != null)
        {
            ExtraProductArrayAdapter extraProductArrayAdapter = new ExtraProductArrayAdapter
                    (extraProducts, activity);

            extraProductList.setAdapter(extraProductArrayAdapter);
            extraProductList.setLayoutManager(new LinearLayoutManager(activity));
            extraProductList.setHasFixedSize(true);

            DetailItem totalTransactionitem = new DetailItem();
            totalTransactionitem.setKey("Total Transacción");

            //Definir moneda
            if (country.equals("mexico"))
            {
                totalTransactionitem.setValue(ChipREDConstants.MX_AMOUNT_FORMAT.format(purchase
                        .getExtraProductsTotal() + purchase.getAmount()) + " MXN");
                totalTransactionitem.setImageResource(activity.getResources().getIdentifier
                        ("ic_money_black", "drawable", activity.getPackageName()));
            }
            else
            {
                totalTransactionitem.setValue(ChipREDConstants.CR_AMOUNT_FORMAT.format(purchase
                        .getExtraProductsTotal() + purchase.getAmount()) + " CRC");
                totalTransactionitem.setImageResource(activity.getResources().getIdentifier
                        ("ic_colon_crc", "drawable", activity.getPackageName()));
            }


            //Crear adaptador de detalles de consumo
            ArrayList<DetailItem> totalList = new ArrayList<>();
            totalList.add(totalTransactionitem);
            DetailArrayAdapter totalAdapter = new DetailArrayAdapter(totalList, activity);
            total.setAdapter(totalAdapter);
            total.setLayoutManager(new LinearLayoutManager(activity));
            total.setHasFixedSize(true);

            extraProdcutsTv.setVisibility(View.VISIBLE);
            extraProductList.setVisibility(View.VISIBLE);
            totalTransactionTv.setVisibility(View.VISIBLE);
            total.setVisibility(View.VISIBLE);
        }

        AlertDialog.Builder alertadd = new AlertDialog.Builder(activity);
        alertadd.setView(alertDialog);
        alertadd.setNegativeButton("Cerrar", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });

        alertadd.setCancelable(false);
        alertadd.show();
    }

    public Date getPurchaseDate(String date)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date convertedDate;
        try
        {
            convertedDate = dateFormat.parse(date);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
            return null;
        }
        return convertedDate;
    }

    public void setAxisYDesc()
    {
        TextView axisYdescp = getActivity().findViewById(R.id.axisYdesc);
        String descp;
        descp = "Litros";

        axisYdescp.setText(descp);

        RotateAnimation rotateAnimation = (RotateAnimation) AnimationUtils.loadAnimation
                (getActivity(), R.anim.myanim);
        rotateAnimation.setFillAfter(true);
        axisYdescp.setAnimation(rotateAnimation);
    }

    public void setGestureListener()
    {
        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener()
        {
            @Override
            public void onValueSelected(Entry e, Highlight h)
            {
                ProductPurchasesList productPurchasesList = (ProductPurchasesList) e.getData();

                Activity activity = getActivity();
                LayoutInflater inflater = getLayoutInflater();

                View alertDialog = inflater.inflate(R.layout.product_purchases_list, null);
                TextView amounts = alertDialog.findViewById(R.id.productListAmount);
                TextView dates = alertDialog.findViewById(R.id.productListDates);

                for (int i = 0; i < productPurchasesList.getSize(); i++)
                {
                    if (country.equals("mexico")) amounts.append("• $");
                    else amounts.append("• ₡");

                    amounts.append(productPurchasesList.getPurchaseAmount(i));
                    amounts.append("\n");
                    dates.append(productPurchasesList.getDate(i));
                    dates.append("\n");
                }

                AlertDialog.Builder alertadd = new AlertDialog.Builder(activity);
                alertadd.setView(alertDialog);
                alertadd.setNegativeButton("Cerrar", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                    }
                });

                alertadd.setTitle("Transacciones - " + productPurchasesList.getProductName());
                alertadd.show();
            }

            @Override
            public void onNothingSelected()
            {

            }
        });
    }

    public class SliceFormat implements IValueFormatter
    {
        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex,
                                        ViewPortHandler viewPortHandler)
        {
            if (country.equals("mexico"))
            {
                return "$" + ChipREDConstants.MX_AMOUNT_FORMAT.format(value);
            }
            else
            {
                return "₡" + ChipREDConstants.CR_AMOUNT_FORMAT.format(value);
            }
        }
    }

    private void setDateTitle()
    {
        Date firstPurchaseDate = datePointer.getFirstPurchaseDate();
        StringBuilder dateTitle = new StringBuilder();

        if (datePointer.getDateRangeType() == DatePointer.BYWEEK)
        {
            dateTitle.append(datePointer.getFirstDateOfCurrentPeriodString());
            dateTitle.append(" - ");
            dateTitle.append(datePointer.getLastDateOfCurrentPeriodString());

            if (datePointer.isSameWeekNumber(new Date())) //Comparar con la semana actual
            {
                arrowRight.setVisibility(View.INVISIBLE);
            }
            else
            {
                arrowRight.setVisibility(View.VISIBLE);
            }

            if (firstPurchaseDate != null)
            {
                if (datePointer.isSameWeekNumber(firstPurchaseDate)) //Comparar con la semana del
                // primer consumo
                {
                    arrowLeft.setVisibility(View.INVISIBLE);
                }
                else
                {
                    arrowLeft.setVisibility(View.VISIBLE);
                }
            }
        }
        else
        {
            dateTitle.append(datePointer.getCurrentMonth());
            if (datePointer.isSameMonth(new Date())) //Comparar con la semana actual
            {
                arrowRight.setVisibility(View.INVISIBLE);
            }
            else
            {
                arrowRight.setVisibility(View.VISIBLE);
            }

            if (firstPurchaseDate != null)
            {
                if (datePointer.isSameMonth(firstPurchaseDate)) //Comparar con la semana del
                // primer consumo
                {
                    arrowLeft.setVisibility(View.INVISIBLE);
                }
                else
                {
                    arrowLeft.setVisibility(View.VISIBLE);
                }
            }
        }

        dateTitleTv.setText(dateTitle.toString());
    }

    public void setServerUnavailableViews()
    {
        noServer.setVisibility(View.VISIBLE);
        content.setVisibility(View.GONE);
        floatingActionMenu.setVisibility(View.GONE);

        serverUnavailable = true;
    }

    private void sortPurchaseByProduct(Purchase purchase)
    {
        //Obtener la descripción del tipo de combustible
        String purchaseProduct = purchase.getFuelType();

        //Evaluar si la lista de producto ya fue creada
        if (!purchasesByProduct.containsKey(purchaseProduct))
        {
            //Si no fue creada, crear una lista y añadirla al HashMap
            purchasesByProduct.put(purchaseProduct, new ArrayList<Purchase>());
        }

        //Añadir consumo a su respectiva lista
        purchasesByProduct.get(purchaseProduct).add(purchase);
    }
}
