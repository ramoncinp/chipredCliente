package com.binarium.chipred;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.widget.ImageView;
import android.view.WindowManager;

import com.squareup.picasso.Picasso;

import java.io.File;

public class FullScreenQR extends AppCompatActivity
{
    private String imagePath;
    private ImageView qrImage;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_screen_qr);

        Toolbar toolbar = findViewById(R.id.toolbar);

        if (getSupportActionBar() == null) setSupportActionBar(toolbar);

        imagePath = getIntent().getStringExtra("imagePath");

        qrImage = (ImageView) findViewById(R.id.full_qr);
        Picasso.with(this).load(new File(imagePath)).into(qrImage);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
/*
        qrImage.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
        qrImage.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
        qrImage.setAdjustViewBounds(false);
        qrImage.setScaleType(ImageView.ScaleType.FIT_XY);*/
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (getSupportActionBar() != null)
            getSupportActionBar().show();
    }
}
