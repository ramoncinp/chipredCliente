package com.binarium.chipred.config;

import android.app.KeyguardManager;
import android.app.admin.DevicePolicyManager;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;
import android.view.MenuItem;

import com.binarium.chipred.BuildConfig;
import com.binarium.chipred.GenericDialog;
import com.binarium.chipred.MainActivity;
import com.binarium.chipred.R;
import com.binarium.chipred.SharedPreferencesManager;
import com.binarium.chipred.auth.Authenticator;
import com.jaredrummler.android.device.DeviceName;

import static com.binarium.chipred.ChipREDConstants.REQUEST_CODE_CONFIRM_DEVICE_CREDENTIALS;
import static com.binarium.chipred.MainActivity.DEVICEUSER;

public class SettingsActivity extends AppCompatPreferenceActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getFragmentManager().beginTransaction().replace(android.R.id.content, new
                MainPreferenceFragment()).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int itemId = item.getItemId();
        switch (itemId)
        {
            case android.R.id.home:
                finish();

            default:
                break;
        }

        return true;
    }

    public static class MainPreferenceFragment extends PreferenceFragment
    {
        private SharedPreferencesManager sharedPreferencesManager;
        private boolean switchChanged = false;
        private SwitchPreference setSecurityMethod;

        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_main);

            sharedPreferencesManager = new SharedPreferencesManager(getActivity());

            Preference aboutApp = findPreference("about_app");
            aboutApp.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
            {
                @Override
                public boolean onPreferenceClick(Preference preference)
                {
                    showAppInfo();
                    return false;
                }
            });

            Preference deviceInfo = findPreference("my_device");
            deviceInfo.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
            {
                @Override
                public boolean onPreferenceClick(Preference preference)
                {
                    showDeviceInfo();
                    return false;
                }
            });

            setSecurityMethod = (SwitchPreference) findPreference("security_method");
            setSecurityMethod.setOnPreferenceChangeListener(new Preference
                    .OnPreferenceChangeListener()
            {
                @Override
                public boolean onPreferenceChange(Preference preference, Object o)
                {
                    switchChanged = true;
                    boolean wasChecked = ((SwitchPreference) preference).isChecked();
                    if (wasChecked)
                    {
                        Authenticator authenticator = new Authenticator(MainPreferenceFragment
                                .this, new Authenticator.AuthenticatorInteface()
                        {
                            @Override
                            public void onAuthenticated()
                            {
                                onAuthOk();
                            }

                            @Override
                            public void onCancelledAuthentication()
                            {
                                onAuthFailed();
                            }
                        });
                        return !authenticator.excecuteAuth();
                    }
                    else
                    {
                        return deviceHasPassword();
                    }
                }
            });
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data)
        {
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == REQUEST_CODE_CONFIRM_DEVICE_CREDENTIALS)
            {
                if (resultCode == RESULT_OK)
                {
                    onAuthOk();
                }
                else
                {
                    onAuthFailed();
                }
            }
        }

        private void onAuthOk()
        {
            setSecurityMethod.setChecked(false);
            sharedPreferencesManager.putBooleanInSP("security_method", false);
        }

        private void onAuthFailed()
        {
            setSecurityMethod.setChecked(true);
            sharedPreferencesManager.putBooleanInSP("security_method", true);
        }

        private void showAppInfo()
        {
            String appInfo;
            appInfo = "Aplicación de cliente ChipRED®\n";
            appInfo += "Versión ";
            appInfo += BuildConfig.VERSION_NAME;
            appInfo += "\n";
            appInfo += "www.chipred.com\n";
            appInfo += "2018\n";

            GenericDialog appInfoDialog = new GenericDialog("Acerca de la aplicación", appInfo,
                    new Runnable()
            {
                @Override
                public void run()
                {
                    //nada...
                }
            }, null, getActivity());
            appInfoDialog.show();
        }

        private void showDeviceInfo()
        {
            StringBuilder deviceInfo = new StringBuilder();

            try
            {
                DeviceName.DeviceInfo info = DeviceName.getDeviceInfo(getActivity());
                deviceInfo.append(info.manufacturer);
                deviceInfo.append(" ");
                deviceInfo.append(info.marketName);

                if (sharedPreferencesManager.isContained(DEVICEUSER))
                {
                    deviceInfo.append(" de ");
                    deviceInfo.append(sharedPreferencesManager.getStringFromSP(DEVICEUSER, ""));
                }
            }
            catch (Exception e)
            {
                deviceInfo.append("Desconocido");
            }

            GenericDialog deviceInfoDialog = new GenericDialog("Dispositivo", deviceInfo.toString
                    (), new Runnable()
            {
                @Override
                public void run()
                {
                    //nada...
                }
            }, null, getActivity());
            deviceInfoDialog.show();
        }

        private boolean deviceHasPassword()
        {
            KeyguardManager keyguardManager = (KeyguardManager) getActivity().getSystemService
                    (KEYGUARD_SERVICE);

            try
            {
                if (!keyguardManager.isKeyguardSecure())
                {
                    //No hay una contraseña asignada
                    GenericDialog askForNewPassword = new GenericDialog("Asignar contraseña",
                            getActivity().getResources().getString(R.string.set_password_text),
                            new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    Intent intent = new Intent(DevicePolicyManager.ACTION_SET_NEW_PASSWORD);
                                    startActivity(intent);
                                }
                            }, new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            //Nada....
                        }
                    }, getActivity());

                    askForNewPassword.setPositiveText("Asignar");
                    askForNewPassword.setNegativeText("Ahora no");
                    askForNewPassword.show();

                    return false;
                }
            }
            catch (NullPointerException e)
            {
                e.printStackTrace();
                return false;
            }

            return true;
        }

        @Override
        public void onDestroyView()
        {
            super.onDestroyView();

            if (switchChanged)
            {
                getActivity().setResult(MainActivity.GO_TO_PREFERENCES);
            }
        }
    }
}
