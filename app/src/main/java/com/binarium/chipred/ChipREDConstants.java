package com.binarium.chipred;

import android.app.Activity;
import android.content.Context;
import androidx.fragment.app.Fragment;

import android.util.DisplayMetrics;
import android.util.Log;

import com.jaredrummler.android.device.DeviceName;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import static com.binarium.chipred.MainActivity.DEVICEUSER;

public class ChipREDConstants
{
    /**
     * Formatos
     **/
    public final static DecimalFormat CR_AMOUNT_FORMAT = new DecimalFormat("###,###,###,###,##0");
    public final static DecimalFormat MX_AMOUNT_FORMAT = new DecimalFormat("###,###,###,###," +
            "##0.00");
    public final static DecimalFormat LITERS_FORMAT = new DecimalFormat("###,###,###,###,##0.000");

    public final static String CRC_CURRENCY_SYMBOL = "₡";
    public final static String MXN_CURRENCY_SYMBOL = "$";

    public final static String CRC_CURRENCY_ISO = "CRC";
    final static String MXN_CURRENCY_ISO = "MXN";

    public final static SimpleDateFormat jsonDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    /**
     * PAISES
     **/
    public final static String[] COUNTRIES = {"mexico", "costa rica"};

    /**
     * Keys y constantes
     */
    public final static String GENERAL_NOTIFICATIONS_ID = "general";
    public final static String SECURITY_METHOD_FOR_BILLETS = "security_method";
    public final static int REQUEST_CODE_CONFIRM_DEVICE_CREDENTIALS = 300;
    public final static String NULL_ACTIVITIES_TAG = "nullActivities";
    public final static String LAST_UPDATE_DATE = "lastUpdate";
    public final static String BILLETS_PATH = "/billetes/";
    public final static String BILLETS_QR_CODES_PATH = "/billetes/codigos/";

    /**
     * Estados de billetes
     */
    public final static String BILLETE_ACTIVO = "AC";
    public final static String BILLETE_USADO = "US";
    public final static String BILLETE_CANCELADO = "CA";

    /**
     * Estados de hacienda CR
     */
    public final static String REPORTE_HACIENDA_PENDIENTE = "PE";
    public final static String REPORTE_HACIENDA_ENVIADO = "EN";
    public final static String REPORTE_HACIENDA_RECIBIDO = "RE";
    public final static String REPORTE_HACIENDA_PROCESANDO = "PR";
    public final static String REPORTE_HACIENDA_RECHAZADO = "RZ";
    public final static String REPORTE_HACIENDA_ACEPTADO = "AC";
    public final static String REPORTE_HACIENDA_PENDIENTE_FACTURAR = "PF";
    public final static String REPORTE_HACIENDA_CANCELADO = "CA";

    /**
     * Colores de estados de hacienda
     */

    public static int getColorStatusHacienda(String status)
    {
        switch (status)
        {
            case REPORTE_HACIENDA_RECHAZADO:
            case REPORTE_HACIENDA_CANCELADO:
                return R.color.cancelado;

            case REPORTE_HACIENDA_ACEPTADO:
                return R.color.aceptado;

            case REPORTE_HACIENDA_PENDIENTE:
            case REPORTE_HACIENDA_ENVIADO:
            case REPORTE_HACIENDA_RECIBIDO:
            case REPORTE_HACIENDA_PROCESANDO:
            case REPORTE_HACIENDA_PENDIENTE_FACTURAR:
            default:
                return R.color.recibido;
        }
    }

    /**
     * Métodos
     */
    public static int dpToPixels(float dp, Activity parentActivity)
    {
        DisplayMetrics metrics = parentActivity.getResources().getDisplayMetrics();
        float pixels = metrics.density * dp;
        return (int) (pixels + 0.5f);
    }

    public static String getDeviceId(Context context)
    {
        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(context);
        StringBuilder appInfo = new StringBuilder();

        try
        {
            DeviceName.DeviceInfo info = DeviceName.getDeviceInfo(context);
            appInfo.append(info.manufacturer);
            appInfo.append(" ");
            appInfo.append(info.marketName);

            if (sharedPreferencesManager.isContained(DEVICEUSER))
            {
                appInfo.append(" de ");
                appInfo.append(sharedPreferencesManager.getStringFromSP(DEVICEUSER, ""));
            }
        }
        catch (Exception e)
        {
            appInfo.append("Desconocido");
        }

        return appInfo.toString();
    }

    /**
     * Método: checkNullParentActivity
     * <p>
     * Checa el estado de la actividad y retorna true si es nula, false si no lo es
     * Utilizada para callBacks que se quedan activos cuando la actividad se destruye
     *
     * @return boolean:
     */

    public static boolean checkNullParentActivity(Activity activity, Fragment fragment)
    {
        //Asegurarse que este viva la actividad padre
        if (activity == null || !fragment.isAdded())
        {
            Log.e(ChipREDConstants.NULL_ACTIVITIES_TAG, "Error de actividad nula");
            return true;
        }

        return false;
    }

    public static String twoDigits(int n)
    {
        return (n <= 9) ? ("0" + n) : String.valueOf(n);
    }

    public static String monthNumberToString(int monthNumber)
    {
        switch (monthNumber)
        {
            case 1:
                return "ENE";

            case 2:
                return "FEB";

            case 3:
                return "MAR";

            case 4:
                return "ABR";

            case 5:
                return "MAY";

            case 6:
                return "JUN";

            case 7:
                return "JUL";

            case 8:
                return "AGO";

            case 9:
                return "SEP";

            case 10:
                return "OCT";

            case 11:
                return "NOV";

            case 12:
                return "DIC";

            default:
                return (monthNumber <= 9) ? ("0" + monthNumber) : String.valueOf(monthNumber);
        }
    }

    public static String monthNumberToAbreviation(String date)
    {
        String monthAbrev = "";

        //Obtener numero de mes
        try
        {
            String monthNumber = date.substring(3, 5);

            switch (monthNumber)
            {
                case "01":
                    monthAbrev = "ENE";
                    break;

                case "02":
                    monthAbrev = "FEB";
                    break;

                case "03":
                    monthAbrev = "MAR";
                    break;

                case "04":
                    monthAbrev = "ABR";
                    break;

                case "05":
                    monthAbrev = "MAY";
                    break;

                case "06":
                    monthAbrev = "JUN";
                    break;

                case "07":
                    monthAbrev = "JUL";
                    break;

                case "08":
                    monthAbrev = "AGO";
                    break;

                case "09":
                    monthAbrev = "SEP";
                    break;

                case "10":
                    monthAbrev = "OCT";
                    break;

                case "11":
                    monthAbrev = "NOV";
                    break;

                case "12":
                    monthAbrev = "DIC";
                    break;
            }

            return date.substring(0, 3) + monthAbrev + date.substring(5);
        }
        catch (Exception e)
        {
            return date;
        }
    }


}
