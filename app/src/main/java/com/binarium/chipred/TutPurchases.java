package com.binarium.chipred;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class TutPurchases extends Fragment
{
    private static final String ARG_LAYOUT_RES_ID = "layoutResId";
    private int layoutResId;
    private static final int DELAY = 2200;

    private int purchasesImages[] = {
            R.drawable.tutorial_consumos,
            R.drawable.tutorial_consumos2,
            R.drawable.tutorial_consumos3
    };

    private Handler imgShow;
    private ImageView imageView;
    private int idx = 1;

    private final Runnable changeImg = new Runnable()
    {
        @Override
        public void run()
        {
            Picasso.with(getActivity()).load(purchasesImages[idx]).into(imageView);
            idx++;
            if (idx == 3) idx = 0;

            imgShow.postDelayed(changeImg, DELAY);
        }
    };

    public static TutPurchases newInstance(int layoutResId)
    {
        TutPurchases tutPurchases = new TutPurchases();

        Bundle args = new Bundle();
        args.putInt(ARG_LAYOUT_RES_ID, layoutResId);
        tutPurchases.setArguments(args);

        return tutPurchases;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(ARG_LAYOUT_RES_ID))
        {
            layoutResId = getArguments().getInt(ARG_LAYOUT_RES_ID);
        }

        imgShow = new Handler();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState)
    {
        View fragmentView = inflater.inflate(layoutResId, container, false);

        imageView = fragmentView.findViewById(R.id.purchaseTutImgView);
        imgShow.postDelayed(changeImg, DELAY/4);

        return fragmentView;
    }

    @Override
    public void onPause()
    {
        super.onPause();

        if (imgShow != null)
        {
            imgShow.removeCallbacks(changeImg);
        }
    }
}