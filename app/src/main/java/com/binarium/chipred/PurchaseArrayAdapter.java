package com.binarium.chipred;

import android.content.Context;
import android.graphics.Color;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class PurchaseArrayAdapter extends RecyclerView.Adapter<PurchaseArrayAdapter
        .PurchaseViewHolder> implements View.OnClickListener
{
    private final ArrayList<Purchase> purchases;
    private Context context;
    private View.OnClickListener listener;

    public PurchaseArrayAdapter(ArrayList<Purchase> purchases, Context context)
    {
        this.purchases = purchases;
        this.context = context;
    }

    @Override
    public int getItemCount()
    {
        return purchases.size();
    }

    @Override
    public PurchaseViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.purchaselayout,
                viewGroup, false);
        PurchaseViewHolder pvh = new PurchaseViewHolder(v);

        v.setOnClickListener(this);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PurchaseViewHolder purchaseViewHolder, int i)
    {
        Purchase purchase = purchases.get(i);

        //Instanciar las sharedpreferences
        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(context);
        String country = sharedPreferencesManager.getStringFromSP("pais", "mexico");
        String amountString, volumeString;

        if (country.equals("mexico"))
        {
            amountString = ChipREDConstants.MX_AMOUNT_FORMAT.format(purchase.getAmount()) + " " +
                    ChipREDConstants.MXN_CURRENCY_ISO;
            purchaseViewHolder.currencyIcon.setBackgroundResource(R.drawable.ic_money);
        }
        else
        {
            amountString = ChipREDConstants.CR_AMOUNT_FORMAT.format(purchase.getAmount()) + " " +
                    ChipREDConstants.CRC_CURRENCY_ISO;
            purchaseViewHolder.currencyIcon.setBackgroundResource(R.drawable.ic_colon_crc_blanco);
        }

        volumeString = ChipREDConstants.LITERS_FORMAT.format(purchase.getVolume()) + " LTS";
        purchaseViewHolder.volume.setText(volumeString);
        purchaseViewHolder.amount.setText(amountString);
        purchaseViewHolder.date.setText(purchase.getDate());
        purchaseViewHolder.time.setText(purchase.getTime());
        ((CardView) purchaseViewHolder.itemView).setCardBackgroundColor(Color.parseColor(purchase
                .getProductColor()));
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setOnClickListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onClick(View view)
    {
        if (listener != null)
        {
            listener.onClick(view);
        }
    }

    public static class PurchaseViewHolder extends RecyclerView.ViewHolder
    {
        private ImageView currencyIcon;
        private TextView amount;
        private TextView volume;
        private TextView date;
        private TextView time;

        public PurchaseViewHolder(View itemView)
        {
            super(itemView);

            amount = itemView.findViewById(R.id.purchaseAmount);
            date = itemView.findViewById(R.id.purchaseDate);
            time = itemView.findViewById(R.id.purchaseTime);
            volume = itemView.findViewById(R.id.purchaseVolume);
            currencyIcon = itemView.findViewById(R.id.purchase_currency_icon);
        }
    }
}

