package com.binarium.chipred;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class DetailArrayAdapter extends RecyclerView.Adapter<DetailArrayAdapter.DetailViewHolder>
{
    private ArrayList<DetailItem> details;
    private Context context;

    public DetailArrayAdapter(ArrayList<DetailItem> details, Context
            context)
    {
        this.details = details;
        this.context = context;
    }

    @NonNull
    @Override
    public DetailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.detail_element,
                parent,
                false);

        return  new DetailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DetailViewHolder holder,
                                 int position)
    {
        DetailItem detailItem = details.get(position);
        holder.itemKey.setText(detailItem.getKey());
        holder.itemValue.setText(detailItem.getValue());
        holder.itemImage.setImageResource(detailItem.getImageResource());

    }

    @Override
    public int getItemCount()
    {
        return details.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class DetailViewHolder extends RecyclerView.ViewHolder
    {
        private TextView itemKey;
        private TextView itemValue;
        private ImageView itemImage;

        public DetailViewHolder(View itemView)
        {
            super(itemView);

            itemKey = itemView.findViewById(R.id.purchase_detail_item_key);
            itemValue = itemView.findViewById(R.id.purchase_detail_item_value);
            itemImage = itemView.findViewById(R.id.purchase_detail_item_icon);
        }
    }
}
