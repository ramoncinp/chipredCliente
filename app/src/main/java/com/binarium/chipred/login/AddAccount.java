package com.binarium.chipred.login;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.binarium.chipred.ChipREDConstants;
import com.binarium.chipred.ChipRedManager;
import com.binarium.chipred.Country;
import com.binarium.chipred.CountryArrayAdapter;
import com.binarium.chipred.GenericDialog;
import com.binarium.chipred.R;
import com.binarium.chipred.SharedPreferencesManager;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AddAccount extends AppCompatActivity
{
    private final static int CREATE_CLIENT = 1;
    private final static int EDIT_CLIENT = 2;

    private Country selectedCountry;
    private ChipRedManager chipRedManager;
    private SharedPreferencesManager sharedPreferencesManager;
    private ProgressDialog progressDialog;
    private VideoView mVideoView;
    private ChipRedManager.OnMessageReceived chipRedListener;

    private JSONObject oldInfoFromWebService;

    private int activityAction;

    private CheckBox capturePlatesCb;
    private MaterialEditText nameEd;
    private MaterialEditText taxCodeEd;
    private MaterialEditText phoneEd;
    private MaterialEditText emailEd;
    private MaterialBetterSpinner countrySelector;

    private Button button;
    private Button cancelButton;

    private View.OnClickListener buttonListener;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_modify_account);

        hideToolBar();

        sharedPreferencesManager = new SharedPreferencesManager(this);
        activityAction = getIntent().getIntExtra("activityAction", 1);

        button = findViewById(R.id.submit_account);
        cancelButton = findViewById(R.id.cancel_modify_account);

        nameEd = findViewById(R.id.new_account_name);
        taxCodeEd = findViewById(R.id.tax_code);
        phoneEd = findViewById(R.id.new_account_phone_number);
        emailEd = findViewById(R.id.new_account_email);
        countrySelector = findViewById(R.id.country_selector);
        capturePlatesCb = findViewById(R.id.capture_plates_cb);

        nameEd.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        taxCodeEd.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        if (activityAction == CREATE_CLIENT)
        {
            capturePlatesCb.setVisibility(View.GONE);
        }

        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived()
        {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN)
            {
                if (progressDialog != null) progressDialog.dismiss();

                if (crMessage.contains("Registrado"))
                {
                    showWelcomeDialog();
                }
                else if (crMessage.equals("Modificado"))
                {
                    Toast.makeText(AddAccount.this, "Información de cliente modificada", Toast
                            .LENGTH_SHORT).show();

                    finish();
                }
                else
                {
                    GenericDialog genericDialog = new GenericDialog("Aviso", crMessage, new
                            Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    if (activityAction == EDIT_CLIENT)
                                    {
                                        finish();
                                    }
                                }
                            }, null, AddAccount.this);

                    genericDialog.setCancelable(false);
                    genericDialog.show();
                }
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN)
            {
                if (progressDialog != null) progressDialog.dismiss();

                GenericDialog genericDialog = new GenericDialog("Error", errorMessage, new
                        Runnable()
                        {
                            @Override
                            public void run()
                            {
                                if (activityAction == EDIT_CLIENT) finish();
                            }
                        }, null, AddAccount.this);

                genericDialog.show();
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN)
            {
                Log.d("CrResponse", response.toString());
                setOldInfo(response);
                progressDialog.dismiss();
            }
        }, this);

        if (activityAction == EDIT_CLIENT)
        {
            setEditActivityViews();
            setEditActivityListeners();
            button.setText("Modificar");

            String clientId = sharedPreferencesManager.getStringFromSP("id_cliente", "");

            progressDialog = ProgressDialog.show(AddAccount.this, "", "Obteniendo datos de " +
                    "cliente...", true, false);
            chipRedManager.searchClient(ChipRedManager.BYID, clientId);
        }
        else
        {
            setCountrySpinner();
            buttonListener = new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    hideSoftKeyboard(AddAccount.this, v);

                    final JSONObject request = validateAndGetUserData();
                    if (request != null)
                    {
                        try
                        {
                            confirmEmailAddress(request.getString("email"), new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    progressDialog = ProgressDialog.show(AddAccount.this, "",
                                            "Procesando" + " información", true, false);
                                    chipRedManager.addOrModifyClient(request, true, null);
                                }
                            });
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            };

            cancelButton.setVisibility(View.GONE);
        }

        button.setOnClickListener(buttonListener);
    }

    private void setEditActivityViews()
    {
        TextView layer = findViewById(R.id.inBetweenLayer);
        layer.setBackgroundColor(getResources().getColor(R.color.white));

        TextView addAcountTitle = findViewById(R.id.add_account_title);
        addAcountTitle.setText("Modificar información");

        MaterialEditText taxCodeEd = (MaterialEditText) findViewById(R.id.tax_code);
        taxCodeEd.setEnabled(false);

        MaterialEditText emailEd = (MaterialEditText) findViewById(R.id.new_account_email);
        emailEd.setEnabled(false);

        //Deshabilitar spinner para seleccionar el país
        countrySelector.setEnabled(false);
        countrySelector.setClickable(false);
        CountryArrayAdapter countryArrayAdapter = new CountryArrayAdapter(this, new
                ArrayList<Country>());
        countrySelector.setAdapter(countryArrayAdapter);
        countrySelector.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                //Nada
            }
        });

        cancelButton.setVisibility(View.VISIBLE);
    }

    private void setEditActivityListeners()
    {
        chipRedListener = new ChipRedManager.OnMessageReceived()
        {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN)
            {
                if (progressDialog != null) progressDialog.dismiss();

                if (crMessage.contains("Registrado"))
                {
                    showWelcomeDialog();
                }
                else
                {
                    GenericDialog genericDialog = new GenericDialog("Aviso", crMessage, new
                            Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    //nada...
                                }
                            }, null, AddAccount.this);

                    genericDialog.show();
                }
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN)
            {
                if (progressDialog != null) progressDialog.dismiss();

                GenericDialog genericDialog = new GenericDialog("Error", errorMessage, new
                        Runnable()
                        {
                            @Override
                            public void run()
                            {
                                //nada...
                            }
                        }, null, AddAccount.this);

                genericDialog.show();
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN)
            {
                Log.d("CrResponse", response.toString());
                setOldInfo(response);
            }
        };

        buttonListener = new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                hideSoftKeyboard(AddAccount.this, v);

                JSONObject data = validateAndGetUserData();
                if (data != null)
                {
                    progressDialog = ProgressDialog.show(AddAccount.this, "", "Procesando " +
                            "información", true, false);

                    chipRedManager.addOrModifyClient(data, false, sharedPreferencesManager
                            .getStringFromSP("id_cliente", ""));
                }

                if (progressDialog != null) progressDialog.dismiss();
            }
        };

        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if (activityAction == CREATE_CLIENT)
        {
            mVideoView = (VideoView) findViewById(R.id.log_in_video);
            Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.driveway);

            mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener()
            {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra)
                {
                    TextView layer = findViewById(R.id.inBetweenLayer);
                    layer.setBackgroundColor(getResources().getColor(R.color.white));
                    return true;
                }
            });
            mVideoView.setVideoURI(uri);
            mVideoView.start();

            mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
            {
                @Override
                public void onPrepared(MediaPlayer mp)
                {
                    mp.setLooping(true);
                }
            });
        }
    }

    private void hideToolBar()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (getSupportActionBar() == null) setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) getSupportActionBar().hide();
    }

    public void showWelcomeDialog()
    {
        LayoutInflater inflater = getLayoutInflater();
        View alertDialog = inflater.inflate(R.layout.welcome_layout, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(alertDialog);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
                finish();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    private void confirmEmailAddress(String email, final Runnable action)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Correo Electrónico");

        View view = getLayoutInflater().inflate(R.layout.dialog_confirm_email_address, null);
        TextView emailToConfirm = view.findViewById(R.id.email_to_validate);

        emailToConfirm.setText(email);
        builder.setPositiveButton("Sí", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                action.run();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });

        builder.setView(view);

        Dialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    public JSONObject validateAndGetUserData()
    {
        boolean isValid;

        isValid = nameEd.validateWith(new METValidator("El nombre debe ser de más de 3 " +
                "caracteres")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                int size = text.length();
                if (size <= 3 || isEmpty)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        });

        isValid &= taxCodeEd.validateWith(new METValidator("Campo obligatorio")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                if (isEmpty)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        });

        isValid &= emailEd.validateWith(new METValidator("Dirección de correo inválida")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                if (isEmpty || !text.toString().contains("@"))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        });

        isValid &= validateSelectedAddressForm();

        if (isValid)
        {
            JSONObject request = new JSONObject();
            try
            {
                request.put("nombre", nameEd.getText().toString());
                request.put("clave_fiscal", taxCodeEd.getText().toString().toUpperCase());
                request.put("email", emailEd.getText().toString());

                //Agregar plantilla de lista de difusión
                request.put("contactos", new JSONArray());

                //Agregar preferencia para captura de placas con billete electrónico
                request.put("captura_placas", capturePlatesCb.isChecked());
            }
            catch (JSONException e)
            {
                Toast.makeText(this, "Error al procesar los datos", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }

            request = setPhoneAndAddress(selectedCountry, request);
            return request;
        }
        else
        {
            return null;
        }
    }

    private boolean validateSelectedAddressForm()
    {
        if (selectedCountry == null)
        {
            countrySelector.setError("Seleccione su país");
            return false;
        }

        return true;
    }

    private JSONObject setPhoneAndAddress(Country selectedCountry, JSONObject request)
    {
        if (selectedCountry.getIdentifier().contains("mexico"))
        {
            MaterialEditText calle = findViewById(R.id.mexico_addr_calle);
            MaterialEditText numeroExterior = findViewById(R.id.mexico_addr_numero_exterior);
            MaterialEditText numeroInterior = findViewById(R.id.mexico_addr_numero_interior);
            MaterialEditText colonia = findViewById(R.id.mexico_addr_colonia);
            MaterialEditText municipio = findViewById(R.id.mexico_addr_municipio);
            MaterialEditText estado = findViewById(R.id.mexico_addr_estado);
            MaterialEditText codigoPostal = findViewById(R.id.mexico_addr_codigo_postal);

            try
            {
                JSONObject address = new JSONObject();
                address.put("pais", "mexico");
                address.put("calle", calle.getText().toString());
                address.put("numero_ext", numeroExterior.getText().toString());
                address.put("numero_int", numeroInterior.getText().toString());
                address.put("colonia", colonia.getText().toString());
                address.put("municipio", municipio.getText().toString());
                address.put("estado", estado.getText().toString());
                address.put("codigo_postal", codigoPostal.getText().toString());

                JSONObject phone = new JSONObject();
                phone.put("numero", phoneEd.getText().toString());
                phone.put("codigo", "52");

                request.put("direccion", address);
                request.put("telefono", phone);

                //Regresar objeto JSON con la dirección ingresada
                return request;
            }
            catch (JSONException e)
            {
                //Devolver el objeto como ingresó al método
                e.printStackTrace();
                return request;
            }
        }
        else
        {
            MaterialEditText provincia = findViewById(R.id.costa_rica_addr_provincia);
            MaterialEditText canton = findViewById(R.id.costa_rica_addr_canton);
            MaterialEditText distrito = findViewById(R.id.costa_rica_addr_distrito);
            MaterialEditText barrio = findViewById(R.id.costa_rica_addr_barrio);
            MaterialEditText otrasSenias = findViewById(R.id.costa_rica_addr_otras_senias);

            try
            {
                JSONObject address = new JSONObject();
                address.put("pais", "costa rica");
                address.put("provincia", provincia.getText().toString());
                address.put("canton", canton.getText().toString());
                address.put("distrito", distrito.getText().toString());
                address.put("barrio", barrio.getText().toString());
                address.put("otras_senas", otrasSenias.getText().toString());

                JSONObject phone = new JSONObject();
                phone.put("numero", phoneEd.getText().toString());
                phone.put("codigo", "506");

                request.put("direccion", address);
                request.put("telefono", phone);

                //Regresar objeto JSON con la dirección ingresada
                return request;
            }
            catch (JSONException e)
            {
                //Devolver el objeto como ingresó al método
                e.printStackTrace();
                return request;
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity, View view)
    {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context
                .INPUT_METHOD_SERVICE);
        if (imm != null) imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

    public void setOldInfo(JSONObject response)
    {
        oldInfoFromWebService = new JSONObject();
        try
        {
            JSONObject data = response.getJSONArray("data").getJSONObject(0);
            oldInfoFromWebService.put("nombre", data.getString("nombre"));
            oldInfoFromWebService.put("email", data.getString("email"));
            oldInfoFromWebService.put("clave_fiscal", data.getString("clave_fiscal"));
            oldInfoFromWebService.put("telefono", data.getJSONObject("telefono"));
            oldInfoFromWebService.put("direccion", data.getJSONObject("direccion"));
            oldInfoFromWebService.put("contactos", data.getJSONArray("contactos"));
            if (data.has("captura_placas"))
                oldInfoFromWebService.put("captura_placas", data.getBoolean("captura_placas"));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        try
        {
            nameEd.setText(oldInfoFromWebService.getString("nombre"));
            emailEd.setText(oldInfoFromWebService.getString("email"));
            taxCodeEd.setText(oldInfoFromWebService.getString("clave_fiscal"));

            if (oldInfoFromWebService.has("captura_placas"))
                capturePlatesCb.setChecked(oldInfoFromWebService.getBoolean("captura_placas"));

            //Rellenar los demás campos dependiendo del país
            phoneEd.setText(oldInfoFromWebService.getJSONObject("telefono").getString("numero"));
            setOldAddressInfo(oldInfoFromWebService);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public void setOldAddressInfo(JSONObject response)
    {
        String countryIdentifier;

        //Obtener el país
        try
        {
            countryIdentifier = response.getJSONObject("direccion").getString("pais");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            Toast.makeText(this, "Error al procesar la información", Toast.LENGTH_SHORT).show();
            return;
        }

        //Crear el objeto country para definir el layout
        Country country;

        if (countryIdentifier.equals("mexico"))
        {
            country = new Country("México", R.drawable.ic_mexico, countryIdentifier);
        }
        else
        {
            country = new Country("Costa Rica", R.drawable.ic_costa_rica, countryIdentifier);
        }

        countrySelector.setText(country.getName());
        selectedCountry = country;
        setForm(country);

        if (countryIdentifier.contains("mexico"))
        {
            MaterialEditText calle = findViewById(R.id.mexico_addr_calle);
            MaterialEditText numeroExterior = findViewById(R.id.mexico_addr_numero_exterior);
            MaterialEditText numeroInterior = findViewById(R.id.mexico_addr_numero_interior);
            MaterialEditText colonia = findViewById(R.id.mexico_addr_colonia);
            MaterialEditText municipio = findViewById(R.id.mexico_addr_municipio);
            MaterialEditText estado = findViewById(R.id.mexico_addr_estado);
            MaterialEditText codigoPostal = findViewById(R.id.mexico_addr_codigo_postal);

            try
            {
                JSONObject address = response.getJSONObject("direccion");
                calle.setText(address.getString("calle"));
                numeroExterior.setText(address.getString("numero_ext"));
                numeroInterior.setText(address.getString("numero_int"));
                colonia.setText(address.getString("colonia"));
                municipio.setText(address.getString("municipio"));
                estado.setText(address.getString("estado"));
                codigoPostal.setText(address.getString("codigo_postal"));
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            MaterialEditText provincia = findViewById(R.id.costa_rica_addr_provincia);
            MaterialEditText canton = findViewById(R.id.costa_rica_addr_canton);
            MaterialEditText distrito = findViewById(R.id.costa_rica_addr_distrito);
            MaterialEditText barrio = findViewById(R.id.costa_rica_addr_barrio);
            MaterialEditText otrasSenias = findViewById(R.id.costa_rica_addr_otras_senias);

            try
            {
                JSONObject address = response.getJSONObject("direccion");
                provincia.setText(address.getString("provincia"));
                canton.setText(address.getString("canton"));
                distrito.setText(address.getString("distrito"));
                barrio.setText(address.getString("barrio"));
                otrasSenias.setText(address.getString("otras_senas"));
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
    }

    public boolean isInfoChanged(JSONObject data)
    {
        try
        {
            if (!data.getString("nombre").equals(oldInfoFromWebService.getString("nombre")))
            {
                return true;
            }

            if (!data.getString("email").equals(oldInfoFromWebService.getString("email")))
            {
                return true;
            }

            if (!data.getString("clave_fiscal").equals(oldInfoFromWebService.getString
                    ("clave_fiscal")))
            {
                return true;
            }

            if (!data.getString("telefono").equals(oldInfoFromWebService.getString("telefono")))
            {
                return true;
            }

            if (!data.getString("direccion").equals(oldInfoFromWebService.getString("direccion")))
            {
                return true;
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return false;
    }

    private void setCountrySpinner()
    {
        //Enlistar países
        ArrayList<Country> countries = new ArrayList<>();
        countries.add(new Country("México", R.drawable.ic_mexico, ChipREDConstants.COUNTRIES[0]));
        countries.add(new Country("Costa Rica", R.drawable.ic_costa_rica, ChipREDConstants
                .COUNTRIES[1]));

        //Definir su adaptador
        CountryArrayAdapter countryArrayAdapter = new CountryArrayAdapter(this, countries);
        countrySelector.setAdapter(countryArrayAdapter);
        countrySelector.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Country country = (Country) parent.getItemAtPosition(position);
                countrySelector.setText(country.getName());

                //Asignar a variable global
                selectedCountry = country;

                //Mostrar layout correspondiente
                setForm(country);

                countrySelector.clearFocus();
            }
        });
    }

    private void setForm(Country country)
    {
        //Definir imagen del pais
        ImageView countryImage = findViewById(R.id.country_image);
        countryImage.setImageResource(country.getImageResource());

        //Obtener el layout del formulario
        LinearLayout linearLayout = findViewById(R.id.client_info_form_layout);

        //Analizar si ya existe un view
        if (linearLayout.getChildCount() != 0)
        {
            linearLayout.removeAllViews();
        }

        //Obtener el view de la forma correspondiente
        View addressForm;
        if (country.getIdentifier().contains("mexico"))
        {
            addressForm = getLayoutInflater().inflate(R.layout.client_form_mexico_address, null);
        }
        else
        {
            addressForm = getLayoutInflater().inflate(R.layout.client_form_costa_rica_address,
                    null);
        }
        linearLayout.addView(addressForm);
    }
}
