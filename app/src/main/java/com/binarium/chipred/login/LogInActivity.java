package com.binarium.chipred.login;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;

import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.binarium.chipred.ChipRedManager;
import com.binarium.chipred.GenericDialog;
import com.binarium.chipred.R;
import com.binarium.chipred.SharedPreferencesManager;

import org.json.JSONException;
import org.json.JSONObject;

import static com.binarium.chipred.MainActivity.DEVICEUSER;

public class LogInActivity extends Activity
{
    private EditText emailText;
    private EditText passwordText;
    private Button login;
    private SharedPreferencesManager sharedPreferences;
    private Context context;
    private VideoView mVideoView;

    private static final int REQUEST_SIGNUP = 0;

    ProgressDialog progressDialog;
    ChipRedManager chipRedManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.login_activity);
        context = getApplicationContext();

        emailText = findViewById(R.id.email);
        passwordText = findViewById(R.id.password);
        login = findViewById(R.id.login);
        sharedPreferences = new SharedPreferencesManager(context);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        TextView newAccountText = findViewById(R.id.newAccount);
        newAccountText.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(LogInActivity.this, AddAccount.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        TextView getPassTextView = findViewById(R.id.getpass);
        getPassTextView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                requestNewPass();
            }
        });

        TextView sendEmailConfirmText = findViewById(R.id.send_email_confirm);
        sendEmailConfirmText.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                requestConfirmationEmail();
            }
        });

        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived()
        {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN)
            {
                if (webServiceN == ChipRedManager.LOGIN_WB || webServiceN == ChipRedManager
                        .NEWPASS_WB || webServiceN == ChipRedManager
                        .RESEND_CONFIRMATION_EMAIL)
                {
                    showResult(false, "ChipRED", crMessage);
                }
                else if (webServiceN == ChipRedManager.ADD_DEVICE_WB)
                {
                    showResult(true, null, null);
                }
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN)
            {
                showResult(false, "Error", errorMessage);
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN)
            {
                if (webServiceN == ChipRedManager.LOGIN_WB)
                {
                    parseLogInResponse(response);
                }
                else if (webServiceN == ChipRedManager.SEARCH_CLIENT_WB)
                {
                    parseResendConfirmEmail(response);
                }
            }
        }, getApplicationContext());
    }

    @Override
    public void onResume()
    {
        super.onResume();

        mVideoView = findViewById(R.id.log_in_video);
        Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.driveway);

        mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener()
        {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra)
            {
                LinearLayout linearLayout = findViewById(R.id.log_in_linear_layout);
                linearLayout.setBackgroundColor(getResources().getColor(R.color.white));

                return true;
            }
        });

        mVideoView.setVideoURI(uri);
        mVideoView.start();

        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
        {
            @Override
            public void onPrepared(MediaPlayer mp)
            {
                mp.setLooping(true);
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        // disable going back to the MainActivity
        moveTaskToBack(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == REQUEST_SIGNUP)
        {
            if (resultCode == RESULT_OK)
            {
                this.finish();
            }
        }
    }

    public void login(View v)
    {
        if (!validate())
        {
            onLoginFailed("Error", "Datos no válidos");
            return;
        }

        login.setEnabled(false);

        progressDialog = new ProgressDialog(LogInActivity.this);

        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Identificando...");
        progressDialog.show();

        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();

        chipRedLogin(email, password);
        hideSoftKeyboard(this, v);
    }

    private void chipRedLogin(String email, String password)
    {
        chipRedManager.userLogIn(email, password);
    }

    private void parseLogInResponse(JSONObject response)
    {
        String username = "";
        String id_cliente = "";
        String tipo_usuario = "";
        String pais = "";
        String id = "";
        JSONObject data;

        try
        {
            data = response.getJSONObject("data");
            username = data.getString("username");
            id_cliente = data.getString("id_cliente");
            tipo_usuario = data.getString("tipo_usuario");
            pais = data.getString("pais");
            id = data.getString("_id");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        sharedPreferences.putStringInSP("username", username);
        sharedPreferences.putStringInSP("id_cliente", id_cliente);
        sharedPreferences.putStringInSP("tipo_usuario", tipo_usuario);
        sharedPreferences.putStringInSP("pais", pais);
        sharedPreferences.putStringInSP("id", id);

        //Si existe ya un nombre de usuario
        if (sharedPreferences.isContained(DEVICEUSER))
        {
            //Añadir usuario a la lista de usuarios autorizados
            chipRedManager.addAuthenticatedDevice(id_cliente);
        }
        else
        {
            //Iniciar sesión normalmente para pedir nombre de usuario en la actividad
            showResult(true, null, null);
        }
    }

    private void parseResendConfirmEmail(JSONObject response)
    {
        try
        {
            // Obtener id del cliente encontrado
            String clientId = response.getJSONArray("data").getJSONObject(0).getString("id");

            // Enviar solicitud de reenvío de corrreo de confirmación
            chipRedManager.resendConfirmationEmail(clientId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public void requestNewPass()
    {
        final LayoutInflater inflater = getLayoutInflater();
        View alertDialog = inflater.inflate(R.layout.input_user_name, null);

        final AlertDialog.Builder builder = new AlertDialog.Builder(LogInActivity.this);
        final EditText input = alertDialog.findViewById(R.id.userNameInput);
        final TextView message = alertDialog.findViewById(R.id.ask_user_name_message);
        final AlertDialog dialog;

        input.setHint("usuario@correo.com");

        message.setText("Ingrese correo electrónico de cliente");
        builder.setTitle("Recuperación de contraseña");
        builder.setView(alertDialog);

        builder.setPositiveButton("Ingresar", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                String email = input.getText().toString().trim().toLowerCase();
                hideSoftKeyboard(LogInActivity.this, input);

                if (email.isEmpty() || !email.contains("@"))
                {
                    showErrorDialog();
                }
                else
                {
                    progressDialog = new ProgressDialog(LogInActivity.this);

                    progressDialog.setIndeterminate(true);
                    progressDialog.setMessage("Solicitando...");
                    progressDialog.show();

                    chipRedManager.forgottenPassword(email);
                }
            }
        });

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });

        dialog = builder.create();

        if (dialog.getWindow() != null)
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams
                    .SOFT_INPUT_STATE_VISIBLE);

        dialog.show();
    }

    public void requestConfirmationEmail()
    {
        final LayoutInflater inflater = getLayoutInflater();
        View alertDialog = inflater.inflate(R.layout.input_user_name, null);

        final AlertDialog.Builder builder = new AlertDialog.Builder(LogInActivity.this);
        final EditText input = alertDialog.findViewById(R.id.userNameInput);
        final TextView message = alertDialog.findViewById(R.id.ask_user_name_message);
        final AlertDialog dialog;

        input.setHint("usuario@correo.com");

        message.setText("Ingrese correo electrónico de cliente");
        builder.setTitle("Reenvío de correo de confirmación");
        builder.setView(alertDialog);

        builder.setPositiveButton("Ingresar", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                String email = input.getText().toString().trim().toLowerCase();
                hideSoftKeyboard(LogInActivity.this, input);

                if (email.isEmpty() || !email.contains("@"))
                {
                    showErrorDialog();
                }
                else
                {
                    progressDialog = new ProgressDialog(LogInActivity.this);

                    progressDialog.setIndeterminate(true);
                    progressDialog.setMessage("Solicitando...");
                    progressDialog.show();

                    chipRedManager.searchClient(ChipRedManager.BYEMAIL, email);
                }
            }
        });

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });

        dialog = builder.create();

        if (dialog.getWindow() != null)
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams
                    .SOFT_INPUT_STATE_VISIBLE);

        dialog.show();
    }

    public void showErrorDialog()
    {
        GenericDialog genericDialog = new GenericDialog("", "Correo electrónico no válido", new
                Runnable()
                {
                    @Override
                    public void run()
                    {
                        //nada
                    }
                }, null, this);
        genericDialog.show();
    }

    public boolean validate()
    {
        boolean valid = true;

        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())
        {
            emailText.setError("Ingrese un correo electrónico válido");
            valid = false;
        }
        else
        {
            emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 5)
        {
            passwordText.setError("Contraseña inválida");
            valid = false;
        }
        else
        {
            passwordText.setError(null);
        }

        return valid;
    }

    public void showResult(final boolean status, final String title, final String message)
    {
        progressDialog.dismiss();
        if (status)
        {
            onLoginSuccess();
        }
        else onLoginFailed(title, message);
    }

    public void onLoginSuccess()
    {
        login.setEnabled(true);
        Intent resultIntent = new Intent();
        setResult(0, resultIntent);
        finish();
    }

    public void onLoginFailed(String title, String message)
    {
        login.setEnabled(true);
        AlertDialog.Builder builder = new AlertDialog.Builder(LogInActivity.this);
        builder.setMessage(message).setTitle(title).setPositiveButton("Ok", null).show();
    }

    public static void hideSoftKeyboard(Activity activity, View view)
    {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context
                .INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }
}
