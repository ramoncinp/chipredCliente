package com.binarium.chipred;

import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;

public class GenericDialog
{
    Runnable positiveAction, negativeAction;
    String title, message, positiveText, negativeText;
    Context context;
    boolean cancelable;

    public GenericDialog(String title,
                         String message,
                         Runnable positiveAction,
                         Runnable negativeAction,
                         Context context)
    {
        this.title = title;
        this.message = message;
        this.positiveAction = positiveAction;
        this.negativeAction = negativeAction;
        this.context = context;

        positiveText = "Ok";
        negativeText = "Cancelar";
    }

    public void setPositiveText(String positiveText)
    {
        this.positiveText = positiveText;
    }

    public void setNegativeText(String negativeText)
    {
        this.negativeText = negativeText;
    }

    public void setCancelable(boolean cancelable)
    {
        this.cancelable = cancelable;
    }

    public void show()
    {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(cancelable);

        if (positiveAction != null)
        {
            alertDialog.setPositiveButton(positiveText,
                    new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i)
                        {
                            positiveAction.run();
                        }
                    });
        }

        if (negativeAction != null)
        {
            alertDialog.setNegativeButton(negativeText,
                    new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i)
                        {
                            if (negativeAction != null) negativeAction.run();
                            dialogInterface.dismiss();
                        }
                    });
        }

        alertDialog.show();
    }
}
