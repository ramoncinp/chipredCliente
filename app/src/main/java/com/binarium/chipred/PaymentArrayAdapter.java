package com.binarium.chipred;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class PaymentArrayAdapter extends RecyclerView.Adapter<PaymentArrayAdapter
        .PaymentViewHolder> implements View.OnClickListener
{
    private final ArrayList<Payment> payments;
    private Context context;
    private View.OnClickListener listener;

    public PaymentArrayAdapter(ArrayList<Payment> payments, Context context)
    {
        this.payments = payments;
        this.context = context;
    }

    @Override
    public int getItemCount()
    {
        return payments.size();
    }

    @Override
    public PaymentArrayAdapter.PaymentViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.paymentlayout,
                viewGroup, false);
        PaymentArrayAdapter.PaymentViewHolder pvh = new PaymentArrayAdapter.PaymentViewHolder(v,
                context, payments);
        v.setOnClickListener(this);

        return pvh;
    }

    @Override
    public void onBindViewHolder(PaymentArrayAdapter.PaymentViewHolder paymentViewHolder, int i)
    {
        Payment payment = payments.get(i);

        //Instanciar las sharedpreferences
        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(context);
        String country = sharedPreferencesManager.getStringFromSP("pais", "mexico");
        String amountString;

        if (country.equals("mexico"))
        {
            amountString = ChipREDConstants.MX_AMOUNT_FORMAT.format(payment.getAmount()) + " " +
                    ChipREDConstants.MXN_CURRENCY_ISO;
            paymentViewHolder.currencyIcon.setBackgroundResource(R.drawable.ic_payment);
        }
        else
        {
            amountString = ChipREDConstants.CR_AMOUNT_FORMAT.format(payment.getAmount()) + " " +
                    ChipREDConstants.CRC_CURRENCY_ISO;
            paymentViewHolder.currencyIcon.setBackgroundResource(R.drawable.ic_colon_crc);
        }

        paymentViewHolder.amount.setText(amountString);
        paymentViewHolder.date.setText(payments.get(i).getDate());
        paymentViewHolder.time.setText(payments.get(i).getTime());
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setOnClickListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onClick(View view)
    {
        if (listener != null)
        {
            listener.onClick(view);
        }
    }

    public static class PaymentViewHolder extends RecyclerView.ViewHolder
    {
        private TextView amount;
        private TextView date;
        private TextView time;
        private ImageView currencyIcon;

        public PaymentViewHolder(View itemView, Context context, ArrayList<Payment> payments)
        {
            super(itemView);

            currencyIcon = itemView.findViewById(R.id.payment_currency_icon);
            amount = (TextView) itemView.findViewById(R.id.paymentAmount);
            date = (TextView) itemView.findViewById(R.id.paymentDate);
            time = (TextView) itemView.findViewById(R.id.paymentTime);
        }
    }
}
