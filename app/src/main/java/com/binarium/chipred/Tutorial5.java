package com.binarium.chipred;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.Toolbar;
import android.view.WindowManager;

import com.github.paolorotolo.appintro.AppIntro;

public class Tutorial5 extends AppIntro
{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        addSlide(TutGenericFragment.newInstance(
                R.layout.tut_layout,
                "Mis abonos",
                "Consulta los abonos que se han hecho a tu cuenta",
                R.drawable.tutorial_frgmt_payments));

        addSlide(TutGenericFragment.newInstance(
                R.layout.tut_layout,
                "Saldo",
                "Toca cualquier elemento de la lista para conocer el detalle",
                R.drawable.tutorial_frgmt_payments2));

        setFadeAnimation();
        setIndicatorColor(
                getResources().getColor(R.color.colorAccent),
                getResources().getColor(R.color.lowRed)
        );

        setColorSkipButton(getResources().getColor(R.color.colorPrimaryDark));
        setColorDoneText(getResources().getColor(R.color.colorPrimaryDark));

        setSkipText("OMITIR");
        setDoneText("LISTO");

        setNextArrowColor(getResources().getColor(R.color.colorAccent));

        Toolbar toolbar = findViewById(R.id.toolbar);
        if (getSupportActionBar() == null) setSupportActionBar(toolbar);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);

        finishActivity();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);

        finishActivity();
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);

    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (getSupportActionBar() != null)
            getSupportActionBar().show();
    }

    private void finishActivity()
    {
        finish();
    }
}
