package com.binarium.chipred;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;

public class NotificationChannelManager
{
    private Context context;

    NotificationChannelManager(Context context)
    {
        this.context = context;
    }

    public void createDefaultNotificationChannel()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            CharSequence name = context.getResources().getString(R.string
                    .default_notification_channel_id);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel channel = new NotificationChannel(ChipREDConstants
                    .GENERAL_NOTIFICATIONS_ID, name, importance);
            NotificationManager notificationManager = context.getSystemService
                    (NotificationManager.class);
            if (notificationManager != null) notificationManager.createNotificationChannel(channel);
        }
    }
}
