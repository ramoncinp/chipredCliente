package com.binarium.chipred.auth;

import android.app.KeyguardManager;
import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

public class FingerprintHandler extends FingerprintManager.AuthenticationCallback
{
    //Objetos
    private KeyStore keyStore;
    private CancellationSignal cancellationSignal;
    private Cipher cipher;
    private Context context;
    private FingerPrintInterface fingerPrintInterface;

    //Constantes
    private static final String KEY_NAME = "ChipRedKey";

    public FingerprintHandler(Context context, FingerPrintInterface fingerPrintInterface)
    {
        this.context = context;
        this.fingerPrintInterface = fingerPrintInterface;
    }

    public void startAuth(FingerprintManager fingerprintManager)
    {
        if (Build.VERSION.SDK_INT >= 23)
        {
            generateKey();
            if (cipherInit())
            {
                FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
                cancellationSignal = new CancellationSignal();
                fingerprintManager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
            }
        }
        else
        {
            fingerPrintInterface.onError("Version de android incompatible");
        }
    }

    public void cancel()
    {
        cancellationSignal.cancel();
    }

    private void generateKey()
    {
        if (Build.VERSION.SDK_INT >= 23)
        {
            try
            {
                keyStore = KeyStore.getInstance("AndroidKeyStore");
                KeyGenerator keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");

                keyStore.load(null);
                keyGenerator.init(new
                        KeyGenParameterSpec.Builder(KEY_NAME,
                        KeyProperties.PURPOSE_ENCRYPT |
                                KeyProperties.PURPOSE_DECRYPT)
                        .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                        .setUserAuthenticationRequired(true)
                        .setEncryptionPaddings(
                                KeyProperties.ENCRYPTION_PADDING_PKCS7)
                        .build());
                keyGenerator.generateKey();

            }
            catch (KeyStoreException | IOException | CertificateException
                    | NoSuchAlgorithmException | InvalidAlgorithmParameterException
                    | NoSuchProviderException e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            fingerPrintInterface.onError("Versión de android incompatible");
        }
    }

    private boolean cipherInit()
    {
        if (Build.VERSION.SDK_INT >= 23)
        {
            try
            {
                cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
            }
            catch (NoSuchAlgorithmException | NoSuchPaddingException e)
            {
                throw new RuntimeException("Failed to get Cipher", e);
            }

            try
            {
                keyStore.load(null);
                SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME, null);
                cipher.init(Cipher.ENCRYPT_MODE, key);

                return true;
            }
            catch (KeyPermanentlyInvalidatedException e)
            {
                return false;
            }
            catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e)
            {
                throw new RuntimeException("Failed to init Cipher", e);
            }
        }
        else
        {
            fingerPrintInterface.onError("Versión de android incompatible");
            return false;
        }
    }

    @Override
    public void onAuthenticationError(int errorCode, CharSequence errString)
    {
        fingerPrintInterface.onError(errString.toString());
    }

    @Override
    public void onAuthenticationHelp(int helpCode, CharSequence helpString)
    {
        fingerPrintInterface.onHelp(helpString.toString());
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result)
    {
        fingerPrintInterface.onSucced(result.toString());
    }

    @Override
    public void onAuthenticationFailed()
    {
        fingerPrintInterface.onError("Error al autenticar");
    }

    public interface FingerPrintInterface
    {
        void onError(String message);

        void onHelp(String message);

        void onSucced(String message);
    }
}
