package com.binarium.chipred.auth;

public class SecurityMethod
{
    private String name;
    private boolean enabled;

    public SecurityMethod(String name, boolean enabled)
    {
        this.name = name;
        this.enabled = enabled;
    }

    public String getName()
    {
        return name;
    }

    public boolean isEnabled()
    {
        return enabled;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }
}
