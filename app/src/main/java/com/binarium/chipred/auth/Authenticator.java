package com.binarium.chipred.auth;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceFragment;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipred.ChipREDConstants;
import com.binarium.chipred.R;
import com.binarium.chipred.SharedPreferencesManager;

import static android.content.Context.FINGERPRINT_SERVICE;
import static com.binarium.chipred.ChipREDConstants.REQUEST_CODE_CONFIRM_DEVICE_CREDENTIALS;

public class Authenticator
{
    private Activity activity;
    private Fragment fragment;
    private PreferenceFragment preferenceFragment;
    private AuthenticatorInteface authenticatorInteface;
    private FingerprintHandler fingerprintHandler;
    private KeyguardManager keyguardManager;
    private SharedPreferencesManager spManager;

    /**
     * Importante!!
     * Esta clase utiliza "startForActivityResult" con el código
     * REQUEST_CODE_CONFIRM_DEVICE_CREDENTIALS,
     * y por lo tanto debe implementarse el método "onResult" en la actividad host para obtener
     * el resultado cuando es autenticado por NIP
     **/

    public Authenticator(Fragment fragment, AuthenticatorInteface authenticatorInteface)
    {
        this.fragment = fragment;
        this.authenticatorInteface = authenticatorInteface;
        activity = fragment.getActivity();

        spManager = new SharedPreferencesManager(activity);
    }

    public Authenticator(PreferenceFragment fragment, AuthenticatorInteface authenticatorInteface)
    {
        this.preferenceFragment = fragment;
        this.authenticatorInteface = authenticatorInteface;
        activity = fragment.getActivity();

        spManager = new SharedPreferencesManager(activity);
    }

    public boolean excecuteAuth()
    {
        boolean securityMethod = spManager.getBooleanFromSP(ChipREDConstants
                .SECURITY_METHOD_FOR_BILLETS, false);

        //Checar si el cliente autorizó la protección de Billetes electrónicos
        if (!securityMethod)
        {
            return false;
        }

        try
        {
            //Validar que exista un método de seguridad
            keyguardManager = (KeyguardManager) activity.getSystemService(Context.KEYGUARD_SERVICE);
            if (!keyguardManager.isKeyguardSecure())
            {
                Toast.makeText(activity, "No hay una contraseña asignada al dispositivo", Toast
                        .LENGTH_LONG).show();

                return false;
            }

            //Validar si hay huella
            if (Build.VERSION.SDK_INT >= 23)
            {
                //Checar que exista una huella registrada
                FingerprintManager fingerprintManager;
                fingerprintManager = (FingerprintManager) activity.getSystemService
                        (FINGERPRINT_SERVICE);

                if (fingerprintManager != null)
                {
                    if (!fingerprintManager.hasEnrolledFingerprints())
                    {
                        Log.d("SecurityMethod", "No hay una huella asignada al dispositivo");
                    }
                    else
                    {
                        //Crear diálogo
                        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                        View content = activity.getLayoutInflater().inflate(R.layout
                                .dialog_put_fingerprint, null);

                        final TextView text = content.findViewById(R.id.finger_print_text);
                        final ImageView fingerPrintImage = content.findViewById(R.id
                                .finger_print_iv);

                        builder.setView(content);
                        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i)
                            {
                                fingerprintHandler.cancel();
                                authenticatorInteface.onCancelledAuthentication();
                            }
                        });

                        final Dialog dialog = builder.create();
                        dialog.setCancelable(false);
                        //Mostrar diálogo
                        dialog.show();

                        fingerprintHandler = new FingerprintHandler(activity, new
                                FingerprintHandler.FingerPrintInterface()
                        {
                            @Override
                            public void onError(String message)
                            {
                                text.setTextColor(activity.getResources().getColor(R.color
                                        .colorPrimary));
                                text.setText(message);
                            }

                            @Override
                            public void onHelp(String message)
                            {
                                text.setTextColor(activity.getResources().getColor(R.color
                                        .colorPrimary));
                                text.setText("Error al autenticar");
                            }

                            @Override
                            public void onSucced(String message)
                            {
                                text.setTextColor(activity.getResources().getColor(R.color.green));
                                text.setText("Autenticado!");
                                fingerPrintImage.setImageResource(R.drawable.ic_fingerprint_green);

                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        //Iniciar fragment de Billetes
                                        authenticatorInteface.onAuthenticated();
                                        dialog.dismiss();
                                    }
                                }, 1000);
                            }
                        });
                        fingerprintHandler.startAuth(fingerprintManager);

                        return true;
                    }
                }
            }

            //Validar si hay NIP
            if (Build.VERSION.SDK_INT >= 21)
            {
                Intent intent = keyguardManager.createConfirmDeviceCredentialIntent(null, null);
                if (intent != null)
                {
                    if (fragment != null)
                    {
                        fragment.startActivityForResult(intent,
                                REQUEST_CODE_CONFIRM_DEVICE_CREDENTIALS);
                    }
                    else
                    {
                        preferenceFragment.startActivityForResult(intent,
                                REQUEST_CODE_CONFIRM_DEVICE_CREDENTIALS);
                    }
                }
                return true;
            }
        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
            Log.d("Authenticator", e.getMessage());
        }

        return false;
    }


    public interface AuthenticatorInteface
    {
        void onAuthenticated();

        void onCancelledAuthentication();
    }
}
