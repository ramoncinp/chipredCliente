package com.binarium.chipred.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.binarium.chipred.R;
import com.binarium.chipred.auth.SecurityMethod;

import java.util.ArrayList;

public class RadioButtonListAdapter extends RecyclerView.Adapter<RadioButtonListAdapter
        .RadioButtonListViewHolder> implements View.OnClickListener
{
    private ArrayList<SecurityMethod> list;
    private Context context;
    private View.OnClickListener listener;

    public RadioButtonListAdapter(ArrayList<SecurityMethod> list, Context context)
    {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getItemCount()
    {
        return list.size();
    }

    @Override
    public RadioButtonListAdapter.RadioButtonListViewHolder onCreateViewHolder(@NonNull ViewGroup
                                                                                           viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout
                .radio_button_list_element, viewGroup, false);

        v.setOnClickListener(this);

        return new RadioButtonListAdapter.RadioButtonListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RadioButtonListAdapter.RadioButtonListViewHolder
                                             radioButtonListViewHolder, int i)
    {
        radioButtonListViewHolder.name.setText(list.get(i).getName());
        radioButtonListViewHolder.radioButton.setChecked(list.get(i).isEnabled());
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public void onClick(View view)
    {
        if (listener != null)
        {
            listener.onClick(view);
        }
    }

    public void setOnClickListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    public static class RadioButtonListViewHolder extends RecyclerView.ViewHolder
    {
        private TextView name;
        private RadioButton radioButton;

        public RadioButtonListViewHolder(View itemView)
        {
            super(itemView);

            radioButton = itemView.findViewById(R.id.radio_button);
            name = itemView.findViewById(R.id.name);
        }
    }
}
