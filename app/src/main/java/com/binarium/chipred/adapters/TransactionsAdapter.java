package com.binarium.chipred.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.binarium.chipred.ChipREDConstants;
import com.binarium.chipred.R;
import com.binarium.chipred.Transaction;

import java.util.ArrayList;

public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter
        .PurchasesViewHolder> implements View.OnClickListener
{
    private final ArrayList<Transaction> transactions;
    private View.OnClickListener listener;

    public TransactionsAdapter(ArrayList<Transaction> transactions)
    {
        this.transactions = transactions;
    }

    @Override
    public int getItemCount()
    {
        return transactions.size();
    }

    @NonNull
    @Override
    public PurchasesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_layout,
                parent, false);

        PurchasesViewHolder purchasesViewHolder = new PurchasesViewHolder(v);
        v.setOnClickListener(this);
        return purchasesViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PurchasesViewHolder purchasesViewHolder, int i)
    {
        Transaction transaction = transactions.get(i);
        String amountText = "$" + ChipREDConstants.MX_AMOUNT_FORMAT.format(transaction.getAmount());

        //El color de la cantidad es verde por default, pero si es negativa o cero, se cambia a rojo
        if (transaction.getType() == Transaction.CONSUMO)
        {
            // Va para columna de abonos
            purchasesViewHolder.expenseAmount.setText(amountText);
            purchasesViewHolder.incomeAmount.setText("");
        }
        else
        {
            // Va para columna de consumos
            purchasesViewHolder.incomeAmount.setText(amountText);
            purchasesViewHolder.expenseAmount.setText("");
        }

        String balanceText =
                "$" + ChipREDConstants.MX_AMOUNT_FORMAT.format(transaction.getNewBalance());
        purchasesViewHolder.newBalance.setText(balanceText);
        purchasesViewHolder.purchaseDate.setText(transaction.getDateTime());
    }

    public void setOnClickListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onClick(View view)
    {
        if (listener != null)
        {
            listener.onClick(view);
        }
    }

    static class PurchasesViewHolder extends RecyclerView.ViewHolder
    {
        private TextView expenseAmount;
        private TextView incomeAmount;
        private TextView newBalance;
        private TextView purchaseDate;

        PurchasesViewHolder(View itemView)
        {
            super(itemView);
            purchaseDate = itemView.findViewById(R.id.date_tv);
            expenseAmount = itemView.findViewById(R.id.expense_amount_tv);
            newBalance = itemView.findViewById(R.id.new_balance_tv);
            incomeAmount = itemView.findViewById(R.id.income_amount_tv);
        }
    }
}
