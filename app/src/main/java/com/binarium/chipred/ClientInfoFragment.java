package com.binarium.chipred;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;

import com.binarium.chipred.facturacion.Invoice;
import com.binarium.chipred.facturacion.InvoicesUtils;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipred.facturacion.InvoicesArrayAdapter;
import com.binarium.chipred.facturacion.InvoiceFileSelector;
import com.binarium.chipred.facturacion.SearchInvoices;
import com.binarium.chipred.login.AddAccount;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.binarium.chipred.MainActivity.LOGGED;

public class ClientInfoFragment extends Fragment
{
    // Constantes
    private static final int SEARCH_INVOICES_INTENT = 1;

    // Variables
    private String username;
    private String idClient;
    private String country;
    private String[] modifyOptions = {"Modificar información de cliente", "Modificar NIP de " +
            "consulta de saldo"};

    // Objetos
    private Activity parentActivity;
    private Bitmap clientQr;
    private ChipRedManager chipRedManager;
    private Handler handler;
    private Menu clientMenu;
    private SharedPreferencesManager sharedPreferencesManager;

    // Views
    private Dialog changeNipDialog;
    private ProgressDialog getInfoProgressDialog;
    private ProgressDialog loadingProgress;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView user;

    public ClientInfoFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState)
    {
        if (getActivity() != null) parentActivity = getActivity();
        else return inflater.inflate(R.layout.error_layout, container, false);

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Inflate the layout for this fragment
        ((MainActivity) parentActivity).setActionBarTitle(getActivity().getResources().getString(R
                .string.clientNvItem));

        sharedPreferencesManager = new SharedPreferencesManager(parentActivity);
        handler = new Handler();

        setHasOptionsMenu(true);

        View v = inflater.inflate(R.layout.fragment_client_info, container, false);
        user = v.findViewById(R.id.user);

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived()
        {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN)
            {
                Log.d("ClientInfoFragment", "Error al obtener informacion");

                //Evaluar actividad padre
                if (ChipREDConstants.checkNullParentActivity(getActivity(),
                        ClientInfoFragment.this))
                    return;

                swipeRefreshLayout.setRefreshing(false);
                if (getInfoProgressDialog != null) getInfoProgressDialog.dismiss();

                if (crMessage.equals(ChipRedManager.LOGOUT))
                {
                    ((MainActivity) getActivity()).logOut(true);
                }
                else
                {
                    if (webServiceN == ChipRedManager.CHANGE_NIP_WB)
                    {
                        changeNipDialog.dismiss();
                    }
                    else if (webServiceN == ChipRedManager.GET_LAST_PURCHASES)
                    {
                        TextView noPurchases = getActivity().findViewById(R.id.no_last_invoices);
                        noPurchases.setVisibility(View.VISIBLE);

                        RecyclerView lastInvoicesList =
                                getActivity().findViewById(R.id.last_purchases_list);
                        lastInvoicesList.setVisibility(View.GONE);
                    }
                    Snackbar.make(getActivity().findViewById(android.R.id.content), crMessage,
                            Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN)
            {
                Log.d("ClientInfoFragment", "Error al obtener informacion");

                //Evaluar actividad padre
                if (ChipREDConstants.checkNullParentActivity(getActivity(),
                        ClientInfoFragment.this))
                    return;

                if (webServiceN == ChipRedManager.LOGIN_WB)
                {
                    Toast.makeText(getActivity(), "Contraseña incorrecta", Toast.LENGTH_SHORT).show();
                    return;
                }

                swipeRefreshLayout.setRefreshing(false);

                final String messageToShow = errorMessage;
                if (getInfoProgressDialog != null) getInfoProgressDialog.dismiss();
                if (webServiceN == ChipRedManager.CHANGE_NIP_WB) changeNipDialog.dismiss();

                GenericDialog genericDialog =
                        new GenericDialog(getActivity().getResources().getString
                                (R.string.errorConexionTitulo), messageToShow, new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                //nada...
                            }
                        }, null, getActivity());

                genericDialog.show();
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN)
            {
                Log.d("ClientInfoFragment", "Informacion obtenida");

                //Evaluar actividad padre
                if (ChipREDConstants.checkNullParentActivity(getActivity(),
                        ClientInfoFragment.this))
                    return;

                swipeRefreshLayout.setRefreshing(false);

                if (webServiceN == ChipRedManager.CHECK_BALANCE_WB)
                {
                    getBalanceInfo(response);
                }
                else if (webServiceN == ChipRedManager.GET_INVOICES)
                {
                    getLastPurchases(response);
                }
                else if (webServiceN == ChipRedManager.LOGIN_WB)
                {
                    // Mostrar progressDialog
                    loadingProgress = new ProgressDialog(getActivity());
                    loadingProgress.setMessage("Cargando...");
                    loadingProgress.show();

                    // Crear Intent
                    Intent intent = new Intent(getActivity(), AddAccount.class);
                    intent.putExtra("activityAction", 2);
                    startActivityForResult(intent, SEARCH_INVOICES_INTENT);
                }

                prepareClientQr();
            }
        }, getActivity());

        getClientInfo();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);

        clientMenu = menu;
        inflater.inflate(R.menu.client_menu, clientMenu);

        //Si no se generó el QR...
        if (clientQr == null) clientMenu.getItem(0).setVisible(true);

        // Obtener menu y ocultar elemento del menu para modificar información de cliente
        if (!country.equals("mexico"))
            clientMenu.findItem(R.id.edit_client_info).setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int itemId = item.getItemId();

        switch (itemId)
        {
            case R.id.edit_client_info:
                showModifyOptions();

                return true;

            case R.id.show_client_qr:
                showQrInDialog();

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SEARCH_INVOICES_INTENT)
        {
            // Ocultar progressDialog si esque se mostraba uno
            if (loadingProgress != null && loadingProgress.isShowing())
            {
                loadingProgress.dismiss();
            }
        }
    }

    private void showModifyOptions()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        TextView textView = new TextView(getActivity());
        textView.setText("Seleccione una opción");
        textView.setTextColor(getActivity().getResources().getColor(R.color.black));
        textView.setTypeface(textView.getTypeface(), Typeface.BOLD);
        textView.setPadding(50, 50, 10, 10);
        textView.setTextSize(ChipREDConstants.dpToPixels(6, getActivity()));
        builder.setCustomTitle(textView);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R
                .layout.simple_list_item_1);
        arrayAdapter.add(modifyOptions[0]);
        arrayAdapter.add(modifyOptions[1]);

        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                selectModifyAction(arrayAdapter.getItem(i));
                dialogInterface.dismiss();
            }
        });

        builder.setCancelable(true);
        builder.show();
    }

    private void selectModifyAction(String itemName)
    {
        if (itemName.equals(modifyOptions[0]))
        {
            showSetPasswordDialog();
        }
        else if (itemName.equals(modifyOptions[1]))
        {
            showChangeNipDialog();
        }
    }

    private void showSetPasswordDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Ingrese contraseña");

        View content = getLayoutInflater().inflate(R.layout.edit_text_layout, null);
        final EditText editText = content.findViewById(R.id.edit_text);
        editText.setHint("Contraseña");

        // Definir EditText
        builder.setView(content);

        // Definir listeners
        builder.setPositiveButton("Ingresar", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                chipRedManager.userLogIn(username, editText.getText().toString());
            }
        });

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.dismiss();
            }
        });

        builder.setCancelable(true);
        builder.show();
    }

    private void showChangeNipDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Cambiar NIP");

        View content = getLayoutInflater().inflate(R.layout.modify_balance_nip, null);

        final EditText newNipEt = content.findViewById(R.id.change_nip_new_nip);
        final EditText passwordEt = content.findViewById(R.id.change_nip_password);
        Button submit = content.findViewById(R.id.change_nip_submit_button);
        Button cancel = content.findViewById(R.id.change_nip_cancel_button);
        final ProgressBar progressBar = content.findViewById(R.id.change_nip_progress);
        final LinearLayout changeNipContent = content.findViewById(R.id.change_nip_content);

        builder.setView(content);
        changeNipDialog = builder.create();
        changeNipDialog.show();

        submit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                final String newNip = newNipEt.getText().toString();
                final String password = passwordEt.getText().toString();
                boolean valid = true;

                if (newNip.length() != 4)
                {
                    newNipEt.setError("NIP inválido");
                    valid = false;
                }

                if (password.isEmpty())
                {
                    passwordEt.setError("Contraseña inválida");
                    valid = false;
                }

                if (valid)
                {
                    //Cerrar teclado
                    try
                    {
                        changeNipDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams
                                .SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    }
                    catch (NullPointerException e)
                    {
                        e.printStackTrace();
                    }

                    //Mostrar ProgressBar
                    progressBar.setVisibility(View.VISIBLE);
                    changeNipContent.setVisibility(View.INVISIBLE);

                    //Enviar solicitud de cambio de NIP a servidor
                    chipRedManager.changeBalanceNip(idClient, newNip, password, username);
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                changeNipDialog.dismiss();
            }
        });
    }

    public void getClientInfo()
    {
        if (sharedPreferencesManager.isContained(LOGGED))
        {
            username = sharedPreferencesManager.getStringFromSP("username", "");
            idClient = sharedPreferencesManager.getStringFromSP("id_cliente", "");
            country = sharedPreferencesManager.getStringFromSP("pais", "");

            // Mostrar texto
            user.setText(username);

            // Mostrar views para el usuario
            showCountryUserViews();

            // Referenciar views
            swipeRefreshLayout = getActivity().findViewById(R.id.client_info_refresh_layout);

            // Mostrar progress
            getInfoProgressDialog = new ProgressDialog(getActivity());
            getInfoProgressDialog.setMessage("Obteniendo información de cliente...");
            getInfoProgressDialog.show();
            getInfoProgressDialog.setCancelable(false);

            // Evaluar país
            if (country.equals("mexico"))
            {
                if (showTutorial())
                {
                    Intent intent = new Intent(getActivity(), Tutorial1.class);
                    startActivity(intent);
                }

                // Pedir información de saldo
                Log.d("ClientInfoFragment", "Pidiendo saldo");
                chipRedManager.checkBalance(idClient, true);

                // Definir swipe listener
                swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
                {
                    @Override
                    public void onRefresh()
                    {
                        chipRedManager.checkBalance(idClient, true);
                    }
                });
            }
            else
            {
                // Obtener ultimos consumos
                chipRedManager.getInvoices("", "", 0, "");

                // Definir swipe listener
                swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
                {
                    @Override
                    public void onRefresh()
                    {
                        // Obtener ultimos consumos
                        chipRedManager.getInvoices("", "", 0, "");
                    }
                });

                // Ocultar elementos del menu
                setCrMenuElements();

                // Definir listener para buscar más facturas
                CardView searchInvoices = getActivity().findViewById(R.id.search_invoices);
                searchInvoices.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        // Abrir actividad para buscar facturas por filtros
                        Intent intent = new Intent(getActivity(), SearchInvoices.class);

                        // Iniciar actividad
                        startActivity(intent);
                    }
                });
            }
        }
    }

    public void getBalanceInfo(JSONObject response)
    {
        setClientInfo();
    }

    public void setClientInfo()
    {
    }

    boolean showTutorial()
    {
        String version = BuildConfig.VERSION_NAME;

        //Eliminar esta variable por las versiones anteriores
        if (sharedPreferencesManager.isContained("WelcomeDialog"))
        {
            sharedPreferencesManager.remove("WelcomeDialog");
        }

        if (!sharedPreferencesManager.getStringFromSP("version", "").equals(version))
        {
            sharedPreferencesManager.putStringInSP("version", version);
            return true;
        }
        else
        {
            return false;
        }
    }

    private void showQrInDialog()
    {
        Activity activity = getActivity();
        LayoutInflater inflater = getLayoutInflater();

        View alertDialog = inflater.inflate(R.layout.qrview, null);

        final ImageView imageView = alertDialog.findViewById(R.id.qrview);
        imageView.setImageBitmap(clientQr);
        imageView.requestLayout();
        imageView.getLayoutParams().height = getDisplayDims() - 100;
        imageView.getLayoutParams().width = getDisplayDims() - 100;

        TextView textView = alertDialog.findViewById(R.id.amountTv);
        textView.setVisibility(View.GONE);

        TextView textViewId = alertDialog.findViewById(R.id.deviceIdTv);
        textViewId.setVisibility(View.GONE);

        ImageView shareIcon = alertDialog.findViewById(R.id.share_billet_icon);
        shareIcon.setVisibility(View.GONE);

        AlertDialog.Builder alertadd = new AlertDialog.Builder(activity);
        alertadd.setView(alertDialog);
        alertadd.setNegativeButton("Cerrar", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });

        alertadd.setTitle("ID de Cliente");
        alertadd.setCancelable(false);
        alertadd.show();
    }

    private void prepareClientQr()
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                final Bitmap bitmap;
                MultiFormatWriter multiFormatWriter = new MultiFormatWriter();

                try
                {
                    BitMatrix bitMatrix = multiFormatWriter.encode(idClient, BarcodeFormat.QR_CODE,
                            250, 250);

                    BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                    bitmap = barcodeEncoder.createBitmap(bitMatrix);

                    if (bitmap != null)
                    {
                        getActivity().runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                //Hacer visible el elemento del menu
                                clientQr = bitmap;
                                clientMenu.findItem(R.id.show_client_qr).setVisible(true);

                                //Terminar proceso de obtención de información
                                if (getInfoProgressDialog != null) getInfoProgressDialog.dismiss();
                            }
                        });
                    }
                }
                catch (WriterException e)
                {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private int getDisplayDims()
    {
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        return outMetrics.widthPixels;
    }

    public void setCrMenuElements()
    {
        // Obtener menu
        NavigationView mNavigationView = getActivity().findViewById(R.id.nav_view);
        Menu navMenu = mNavigationView.getMenu();

        // Obtener elementos para ocultar
        MenuItem billetList = navMenu.findItem(R.id.billetList);
        MenuItem generateQr = navMenu.findItem(R.id.generate_qr);
        MenuItem purchases = navMenu.findItem(R.id.listpurchases);
        MenuItem payments = navMenu.findItem(R.id.listpayments);
        MenuItem mainTut = navMenu.findItem(R.id.main_tut);
        MenuItem reports = navMenu.findItem(R.id.reports);

        // Deshabilitar elementos
        if (billetList != null) billetList.setVisible(false);
        if (generateQr != null) generateQr.setVisible(false);
        if (purchases != null) purchases.setVisible(false);
        if (payments != null) payments.setVisible(false);
        if (mainTut != null) mainTut.setVisible(false);
        if (reports != null) reports.setVisible(false);
    }

    public void getLastPurchases(JSONObject response)
    {
        //Crear lista de comprobantes
        final ArrayList<Invoice> invoices = new ArrayList<>();

        try
        {
            //Obtener los comprobantes
            JSONArray data = response.getJSONArray("data");
            for (int i = 0; i < data.length(); i++)
            {
                // Obtener objeto JSON
                JSONObject purchase = data.getJSONObject(i);

                // Convertir json a objeto factura
                Invoice invoice = InvoicesUtils.purchaseToInvoice(purchase);

                // Validar si la factura se pudo convertir
                if (invoice != null)
                    // Agregar a la lista
                    invoices.add(invoice);
            }

            // Referenciar lista
            final RecyclerView lastInvoicesList =
                    getActivity().findViewById(R.id.last_purchases_list);
            TextView noPurchases = getActivity().findViewById(R.id.no_last_invoices);

            if (invoices.isEmpty())
            {
                noPurchases.setVisibility(View.VISIBLE);
                lastInvoicesList.setVisibility(View.GONE);
            }
            else
            {
                noPurchases.setVisibility(View.GONE);
                lastInvoicesList.setVisibility(View.VISIBLE);

                // Crear adaptador
                InvoicesArrayAdapter adapter = new InvoicesArrayAdapter(invoices, getContext());

                // Asignar clickListener
                adapter.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        InvoiceFileSelector invoiceFileSelector =
                                InvoiceFileSelector.newInstance(invoices.get(lastInvoicesList.getChildAdapterPosition(view)).toJSON().toString());
                        invoiceFileSelector.show(getActivity().getSupportFragmentManager(),
                                "invoice_file_selector");
                    }
                });

                // Asignar adaptador
                lastInvoicesList.setAdapter(adapter);
                lastInvoicesList.setLayoutManager(new LinearLayoutManager(getContext()));
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            Snackbar.make(getActivity().findViewById(android.R.id.content), "Error al procesar " +
                            "los últimos consumos",
                    Snackbar.LENGTH_LONG).show();
        }
    }

    public void showCountryUserViews()
    {
        LinearLayout balanceLayout = getActivity().findViewById(R.id.balance_layout);
        LinearLayout lastPurchasesLayout =
                getActivity().findViewById(R.id.last_invoices_layout);

        if (country.equals("mexico"))
        {
            // Obtener layout correspondiente
            balanceLayout.setVisibility(View.VISIBLE);
            lastPurchasesLayout.setVisibility(View.GONE);
        }
        else
        {
            // Obtener layout correspondiente
            balanceLayout.setVisibility(View.GONE);
            lastPurchasesLayout.setVisibility(View.VISIBLE);
        }
    }
}