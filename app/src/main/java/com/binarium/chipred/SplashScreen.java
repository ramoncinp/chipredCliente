package com.binarium.chipred;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.ImageView;

public class SplashScreen extends Activity
{
    ImageView img;

    private final Handler handler = new Handler();

    private final Runnable startActivityRunnable = new Runnable() {

        @Override
        public void run() {
            Intent intent = new Intent();
            intent.setClass(SplashScreen.this,MainActivity.class);
            startActivity(intent);
            finish();
        }
    };

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        img = (ImageView) findViewById (R.id.chipRedSplashIcon);
    }

    @Override
    protected void onResume() {
        super.onResume();

        AnimationSet set = new AnimationSet(true);

        Animation fadeIn = FadeIn(1000);
        fadeIn.setStartOffset(0);
        set.addAnimation(fadeIn);

        img.startAnimation(set);

        handler.postDelayed(startActivityRunnable, 2000);
    }

    public void onPause()
    {
        super.onPause();
        handler.removeCallbacks(startActivityRunnable);
    }

    private Animation FadeIn(int t)
    {
        Animation fade;
        fade = new AlphaAnimation(0.0f,1.0f);
        fade.setDuration(t);
        fade.setInterpolator(new AccelerateInterpolator());
        return fade;
    }
}
