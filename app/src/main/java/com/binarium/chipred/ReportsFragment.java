package com.binarium.chipred;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipred.adapters.TransactionsAdapter;
import com.binarium.chipred.utils.DateUtils;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION;
import static android.content.Intent.FLAG_GRANT_WRITE_URI_PERMISSION;
import static com.binarium.chipred.ChipREDConstants.twoDigits;

public class ReportsFragment extends Fragment implements ChipRedManager.OnMessageReceived
{
    // Constantes
    private final static String TAG = ReportsFragment.class.getSimpleName();
    private final static int SHARE_INTENT = 3;

    // Variables
    private boolean exportPdf = false;
    private boolean sharePdf = false;
    private int bitmapsCount;
    private String totalIncomesText;
    private String totalExpensesText;

    // Views
    private LinearLayout expensesTableLayout;
    private LinearLayout totalsLayout;
    private ProgressBar progressBar;
    private RecyclerView transactionsList;
    private TextView datesTitle;
    private TextView noTransactions;
    private TextView tableTotalIncome;
    private TextView tableTotalExpense;

    // Objetos
    private Activity activity;
    private ArrayList<Transaction> transactions = new ArrayList<>();
    private Bitmap[] transactionBitmaps;
    private ChipRedManager chipRedManager;
    private Date startDate;
    private Date endDate;
    private File pdfFile;
    private List<Target> targets = new ArrayList<>();

    public ReportsFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        if (getActivity() != null)
        {
            getActivity().setTitle("Reportes");
        }

        setHasOptionsMenu(true);

        View content = inflater.inflate(R.layout.fragment_reports, container, false);
        transactionsList = content.findViewById(R.id.transactions_list);
        progressBar = content.findViewById(R.id.progress_bar);
        expensesTableLayout = content.findViewById(R.id.expenses_table_layout);
        totalsLayout = content.findViewById(R.id.totals_layout);
        datesTitle = content.findViewById(R.id.dates_title);
        tableTotalIncome = content.findViewById(R.id.total_income);
        tableTotalExpense = content.findViewById(R.id.total_expense);
        noTransactions = content.findViewById(R.id.no_transactions);

        // Inflate the layout for this fragment
        return content;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        //Definir fechas de este mes por default
        setDates();
        setDatesTitle();

        //Crear instancia
        chipRedManager = new ChipRedManager(this, getContext());

        //Obtener reportes
        chipRedManager.getTransactions(startDate, endDate, true);

        //Revisar permisos
        checkPermissions();
    }

    private void checkPermissions()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED)
            {
                Log.v(TAG, "Permission is granted");
            }
            else
            {
                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }
        }
        else
        {
            //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.reports_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        if (item.getItemId() == R.id.date_selector)
        {
            showDateSelectorDialog();
        }
        else if (item.getItemId() == R.id.export_as_pdf)
        {
            if (transactions.size() > 0)
            {
                exportPdf = true;
                if (pdfFile == null) createPdf();
                else exportPdf();
            }
        }
        else if (item.getItemId() == R.id.share)
        {
            if (transactions.size() > 0)
            {
                sharePdf = true;
                if (pdfFile == null) createPdf();
                else sharePdf();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void chipRedMessage(String crMessage, int webServiceN)
    {
        //Evaluar actividad padre
        if (ChipREDConstants.checkNullParentActivity(getActivity(),
                ReportsFragment.this))
            return;

        if (crMessage.equals(ChipRedManager.LOGOUT))
        {
            ((MainActivity) getActivity()).logOut(true);
        }
        else
        {
            if (transactions.isEmpty())
            {
                noTransactions.setVisibility(View.VISIBLE);
            }
            else
            {
                Toast.makeText(getActivity(), crMessage, Toast.LENGTH_LONG).show();
            }
            progressBar.setVisibility(View.GONE);
        }

        Log.d(TAG, crMessage);
    }

    @Override
    public void chipRedError(String errorMessage, int webServiceN)
    {
        //Evaluar actividad padre
        if (ChipREDConstants.checkNullParentActivity(getActivity(),
                ReportsFragment.this))
            return;

        if (errorMessage.equals(ChipRedManager.LOGOUT))
        {
            ((MainActivity) getActivity()).logOut(true);
        }
        else
        {
            Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
        }

        Log.e(TAG, errorMessage);
    }

    @Override
    public void showResponse(JSONObject response, int webServiceN)
    {
        //Evaluar actividad padre
        if (ChipREDConstants.checkNullParentActivity(getActivity(),
                ReportsFragment.this))
            return;

        // Convertir respuesta a lista de objetos
        getReports(response);
    }

    private void getReports(JSONObject response)
    {
        double incomesAmount = 0;
        double expensesAmount = 0;

        Log.d(TAG, response.toString());

        try
        {
            // Obtener arreglo de transacciones
            JSONArray data = response.getJSONArray("data");
            Log.d(TAG, "Se encontraron " + data.length() + " transacciones");

            // Iterar en el arreglo
            for (int i = 0; i < data.length(); i++)
            {
                // Obtener objeto JSON
                JSONObject item = data.getJSONObject(i);

                //Crear objeto Transaction
                Transaction transaction = new Transaction();
                transaction.setDateTime(item.getString("fecha"));
                transaction.setLastBalance(item.getDouble("saldo_anterior"));
                transaction.setNewBalance(item.getDouble("saldo_nuevo"));

                // Obtener cantidad
                double amount = item.getDouble("cantidad");

                //Evaluar si es consumo o abono
                if (item.getString("tipo_consumo").equals("VB"))
                {
                    // Es consumo
                    transaction.setType(Transaction.CONSUMO);
                    transaction.setAmount(amount);
                    transaction.setStationNumber(item.getString("numero_estacion"));
                }
                else
                {
                    // Es abono
                    transaction.setType(Transaction.ABONO);
                    transaction.setAmount(amount);
                    transaction.setStationNumber(item.getString("id_estacion"));
                }

                // Agregar datos de vehículo
                if (item.has("reservacion"))
                {
                    transaction.setVehiclePlates(item.getJSONObject("reservacion").getString(
                            "placas"));
                    transaction.setVehiclePlatesUrl(item.getJSONObject("reservacion").getString(
                            "url_placas"));
                }

                // Agregar al respectivo acumulador
                if (transaction.getType() == Transaction.ABONO)
                {
                    incomesAmount += transaction.getAmount();
                }
                else
                {
                    expensesAmount += transaction.getAmount();
                }

                // Agregar transaccion
                transactions.add(0, transaction);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        // Crear adaptador
        TransactionsAdapter transactionsAdapter = new TransactionsAdapter(transactions);

        // Asignar adaptador
        transactionsList.setAdapter(transactionsAdapter);
        transactionsList.setLayoutManager(new LinearLayoutManager(getActivity()));

        // Mostrar totales
        totalIncomesText = "$" + ChipREDConstants.MX_AMOUNT_FORMAT.format(incomesAmount);
        totalExpensesText = "$" + ChipREDConstants.MX_AMOUNT_FORMAT.format(expensesAmount);
        tableTotalIncome.setText(totalIncomesText);
        tableTotalExpense.setText(totalExpensesText);

        // Obtener bitmaps de transacciones en un nuevo hilo
        getTransactionsBitmaps();
    }

    private void showDateSelectorDialog()
    {
        // Crear builder
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Definir titulo
        builder.setTitle("Seleccionar fechas");

        // Crear view
        View content = getLayoutInflater().inflate(R.layout.set_dates_layout, null);

        // Definir view
        builder.setView(content);

        // Obtener views
        final MaterialEditText startDateEt = content.findViewById(R.id.start_date);
        final MaterialEditText endDateEt = content.findViewById(R.id.end_date);
        Button cancelButton = content.findViewById(R.id.cancel_button);
        Button submitButton = content.findViewById(R.id.submit_button);

        // Mostrar fechas actuales
        startDateEt.setText(DateUtils.dateObjectToString(startDate));
        endDateEt.setText(DateUtils.dateObjectToString(endDate));

        // Definir datePickers
        View.OnClickListener datePickerListener = new View.OnClickListener()
        {
            @Override
            public void onClick(final View view)
            {
                DatePickerFragment fragment = DatePickerFragment.newInstance(new DatePickerDialog
                        .OnDateSetListener()
                {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2)
                    {
                        String gottenDate = twoDigits(i2) + "/" + DateUtils.monthNumberToString
                                (i1) + "/" + i;
                        ((MaterialEditText) (view)).setText(gottenDate);
                    }
                });

                fragment.show(getActivity().getFragmentManager(), "datePicker");
            }
        };

        // Asignar datePickers
        startDateEt.setOnClickListener(datePickerListener);
        endDateEt.setOnClickListener(datePickerListener);

        // Crear diálogo
        final Dialog dialog = builder.create();
        dialog.show();

        // Asignar listener de botones
        submitButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Validar
                boolean valid = startDateEt.validateWith(new METValidator("Seleccionar fecha")
                {
                    @Override
                    public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
                    {
                        return !isEmpty;
                    }
                });
                valid &= endDateEt.validateWith(new METValidator("Seleccionar fecha")
                {
                    @Override
                    public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
                    {
                        return !isEmpty;
                    }
                });

                // Si es valido...
                if (valid)
                {
                    Date startDateObj =
                            DateUtils.dateStringToObject(startDateEt.getText().toString());
                    Date endDateObj =
                            DateUtils.dateStringToObject(endDateEt.getText().toString());

                    if (startDateObj == null || endDateObj == null)
                    {
                        Toast.makeText(getActivity(), "Error al procesar fechas " +
                                "seleccionadas", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        // Validar que la fecha final sea mayor
                        if (endDateObj.getTime() > startDateObj.getTime())
                        {
                            // Definir las nuevas fechas
                            ReportsFragment.this.startDate.setTime(startDateObj.getTime());
                            ReportsFragment.this.endDate.setTime(endDateObj.getTime());

                            // Limpiar variables y objetos
                            transactions.clear();
                            bitmapsCount = 0;
                            transactionBitmaps = null;
                            pdfFile = null;

                            // Ocultar views
                            showProgress();

                            // Mostrar fechas en título
                            setDatesTitle();

                            //Obtener reportes
                            chipRedManager.getTransactions(ReportsFragment.this.startDate,
                                    ReportsFragment.this.endDate, true);

                            // Cerrar diálogo
                            dialog.dismiss();
                        }
                        else
                        {
                            startDateEt.setError("Fecha inválida");
                            endDateEt.setError("Fecha inválida");
                        }
                    }
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });
    }

    private void setDatesTitle()
    {
        String title = getDatesTitle();
        datesTitle.setText(title);
    }

    private String getDatesTitle()
    {
        // Obtener texto de las fechas
        return DateUtils.dateObjectToString(startDate) + " - " + DateUtils.dateObjectToString(endDate);
    }

    private void showProgress()
    {
        noTransactions.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        expensesTableLayout.setVisibility(View.GONE);
        totalsLayout.setVisibility(View.GONE);
    }

    private void setDates()
    {
        // Definir la fecha de inicio
        startDate = DateUtils.getFirstDayOfTheMonth();

        // Definir la fecha final
        endDate = DateUtils.getLastDayOfTheMonth();
    }

    private void createPdf()
    {
        Toast.makeText(getActivity(), "Generando reporte en PDF, por favor espere...",
                Toast.LENGTH_LONG).show();

        Thread thread = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                // File dir
                String fileDir =
                        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();

                // Definir archivo
                pdfFile = new File(fileDir + "/ReporteChipRED.pdf");

                // Create PDF file
                try
                {
                    // Create file
                    Document document = new Document();
                    PdfWriter writer = PdfWriter.getInstance(document,
                            new FileOutputStream(pdfFile.getAbsolutePath()));
                    document.open();

                    Rectangle pageSize = document.getPageSize();
                    float width = pageSize.getRight();
                    float height = pageSize.getTop();

                    // Add metadata
                    document.setPageSize(PageSize.A4);
                    document.addCreationDate();

                    // Agregar marca de agua
                    Image backgroundWatermark = getImageFromAsset("img/icon_chipred.png");
                    BackGroundImage backGroundImageEvent = new BackGroundImage(backgroundWatermark);
                    writer.setPageEvent(backGroundImageEvent);
                    if (backgroundWatermark != null)
                    {
                        backgroundWatermark.scalePercent(50);
                        backgroundWatermark.setAbsolutePosition(width / 2 - backgroundWatermark.getScaledWidth() / 2,
                                height / 2 - backgroundWatermark.getScaledHeight() / 2);
                        document.add(backgroundWatermark);
                    }

                    // Agregar imagen de título
                    Image crImage = getImageFromAsset("img/chipred.png");
                    if (crImage != null)
                    {
                        crImage.scalePercent(30);
                        document.add(crImage);
                    }

                    // Motrar capítulo
                    Chunk titleChunk = new Chunk("Estado de cuenta",
                            new Font(Font.FontFamily.HELVETICA, 20,
                                    Font.BOLD));
                    Paragraph titleParagraph = new Paragraph(titleChunk);
                    titleParagraph.setAlignment(Element.ALIGN_CENTER);
                    document.add(titleParagraph);

                    // Crear tabla
                    PdfPTable table = new PdfPTable(5);
                    table.setTotalWidth(width - 100);
                    table.setLockedWidth(true);
                    table.setSpacingBefore(8);

                    // Agregar titulos
                    Chunk chunk = new Chunk(getDatesTitle());
                    chunk.setFont(new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD,
                            BaseColor.WHITE));
                    Phrase titlePhrase = new Phrase(chunk);

                    PdfPCell headerCell = new PdfPCell();
                    headerCell.setColspan(5);
                    headerCell.setPhrase(titlePhrase);
                    headerCell.setPadding(8);
                    headerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    headerCell.setBackgroundColor(BaseColor.RED);
                    table.addCell(headerCell);

                    // Definir títulos
                    String[] subtitles = new String[]{"Fecha", "Consumo", "Abono", "Saldo",
                            "Placas"};
                    Font headerSubtitleFont = new Font();
                    headerSubtitleFont.setStyle(Font.BOLD);
                    headerSubtitleFont.setColor(BaseColor.WHITE);

                    for (int i = 0; i < subtitles.length; i++)
                    {
                        Chunk subtitleChunk = new Chunk(subtitles[i]);
                        Font subtitleFont = new Font(Font.FontFamily.HELVETICA);
                        subtitleFont.setColor(BaseColor.WHITE);
                        subtitleChunk.setFont(headerSubtitleFont);
                        Phrase subtitlePhrase = new Phrase(subtitleChunk);
                        PdfPCell subtitleCell = new PdfPCell();
                        subtitleCell.setPhrase(subtitlePhrase);
                        subtitleCell.setPadding(6);
                        subtitleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        subtitleCell.setBackgroundColor(new BaseColor(62, 62, 62));
                        table.addCell(subtitleCell);
                    }

                    //Evaluar lista de transacciones
                    for (int i = 0; i < transactions.size(); i++)
                    {
                        Transaction transaction = transactions.get(i);

                        String amountText =
                                "$" + ChipREDConstants.MX_AMOUNT_FORMAT.format(transaction.getAmount());
                        String balanceText =
                                "$" + ChipREDConstants.MX_AMOUNT_FORMAT.format(transaction.getNewBalance());

                        PdfPCell content = new PdfPCell();
                        content.setPadding(6);

                        content.setPhrase(new Phrase(DateUtils.getFormattedDateString(transaction.getDateTime())));
                        table.addCell(content);

                        content.setPhrase(new Phrase(transaction.getType() == Transaction.CONSUMO ?
                                amountText : ""));
                        table.addCell(content);

                        content.setPhrase(new Phrase(transaction.getType() == Transaction.ABONO ?
                                amountText : ""));
                        table.addCell(content);

                        content.setPhrase(new Phrase(balanceText));
                        table.addCell(content);

                        if (transaction.getVehiclePlatesUrl() != null && !transaction.getVehiclePlatesUrl().isEmpty())
                        {
                            // Obtener imagen
                            Image platesImage = getImageFromBitmap(transactionBitmaps[i]);
                            if (platesImage != null)
                            {
                                content = new PdfPCell(platesImage, true);
                                content.setFixedHeight(50);
                                content.setHorizontalAlignment(Element.ALIGN_CENTER);
                            }
                            else
                            {
                                content.setPhrase(new Phrase(transaction.getVehiclePlates()));
                            }
                        }
                        else
                        {
                            content.setPhrase(new Phrase(transaction.getVehiclePlates()));
                        }
                        table.addCell(content);
                    }

                    // Agregar totales
                    Font totalsFont = new Font();
                    totalsFont.setStyle(Font.BOLD);
                    totalsFont.setColor(BaseColor.BLACK);

                    PdfPCell totalCell = new PdfPCell();
                    totalCell.setBackgroundColor(BaseColor.YELLOW);
                    totalCell.setPadding(6);

                    Chunk totalChunk1 = new Chunk("TOTAL");
                    totalChunk1.setFont(totalsFont);
                    totalCell.setPhrase(new Phrase(totalChunk1));
                    table.addCell(totalCell);

                    Chunk totalChunk2 = new Chunk(totalExpensesText);
                    totalChunk2.setFont(totalsFont);
                    totalCell.setPhrase(new Phrase(totalChunk2));
                    table.addCell(totalCell);

                    Chunk totalChunk3 = new Chunk(totalIncomesText);
                    totalChunk3.setFont(totalsFont);
                    totalCell.setPhrase(new Phrase(totalChunk3));
                    table.addCell(totalCell);

                    Chunk totalChunk4 = new Chunk("");
                    totalChunk4.setFont(totalsFont);
                    totalCell.setPhrase(new Phrase(totalChunk4));
                    table.addCell(totalCell);

                    Chunk totalChunk5 = new Chunk("");
                    totalChunk5.setFont(totalsFont);
                    totalCell.setPhrase(new Phrase(totalChunk5));
                    table.addCell(totalCell);

                    // Agregar tabla
                    document.add(table);

                    // Cerrar documento
                    document.close();

                    // Decidir qué hacer con el archivo
                    if (sharePdf)
                    {
                        sharePdf();
                        sharePdf = false;
                    }
                    else if (exportPdf)
                    {
                        exportPdf();
                        exportPdf = false;
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    getActivity().runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            Toast.makeText(getActivity(), "Error al generar PDF",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

        thread.start();
    }

    private void getTransactionsBitmaps()
    {
        // Inicializar arreglo del tamaño de la lista de transacciones
        transactionBitmaps = new Bitmap[transactions.size()];

        // Iterar en la lista de transacciones
        for (int i = 0; i < transactions.size(); i++)
        {
            Transaction transaction = transactions.get(i);
            if (transaction.getVehiclePlatesUrl() != null && !transaction.getVehiclePlatesUrl().isEmpty())
            {
                getImageFromUrl(transaction.getVehiclePlatesUrl(), i);
            }
            else
            {
                bitmapsCount++;
                Log.d(TAG, "BitmapsCount -> " + bitmapsCount + " de " + transactions.size());
                evaluateAllBitmapsReady();
            }
        }
    }

    private void evaluateAllBitmapsReady()
    {
        if (bitmapsCount == transactions.size())
        {
            // Ocultar progressBar
            progressBar.setVisibility(View.GONE);
            expensesTableLayout.setVisibility(View.VISIBLE);
            totalsLayout.setVisibility(View.VISIBLE);
        }
    }

    private Image getImageFromAsset(String assetPath)
    {
        try
        {
            InputStream ims = getActivity().getAssets().open(assetPath);
            Bitmap bm = BitmapFactory.decodeStream(ims);
            return getImageFromBitmap(bm);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    private Image getImageFromBitmap(Bitmap bm)
    {
        try
        {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
            return Image.getInstance(stream.toByteArray());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    private void getImageFromUrl(final String url, final int itemIdx)
    {
        final Target mTarget = new Target()
        {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from)
            {
                transactionBitmaps[itemIdx] = bitmap;
                bitmapsCount++;
                Log.d(TAG, "BitmapsCount -> " + bitmapsCount + " de " + transactions.size());
                Log.d(TAG, "BitmapsItemIdx -> " + itemIdx);
                evaluateAllBitmapsReady();
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable)
            {
                bitmapsCount++;
                Log.d(TAG, "BitmapFailed");
                Log.d(TAG, "BitmapsCount -> " + bitmapsCount + " de " + transactions.size());
                evaluateAllBitmapsReady();
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable)
            {
                /*bitmapsCount++;
                Log.d(TAG, "onPrepareLoad");
                Log.d(TAG, "BitmapsCount -> " + bitmapsCount + " de " + transactions.size());
                evaluateAllBitmapsReady();*/
            }
        };
        targets.add(mTarget);

        Picasso.with(getActivity()).load(url).into(mTarget);
    }

    private class BackGroundImage extends PdfPageEventHelper
    {
        private Image image;

        public BackGroundImage(Image image)
        {
            this.image = image;
        }

        @Override
        public void onStartPage(PdfWriter writer, Document document)
        {
            Rectangle pageSize = document.getPageSize();
            float width = pageSize.getRight();
            float height = pageSize.getTop();

            if (image != null)
            {
                try
                {
                    image.scalePercent(50);
                    image.setAbsolutePosition(width / 2 - image.getScaledWidth() / 2,
                            height / 2 - image.getScaledHeight() / 2);
                    document.add(image);
                }
                catch (DocumentException e)
                {
                    e.printStackTrace();
                    Log.d(TAG, e.getMessage());
                }
            }
        }
    }

    private void exportPdf()
    {
        try
        {
            Uri fileUri;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            {
                fileUri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID +
                        ".fileprovider", pdfFile);
            }
            else
            {
                fileUri = Uri.fromFile(pdfFile);
            }

            if (fileUri != null)
            {
                String mime = getActivity().getContentResolver().getType(fileUri);
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(fileUri, mime);
                intent.setFlags(FLAG_GRANT_READ_URI_PERMISSION | FLAG_GRANT_WRITE_URI_PERMISSION);

                getActivity().startActivity(intent);
            }
        }
        catch (NullPointerException | IllegalArgumentException e)
        {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Error al exportar a PDF", Toast.LENGTH_SHORT).show();
        }
    }

    private void sharePdf()
    {
        try
        {
            Uri fileUri;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            {
                fileUri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID +
                        ".fileprovider", pdfFile);
            }
            else
            {
                fileUri = Uri.fromFile(pdfFile);
            }

            if (fileUri != null)
            {
                Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_STREAM, fileUri);
                shareIntent.setDataAndType(fileUri,
                        getActivity().getContentResolver().getType(fileUri));
                startActivityForResult(Intent.createChooser(shareIntent, "Compartir archivo"),
                        SHARE_INTENT);
            }
        }
        catch (NullPointerException | IllegalArgumentException e)
        {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Error al compartir archivo", Toast.LENGTH_SHORT).show();
        }
    }
}
