package com.binarium.chipred;

import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class TutGenericFragment extends Fragment
{
    private static final String ARG_LAYOUT_RES_ID = "layoutResId";
    private int layoutResId;

    private String title;
    private String description;
    private int imageId;

    public static TutGenericFragment newInstance(
            int layoutResId,
            String title,
            String description,
            int imageId)
    {
        TutGenericFragment tutFragment = new TutGenericFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_LAYOUT_RES_ID, layoutResId);
        args.putString("title", title);
        args.putString("description", description);
        args.putInt("image", imageId);

        tutFragment.setArguments(args);

        return tutFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(ARG_LAYOUT_RES_ID))
        {
            layoutResId = getArguments().getInt(ARG_LAYOUT_RES_ID);
            title = getArguments().getString("title");
            description = getArguments().getString("description");
            imageId = getArguments().getInt("image");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState)
    {
        View fragmentView = inflater.inflate(layoutResId, container, false);

        ImageView imageView = fragmentView.findViewById(R.id.purchaseTutImgView);
        TextView titleTv = fragmentView.findViewById(R.id.titleTut1);
        TextView descTv = fragmentView.findViewById(R.id.descTut1);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(getResources(), imageId, options);

        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;

        int newWidth = 500;
        int newHeight = (newWidth * imageHeight) / imageWidth;

        Picasso.with(getContext()).load(imageId).resize(newWidth, newHeight).into(imageView);
        titleTv.setText(title);
        descTv.setText(description);

        return fragmentView;
    }
}
