package com.binarium.chipred;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class TwoRowAdapter extends BaseAdapter
{
    Context context;
    ArrayList<String> keys, values, colors;
    LayoutInflater inflater;

    private float textSize = 0;

    public TwoRowAdapter(Context context, ArrayList<String> keys, ArrayList<String> values, float
            textSize, ArrayList<String> colors)
    {
        this.context = context;
        this.keys = keys;
        this.values = values;
        this.textSize = textSize;
        this.colors = colors;
    }

    public TwoRowAdapter(Context context, ArrayList<String> keys, ArrayList<String> values, float
            textSize)
    {
        this.context = context;
        this.keys = keys;
        this.values = values;
        this.textSize = textSize;
    }

    public TwoRowAdapter(Context context, ArrayList<String> keys, ArrayList<String> values)
    {
        this.context = context;
        this.keys = keys;
        this.values = values;
    }

    @Override
    public int getCount()
    {
        return keys.size();
    }

    @Override
    public Object getItem(int position)
    {
        return null;
    }

    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        TextView keysTv, valuesTv;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.two_row_column, parent, false);
        keysTv = itemView.findViewById(R.id.row_two_column_key);
        valuesTv = itemView.findViewById(R.id.row_two_column_value);

        keysTv.setText(keys.get(position));
        valuesTv.setText(values.get(position));

        if (textSize != 0)
        {
            keysTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
            valuesTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        }

        if (colors != null)
        {
            String color = colors.get(position);
            keysTv.setTextColor(Color.parseColor(color));
            valuesTv.setTextColor(Color.parseColor(color));
        }

        return itemView;
    }
}
