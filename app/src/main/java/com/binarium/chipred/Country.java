package com.binarium.chipred;

public class Country
{
    private String name;
    private String identifier;
    private int imageResource;

    public Country(String name, int imageResource, String identifier)
    {
        this.name = name;
        this.imageResource = imageResource;
        this.identifier = identifier;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getImageResource()
    {
        return imageResource;
    }

    public void setImageResource(int imageResource)
    {
        this.imageResource = imageResource;
    }

    public String getIdentifier()
    {
        return identifier;
    }

    public void setIdentifier(String identifier)
    {
        this.identifier = identifier;
    }
}
