package com.binarium.chipred;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.Toolbar;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.github.paolorotolo.appintro.AppIntro;

public class Tutorial3 extends AppIntro
{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        addSlide(TutGenericFragment.newInstance(
                R.layout.tut_layout,
                "Generación de Billetes",
                "Para generar un Billete, " +
                        "ingresa la denominación que requieras y presiona el botón " +
                        "\"GENERAR\"",
                R.drawable.tutorial_frgmt_gnrt_billet1));

        addSlide(TutGenericFragment.newInstance(
                R.layout.tut_layout,
                "Generación de Billetes",
                "El código QR representa al Billete generado y es almacenado en tu lista " +
                        "de Billetes",
                R.drawable.tutorial_frgmt_gnrt_billet2));

        setFadeAnimation();
        setIndicatorColor(
                getResources().getColor(R.color.colorAccent),
                getResources().getColor(R.color.lowRed)
        );

        setColorSkipButton(getResources().getColor(R.color.colorPrimaryDark));
        setColorDoneText(getResources().getColor(R.color.colorPrimaryDark));

        setSkipText("OMITIR");
        setDoneText("LISTO");

        setNextArrowColor(getResources().getColor(R.color.colorAccent));

        Toolbar toolbar = findViewById(R.id.toolbar);
        if (getSupportActionBar() == null) setSupportActionBar(toolbar);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (getSupportActionBar() != null)
            getSupportActionBar().hide();

        hideKeyboard();
    }

    @Override
    public void onSkipPressed(Fragment currentFragment)
    {
        super.onSkipPressed(currentFragment);

        finishActivity();
    }

    @Override
    public void onDonePressed(Fragment currentFragment)
    {
        super.onDonePressed(currentFragment);

        finishActivity();
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment)
    {
        super.onSlideChanged(oldFragment, newFragment);

    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (getSupportActionBar() != null)
            getSupportActionBar().show();
    }

    private void finishActivity()
    {
        finish();
    }

    public void hideKeyboard()
    {
        if (getCurrentFocus() != null)
        {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

            if (inputMethodManager != null)
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}
