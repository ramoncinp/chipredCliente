package com.binarium.chipred;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.Toolbar;
import android.view.WindowManager;

import com.github.paolorotolo.appintro.AppIntro;

public class Tutorial1 extends AppIntro
{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        addSlide(TutGenericFragment.newInstance(R.layout.tut_layout, "¡Bienvenido a ChipRED!",
                "Desliza para conocer más sobre la aplicación", R.drawable.chipred));

        addSlide(TutGenericFragment.newInstance(R.layout.tut_layout, "Saldo", "Consulta el saldo " +
                "" + "que tienes en tus cuentas ChipRED", R.drawable.tutorial_saldo));

        addSlide(TutGenericFragment.newInstance(R.layout.tut_layout, "Generación de Billetes",
                "Genera los Billetes de la denominación que requieras", R.drawable
                        .tutorial_generacion));

        addSlide(TutGenericFragment.newInstance(R.layout.tut_layout, "Billetes Electrónicos",
                "Consulta la información de tus Billetes, " + "sus estados y el dispositivo " +
                        "donde" + " fueron generados", R.drawable.tutorial_billetes));

        addSlide(TutPurchases.newInstance(R.layout.tut_layout));

        addSlide(TutGenericFragment.newInstance(R.layout.tut_layout, "Abonos", "Consulta la " +
                "información de los abonos que " + "se han hecho a tus cuentas ChipRED", R
                .drawable.tutorial_abonos));

        addSlide(TutGenericFragment.newInstance(R.layout.tut_layout, "Cuenta", "Puedes consultar " +
                "" + "la información de tu cuenta y modificarla si es " + "necesario", R.drawable
                .tutorial_editar));

        addSlide(TutGenericFragment.newInstance(R.layout.tut_layout, "Identificación", "Utiliza "
                + "tu código de identificación para agilizar tus operaciones en una estación", R
                .drawable.tutorial_identificacion));

        addSlide(TutGenericFragment.newInstance(R.layout.tut_layout, "Menú", "Accede a todas las " +
                "" + "funciones desde el menú principal", R.drawable.tutorial_menu3));

        addSlide(TutGenericFragment.newInstance(R.layout.tut_layout, "Ayuda", "Busca el botón de " +
                "" + "ayuda para obtener más detalles", R.drawable.tutorial_ayuda));

        setFadeAnimation();
        setIndicatorColor(getResources().getColor(R.color.colorAccent), getResources().getColor(R
                .color.lowRed));

        setColorSkipButton(getResources().getColor(R.color.colorPrimaryDark));
        setColorDoneText(getResources().getColor(R.color.colorPrimaryDark));

        setSkipText("OMITIR");
        setDoneText("COMENZAR");

        setNextArrowColor(getResources().getColor(R.color.colorAccent));

        Toolbar toolbar = findViewById(R.id.toolbar);
        if (getSupportActionBar() == null) setSupportActionBar(toolbar);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager
                .LayoutParams.FLAG_FULLSCREEN);

        if (getSupportActionBar() != null) getSupportActionBar().hide();
    }

    @Override
    public void onSkipPressed(Fragment currentFragment)
    {
        super.onSkipPressed(currentFragment);

        finishActivity();
    }

    @Override
    public void onDonePressed(Fragment currentFragment)
    {
        super.onDonePressed(currentFragment);

        finishActivity();
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment)
    {
        super.onSlideChanged(oldFragment, newFragment);

    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (getSupportActionBar() != null) getSupportActionBar().show();
    }

    private void finishActivity()
    {
        finish();
    }
}
