package com.binarium.chipred;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CountryArrayAdapter extends ArrayAdapter<Country>
{
    public CountryArrayAdapter(Context context, ArrayList<Country> countries)
    {
        super(context, 0, countries);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        return initView(position, convertView, parent);
    }

    @Nullable
    @Override
    public Country getItem(int position)
    {
        return super.getItem(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.country_item,
                    parent, false);
        }

        ImageView imageViewFlag = convertView.findViewById(R.id.country_item_image);
        TextView textViewName = convertView.findViewById(R.id.country_item_name);

        Country country = getItem(position);

        if (country != null)
        {
            imageViewFlag.setImageResource(country.getImageResource());
            textViewName.setText(country.getName());
        }

        return convertView;
    }
}
