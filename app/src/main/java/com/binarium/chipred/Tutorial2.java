package com.binarium.chipred;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.Toolbar;
import android.view.WindowManager;

import com.github.paolorotolo.appintro.AppIntro;

public class Tutorial2 extends AppIntro
{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        addSlide(TutGenericFragment.newInstance(
                R.layout.tut_layout,
                "Billetes Electrónicos",
                "Si tu cuenta es utilizada en más de un dispositivo, " +
                        "se generan estas tarjetas indicando el nombre de usuario y dispositivo",
                R.drawable.tutorial_frgmt_billet_list1));

        addSlide(TutGenericFragment.newInstance(
                R.layout.tut_layout,
                "Billetes Electrónicos",
                "Toca la tarjeta de un usuario para ver sus Billetes generados",
                R.drawable.tutorial_frgmt_billet_list2));

        addSlide(TutGenericFragment.newInstance(
                R.layout.tut_layout,
                "Billetes Electrónicos",
                "Toca la tarjeta de un Billete para ver su código QR",
                R.drawable.tutorial_frgmt_billet_list3));

        addSlide(TutGenericFragment.newInstance(
                R.layout.tut_layout,
                "Billetes Electrónicos",
                "Toca el código QR para verlo en pantalla completa",
                R.drawable.tutorial_frgmt_billet_list4));

        addSlide(TutGenericFragment.newInstance(
                R.layout.tut_layout,
                "Billetes Electrónicos",
                "Si necesitas cancelar un Billete, mantén presionada su tarjeta",
                R.drawable.tutorial_frgmt_billet_list5));

        setFadeAnimation();
        setIndicatorColor(
                getResources().getColor(R.color.colorAccent),
                getResources().getColor(R.color.lowRed)
        );

        setColorSkipButton(getResources().getColor(R.color.colorPrimaryDark));
        setColorDoneText(getResources().getColor(R.color.colorPrimaryDark));

        setSkipText("OMITIR");
        setDoneText("LISTO");

        setNextArrowColor(getResources().getColor(R.color.colorAccent));

        Toolbar toolbar = findViewById(R.id.toolbar);
        if (getSupportActionBar() == null) setSupportActionBar(toolbar);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);

        finishActivity();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);

        finishActivity();
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);

    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (getSupportActionBar() != null)
            getSupportActionBar().show();
    }

    private void finishActivity()
    {
        finish();
    }
}
